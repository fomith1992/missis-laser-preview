module.exports = function (api) {
  if (api && api.cache) {
    api.cache(true)
  }

  return {
    presets: ['babel-preset-expo'],
    plugins: [
      [
        'module-resolver',
        {
          cwd: 'babelrc',
          root: ['.'],
          extensions: ['.js', '.ts', '.tsx', '.ios.js', '.android.js'],
          alias: require('./aliases.js'),
        },
      ],
      ['react-native-reanimated/plugin'],
    ],
  }
}
