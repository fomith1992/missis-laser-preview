import styled from '@emotion/native'
import { ContentLayout } from '@shared/ui'
import React from 'react'
import { Dimensions, Image, StyleSheet } from 'react-native'
import Icon from '../../assets/splash.png'

export const SplashScreen = () => {
  return (
    <Container flex>
      <Image source={Icon} style={style.icon} />
    </Container>
  )
}

const width = Dimensions.get('window').width

const Container = styled(ContentLayout)`
  justify-content: center;
  align-items: center;
`
export const style = StyleSheet.create({
  icon: {
    width: width,
  },
})
