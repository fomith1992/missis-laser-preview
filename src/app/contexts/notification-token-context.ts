import { createContext, useContext } from 'react'
import * as Device from 'expo-device'

const notificationTokenContext = createContext<string | null>(null)

export const NotificationTokenProvider = notificationTokenContext.Provider

export function useNotificationToken(): string {
  const context = useContext(notificationTokenContext)
  if (context == null && Device.isDevice) {
    throw new Error('Notification context not initialized')
  }

  return context ?? 'Notification working only in real devices and Web'
}
