import React from 'react'
import { RootNavigator } from '@navigations/stack-root'

import { useLoadingProcesses } from './processes'

import AppLoading from 'expo-app-loading'

export const Application = () => {
  const loading = useLoadingProcesses()

  if (loading) {
    return <AppLoading />
  }

  return <RootNavigator />
}
