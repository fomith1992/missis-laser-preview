import styled from '@emotion/native'
import { BaseView, Body, Headiline } from '@shared/ui'
import { TextInput } from '@shared/ui/atoms/text-input'
import React from 'react'
import { SectionList } from 'react-native'
import { color } from 'src/style/mixins/color'

export interface OptionsProps {
  title: string | null
  data: Array<{
    city_id: number
    city: string
  }>
}

interface SelectCityViewProps {
  items: OptionsProps[]
  onChange: (city_id: number) => void
  searchItem: string
  setSearchItem: (searchItem: string) => void
}

export function SelectCityView({
  items,
  onChange,
  searchItem,
  setSearchItem,
}: SelectCityViewProps): React.ReactElement {
  const filteredOptions =
    searchItem === ''
      ? items
      : items
          .map((group) => ({
            title: group.title,
            data: group.data.filter(
              ({ city }) =>
                city
                  .split(' ')
                  .filter((piece) => piece.toLowerCase().replace(/[()+]/g, '').startsWith(searchItem.toLowerCase()))
                  .length !== 0,
            ),
          }))
          .filter((result) => result.data.length !== 0)
  return (
    <SectionList
      contentContainerStyle={{ paddingHorizontal: 16, flexGrow: 1 }}
      ListHeaderComponent={
        <BaseView mb={4}>
          <TextInput placeholder="Поиск" value={searchItem} onChangeText={setSearchItem} />
        </BaseView>
      }
      sections={filteredOptions}
      renderItem={({ item }) => {
        return (
          <ListItemContainer onPress={() => onChange(item.city_id)}>
            <Body>{item.city}</Body>
          </ListItemContainer>
        )
      }}
      keyExtractor={(item) => item.city}
      renderSectionHeader={({ section: { title } }) =>
        title != null ? (
          <ListTitleItem mt={16} mb={4}>
            {title}
          </ListTitleItem>
        ) : (
          <></>
        )
      }
      ListEmptyComponent={
        <EmptyContainer>
          <Body>Ничего не найдено</Body>
        </EmptyContainer>
      }
    />
  )
}

const ListItemContainer = styled.TouchableOpacity`
  padding-top: 12px;
  padding-bottom: 12px;
  margin-top: 8px;
`

const EmptyContainer = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`

const ListTitleItem = styled(Headiline)`
  color: ${color('primary')};
`
