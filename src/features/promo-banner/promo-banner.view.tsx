import styled from '@emotion/native'
import { BaseView, Headiline, RowView } from '@shared/ui'
import { TextButton } from '@shared/ui/buttons/text-button.view'
import { CloseIcon20 } from '@shared/ui/icons/close.icon-20'
import React, { useRef, useState } from 'react'
import { ImageSourcePropType, ScrollView, Animated, useWindowDimensions } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import { color } from 'src/style/mixins/color'
import { container } from 'src/style/mixins/space'

export interface PromoBannerData {
  icon: ImageSourcePropType
  title: string
  description?: string
  buttonText: string
}

interface PromoBannerProps {
  data: PromoBannerData[]
  onEndAnimated: () => void
}

const AnimatedView = Animated.View

export function PromoBanner({ data, onEndAnimated }: PromoBannerProps): React.ReactElement {
  const scrollX = useRef<ScrollView>(null)
  const scroll = useRef(new Animated.Value(0)).current
  const [page, setPage] = useState(0)

  const { width: windowWidth } = useWindowDimensions()

  const moveBody = (index: number): void => {
    scrollX.current?.scrollTo({
      x: index * windowWidth,
      animated: true,
    })
    setPage(index)
  }

  return (
    <Container>
      <ScrollView
        horizontal
        pagingEnabled
        scrollEnabled={false}
        showsHorizontalScrollIndicator={false}
        ref={scrollX}
        onScroll={Animated.event(
          [
            {
              nativeEvent: {
                contentOffset: {
                  x: scroll,
                },
              },
            },
          ],
          { useNativeDriver: false },
        )}
        scrollEventThrottle={1}
      >
        {data.map((item, idx) => {
          const { icon, description, title, buttonText } = item
          return (
            <SafeAreaView key={idx}>
              <ItemContainer style={{ width: windowWidth }} key={idx}>
                <RowView>
                  <CloseButton onPress={onEndAnimated}>
                    <CloseIcon20 />
                  </CloseButton>
                </RowView>
                <BodyContainer>
                  <IconContainer>
                    <Image source={icon} width={220} height={230} />
                  </IconContainer>
                </BodyContainer>
                <TitleContainer>
                  <Headiline>{title}</Headiline>
                  {description && <HeadilineAccent>{description}</HeadilineAccent>}
                </TitleContainer>
                <ButtonContainer>
                  <TextButton
                    onPress={() => {
                      if (data.length - 1 === page) {
                        onEndAnimated()
                      } else {
                        moveBody(page + 1)
                      }
                    }}
                  >
                    {buttonText}
                  </TextButton>
                </ButtonContainer>
              </ItemContainer>
            </SafeAreaView>
          )
        })}
      </ScrollView>
      <DotsContainer>
        {data.map((_, imageIndex) => {
          const width = scroll.interpolate({
            inputRange: [windowWidth * (imageIndex - 1), windowWidth * imageIndex, windowWidth * (imageIndex + 1)],
            outputRange: [12, 62, 12],
            extrapolate: 'clamp',
          })
          return <Dot key={imageIndex} style={[{ width }]} />
        })}
      </DotsContainer>
    </Container>
  )
}

const Container = styled(BaseView)`
  flex: 1;
`
const ItemContainer = styled(BaseView)`
  flex-grow: 1;
`
const IconContainer = styled(BaseView)`
  width: 260px;
  height: 260px;
  border-radius: 130px;
  overflow: hidden;
  background-color: ${color('primaryLight')};
  justify-content: center;
  align-items: center;
`
const TitleContainer = styled(BaseView)`
  ${container('margin')}
`
const ButtonContainer = styled(BaseView)`
  margin-top: 28px;
  ${container('margin')}
`
const BodyContainer = styled(BaseView)`
  flex: 1;
  justify-content: center;
  align-items: center;
`
const HeadilineAccent = styled(Headiline)`
  color: ${color('primary')};
`
const Image = styled.Image`
  width: 220px;
  height: 240px;
`

const Dot = styled(AnimatedView)`
  width: 12px;
  height: 12px;
  margin-left: 6px;
  margin-right: 6px;
  border-radius: 6px;
  overflow: hidden;
  background-color: ${color('primaryLight')};
`
const DotsContainer = styled(RowView)`
  margin-top: 24px;
  margin-bottom: 24px;
  justify-content: center;
`
const CloseButton = styled.TouchableOpacity`
  padding: 16px;
`
