export const DEFAULT_NUMBER_OF_MONTHS = 3

export const weekDayNames = ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс']

export const dayParts = ['утро', 'день', 'вечер']
