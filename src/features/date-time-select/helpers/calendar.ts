import { startOfMonth, startOfWeek, endOfMonth, endOfWeek, startOfDay, addDays, add, format } from 'date-fns'
import { ru } from 'date-fns/locale'

export type TMonthsData = ReturnType<typeof takeMonths>
export interface TMonthObj {
  monthName: string
  monthData: {
    date: Date
    canAppoint: boolean
  }[][]
}

function takeWeek(start = new Date()) {
  let date = startOfWeek(startOfDay(start), { weekStartsOn: 1 })

  return function () {
    const week = [...Array(7)].map((_, i) => addDays(date, i))
    date = addDays(week[6], 1)
    return week
  }
}

export function takeMonth(start = new Date()) {
  let month: Date[][] = []
  let date = start

  function lastDayOfRange(range: Date[][]) {
    return range[range.length - 1][6]
  }

  return function () {
    const weekGen = takeWeek(startOfMonth(date))
    const endDate = startOfDay(endOfWeek(endOfMonth(date)))
    month.push(weekGen())

    while (lastDayOfRange(month) < endDate) {
      month.push(weekGen())
    }

    const range = month
    month = []
    date = addDays(lastDayOfRange(range), 1)

    return range
  }
}

export function takeMonths(numberOfMonths: number, now: Date) {
  const monthsArr = []
  for (let i = 0; i < numberOfMonths; i++) {
    const monthData = takeMonth(
      add(now, {
        months: i,
      }),
    )()

    const monthName = getMonthName(
      add(now, {
        months: i,
      }),
    )
    monthsArr.push({ monthData, monthName })
  }
  return monthsArr
}

export function getMonthName(date: Date): string {
  return format(date, 'LLLL', {
    locale: ru,
  })
}
