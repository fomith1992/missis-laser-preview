import { dayParts } from '@features/date-time-select/constants'
import { format, parseISO } from 'date-fns'
import { ru } from 'date-fns/locale'

export const converTimeToDayTime = (time: string) => {
  const dayPartWithDatetime = format(parseISO(time), 'k', { locale: ru })
  if (+dayPartWithDatetime < 12) return dayParts[0]
  if (+dayPartWithDatetime >= 12 && +dayPartWithDatetime < 17) return dayParts[1]
  if (+dayPartWithDatetime >= 17) return dayParts[2]
  return undefined
}

export const removeZero = (time: string) => {
  if (time === '00:00') return time
  if (time[0] === '0') return time.slice(1)
  return time
}
