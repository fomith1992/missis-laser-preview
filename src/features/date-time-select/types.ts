export interface ITime {
  id: string
  time: string
}

export interface ITimeWithFormatting {
  formattedTimeNoZero: string
  dayPart: string | undefined
  datetime: string
  seance_length: number
  sum_length: number
  time: string
}

export interface IViewObject {
  index: number
  isViewable: boolean
  item: ITimeWithFormatting
}
