import React from 'react'
import { takeMonths } from '@features/date-time-select/helpers/calendar'
import { FlatList } from 'react-native'
import { TimeSelector } from '@features/date-time-select/ui/time-selector'
import { DEFAULT_NUMBER_OF_MONTHS } from '@features/date-time-select/constants'
import { Calendar } from '@features/date-time-select/ui/calendar.view'
import { CalendarTitle } from '@features/date-time-select/ui/calendar-title'
import { ConfirmButton } from '@features/date-time-select/ui/confirm-button'
import { ITimeWithFormatting } from '@features/date-time-select/types'
import styled from '@emotion/native'

import { AxiosError } from 'axios'
import { TResponse } from '@shared/api/types'
import useAsync from 'react-use/lib/useAsync'
import { bookAPI } from '@shared/api'
import { format } from 'date-fns'
import { LoadingIndicator } from '@shared/ui/atoms/loading-indicator'
import { TimesResponse } from '../../shared/api/book'
import { useRootStore } from 'src/stores/lib/use-root-store'
import { Body } from '@shared/ui'

type DateTimeSelectProps = {
  selectedServices: number[]
  workerId: number
  excludeRecordId?: number
  moveRecordCompanyId?: number
  recordUpdating?: boolean
  onConfirm: (date: string) => void
}

export const DateTimeSelect = ({
  onConfirm,
  selectedServices,
  moveRecordCompanyId,
  workerId,
  recordUpdating,
  excludeRecordId,
}: DateTimeSelectProps): React.ReactElement => {
  const now = React.useRef(new Date()).current
  const slidesRef = React.useRef<FlatList>(null)
  const { currentClinicId } = useRootStore().geolocationReport

  const monthsData = React.useMemo(() => takeMonths(DEFAULT_NUMBER_OF_MONTHS, now), [now])
  const [datesData, setDatesData] = React.useState<string[]>([])
  const [timeData, setTimeData] = React.useState<null | TimesResponse[]>(null)

  const [selectedMonth, setSelectedMonth] = React.useState(monthsData[0].monthName)
  const [selectedDate, setSelectedDate] = React.useState<Date | null>(null)
  const [selectedTime, setSelectedTime] = React.useState<ITimeWithFormatting | null>(null)

  const queryTimes = useAsync(async () => {
    setTimeData(null)
    setSelectedTime(null)
    if (selectedDate) {
      const companyId = moveRecordCompanyId || currentClinicId
      if (!companyId) return
      await bookAPI
        .getTimesByStaffId({
          selectedServices,
          workerId,
          excludeRecordId,
          companyId,
          date: format(selectedDate, 'yyyy-MM-dd'),
        })
        .then(({ data }) => setTimeData(data.data))
        .catch((e: AxiosError<TResponse>) => console.log('error ', e))
    }
  }, [selectedDate])

  const queryDates = useAsync(async () => {
    const companyId = moveRecordCompanyId || currentClinicId
    if (!companyId) return
    await bookAPI
      .getDatesByStaffId({
        selectedServices,
        workerId,
        excludeRecordId,
        companyId,
      })
      .then(({ data }) => setDatesData(data.data.working_dates))
      .catch((e: AxiosError<TResponse>) => console.log('error ', e))
  }, [selectedServices, workerId, excludeRecordId])

  const onCalendarTitlePress = React.useCallback((index) => {
    slidesRef?.current?.scrollToIndex({ animated: true, index })
  }, [])

  const handleViewableItemsChanged = React.useCallback(({ viewableItems }) => {
    setSelectedMonth(viewableItems[0].item.monthName)
  }, [])

  const data = monthsData.map((defaultDate) => ({
    monthName: defaultDate.monthName,
    monthData: defaultDate.monthData.map((x) =>
      x.map((y) => ({
        date: y,
        canAppoint: datesData.includes(format(y, 'yyyy-MM-dd')),
      })),
    ),
  }))

  if (queryDates.loading) {
    return <LoadingIndicator visible />
  }

  return (
    <ScreenContainer>
      <CalendarTitle
        monthsData={monthsData}
        selectedMonth={selectedMonth}
        setSelectedMonth={setSelectedMonth}
        onPress={onCalendarTitlePress}
      />
      <CalendarContainer>
        <FlatList
          data={data}
          renderItem={({ item }) => (
            <Calendar now={now} selectedDate={selectedDate} setSelectedDate={setSelectedDate} monthData={item} />
          )}
          keyExtractor={(item) => item.monthName}
          showsHorizontalScrollIndicator={false}
          horizontal
          pagingEnabled
          contentContainerStyle={{
            width: `${100 * DEFAULT_NUMBER_OF_MONTHS}%`,
          }}
          scrollEventThrottle={200}
          onViewableItemsChanged={handleViewableItemsChanged}
          ref={slidesRef}
        />
      </CalendarContainer>
      {timeData && timeData.length > 0 ? (
        <TimeSelector setSelectedTime={setSelectedTime} timeData={timeData} selectedTime={selectedTime} />
      ) : (
        !queryTimes.loading &&
        timeData && (
          <Body center mt={16}>
            Нет свободных слотов для записи {'\n'} в этот день
          </Body>
        )
      )}

      {!!selectedTime && <ConfirmButton onPress={onConfirm} selectedTime={selectedTime} />}
      <LoadingIndicator visible={queryTimes.loading || Boolean(recordUpdating)} />
    </ScreenContainer>
  )
}

const ScreenContainer = styled.View`
  height: 100%;
`
const CalendarContainer = styled.View`
  height: 328px;
`
