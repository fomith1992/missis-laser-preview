import React, { FC, ReactElement } from 'react'
import { TouchableOpacity } from 'react-native'
import styled from '@emotion/native'
import { color } from 'src/style/mixins/color'
import { DateText } from '@features/date-time-select/ui/day-button'
import { ITimeWithFormatting } from '@features/date-time-select/types'

interface Props {
  isSelected: boolean
  timeObj: ITimeWithFormatting
  setSelectedTime: () => void
}

export const TimeButton: FC<Props> = ({ isSelected, timeObj, setSelectedTime }): ReactElement => {
  return (
    <>
      <TouchableOpacity onPress={setSelectedTime}>
        <BorderCircle isActive={isSelected}>
          <DateText isActive={isSelected}>{timeObj.formattedTimeNoZero}</DateText>
        </BorderCircle>
      </TouchableOpacity>
    </>
  )
}

const BorderCircle = styled.View<{ isActive: boolean }>`
  ${(props) =>
    !props.isActive
      ? `border: 1px solid  ${color('primaryText')(props)}`
      : `border: 1px solid  ${color('primary')(props)}`};
  background-color: ${({ isActive }) => (isActive ? color('primary') : color('white'))};
  border-radius: 30px;
  padding: 8px 12px;
`
