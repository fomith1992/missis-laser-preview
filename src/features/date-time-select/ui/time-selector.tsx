import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { FlatList, StyleSheet, TouchableOpacity, View } from 'react-native'
import styled from '@emotion/native'
import { CalendarTitleContainer, DateTitle } from '@features/date-time-select/ui/calendar-title'
import { TimeButton } from '@features/date-time-select/ui/time-button'
import { ITimeWithFormatting } from '@features/date-time-select/types'
import { converTimeToDayTime, removeZero } from '@features/date-time-select/helpers/time-handlers'
import { TimesResponse } from '@shared/api/book'
import useDebounce from 'react-use/lib/useDebounce'

interface TimeSelectorProps {
  timeData: TimesResponse[]
  selectedTime: ITimeWithFormatting | null
  setSelectedTime: (time: ITimeWithFormatting) => void
}

export const TimeSelector = ({ timeData, selectedTime, setSelectedTime }: TimeSelectorProps): React.ReactElement => {
  const [selectedDayPart, setSelectedDayPart] = useState<string | null>(null)
  const [dayPartsParse, setDayPartsParse] = useState<string[] | null>(null)
  const sliderRef = React.useRef<FlatList>(null)
  const [debouncedValue, setDebouncedValue] = useState<string | null>(null)

  const arrOfFormattedTimeData = useMemo(
    () =>
      timeData.map((time) => {
        const formattedTimeNoZero = removeZero(time.time)
        const dayPart = converTimeToDayTime(time.datetime)
        return { ...time, formattedTimeNoZero, dayPart }
      }),
    [timeData],
  )

  useEffect(() => {
    const allDaysParts: string[] = []
    arrOfFormattedTimeData.map((x) => (x.dayPart ? allDaysParts.push(x.dayPart) : null))
    const allDaysPartsFiltered = [...new Set(allDaysParts)]
    setDayPartsParse(allDaysPartsFiltered)
    setSelectedDayPart(allDaysPartsFiltered[0])
  }, [arrOfFormattedTimeData])

  useDebounce(
    () => {
      debouncedValue && setSelectedDayPart(debouncedValue)
    },
    300,
    [debouncedValue],
  )

  const handleViewableItemsChanged = useCallback(({ viewableItems }) => {
    setSelectedDayPart(viewableItems[0].item.dayPart)
  }, [])

  const onDayPartPress = (dayPartPressed: string) => {
    const index = arrOfFormattedTimeData.findIndex(({ dayPart }) => dayPart === dayPartPressed)
    sliderRef?.current?.scrollToIndex({ index, viewPosition: 0 })
    setDebouncedValue(dayPartPressed)
  }

  return (
    <>
      <TimeSelectorTitleContainer>
        {dayPartsParse?.map((dayPart, index) => {
          return (
            <TouchableOpacity key={dayPart} onPress={() => onDayPartPress(dayPart)}>
              <DateTitle isActive={dayPart === selectedDayPart} isFirst={index === 0}>
                {dayPart}
              </DateTitle>
            </TouchableOpacity>
          )
        })}
      </TimeSelectorTitleContainer>
      <View style={styles.container}>
        <FlatList<ITimeWithFormatting>
          data={arrOfFormattedTimeData}
          renderItem={({ item }) => (
            <TimeButton
              isSelected={item.time === selectedTime?.time}
              timeObj={item}
              setSelectedTime={() => setSelectedTime(item)}
            />
          )}
          ItemSeparatorComponent={() => <Separator />}
          keyExtractor={(item) => item.datetime.toString()}
          showsHorizontalScrollIndicator={false}
          horizontal
          scrollEventThrottle={200}
          onViewableItemsChanged={handleViewableItemsChanged}
          ref={sliderRef}
        />
      </View>
    </>
  )
}

const TimeSelectorTitleContainer = styled(CalendarTitleContainer)`
  margin-top: 8px;
`
const Separator = styled.View`
  width: 12px;
`

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    height: 54,
  },
})
