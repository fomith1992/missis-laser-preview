import React from 'react'
import styled from '@emotion/native'
import { Footnote } from '@shared/ui'
import { Dimensions } from 'react-native'
import { getMonthName, TMonthObj } from '@features/date-time-select/helpers/calendar'
import { DateText, DayButton } from '@features/date-time-select/ui/day-button'
import { format, isBefore, isSameDay } from 'date-fns'
import { weekDayNames } from '@features/date-time-select/constants'

export interface CalendarProps {
  selectedDate: Date | null
  setSelectedDate: (date: Date | null) => void
  monthData: TMonthObj
  now: Date
}

const SCREEN_WIDTH = Dimensions.get('window').width

function WeekDaysNames(): React.ReactElement {
  return (
    <RowContainer>
      {weekDayNames.map((name) => (
        <Cell key={name}>
          <Footnote>{name}</Footnote>
        </Cell>
      ))}
    </RowContainer>
  )
}

export const Calendar = ({ now, selectedDate, setSelectedDate, monthData }: CalendarProps): React.ReactElement => {
  return (
    <CalendarContainer screenWidth={SCREEN_WIDTH}>
      <WeekDaysNames />
      {monthData.monthData.map((week, index) => (
        <RowContainer key={index}>
          {week.map((day, dayIndex) => {
            return (
              <Cell key={dayIndex}>
                {getMonthName(day.date) !== monthData.monthName ? (
                  <DateText>{''}</DateText>
                ) : (isBefore(day.date, now) && !isSameDay(day.date, now)) || !day.canAppoint ? (
                  <DateText disabled={true}>{format(day.date, 'dd')}</DateText>
                ) : (
                  <DayButton day={day.date} setSelectedDate={setSelectedDate} isActive={selectedDate === day.date} />
                )}
              </Cell>
            )
          })}
        </RowContainer>
      ))}
    </CalendarContainer>
  )
}

const Cell = styled.View`
  justify-content: center;
  align-items: center;
  flex: 1;
  width: 36px;
  height: 36px;
`

const CalendarContainer = styled.View<{ screenWidth: number }>`
  margin-top: 12px;
  width: ${(SCREEN_WIDTH - 16 * 2).toString()}px;
`
const RowContainer = styled.View`
  flex-direction: row;
  margin-top: 8px;
`
