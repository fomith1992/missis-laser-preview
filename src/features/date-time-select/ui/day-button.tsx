import React, { FC } from 'react'
import { format } from 'date-fns'
import styled from '@emotion/native'
import { Body } from '@shared/ui'
import { color } from 'src/style/mixins/color'
import { TouchableOpacity } from 'react-native'

interface DateButtonProps {
  day: Date
  setSelectedDate: (date: Date | null) => void
  isActive: boolean
}

export const DayButton: FC<DateButtonProps> = React.memo(({ day, setSelectedDate, isActive }) => {
  const formattedDay = format(day, 'dd')
  const onDayPress = () => setSelectedDate(day)

  return (
    <TouchableOpacity onPress={onDayPress}>
      <ActiveCircle isActive={isActive}>
        <DateText isActive={isActive}>{formattedDay}</DateText>
      </ActiveCircle>
    </TouchableOpacity>
  )
})

const ActiveCircle = styled.View<{ isActive: boolean }>`
  justify-content: center;
  align-items: center;
  background-color: ${({ isActive }) => (isActive ? color('primary') : color('white'))};
  border-radius: 30px;
  width: 36px;
  height: 36px;
`

export const DateText = styled(Body)<{
  disabled?: boolean
  isActive?: boolean
}>`
  color: ${({ disabled, isActive }) => {
    if (disabled) return color('tertiaryText')
    if (isActive) return color('white')
    return color('primaryText')
  }};
`
