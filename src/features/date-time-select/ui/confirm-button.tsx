import React from 'react'
import styled from '@emotion/native'
import { TextButton } from '@shared/ui/buttons/text-button.view'
import { ITimeWithFormatting } from '@features/date-time-select/types'
import { formatISOdate } from '@shared/lib/date'

interface Props {
  selectedTime: ITimeWithFormatting
  onPress: (date: string) => void
}

export const ConfirmButton: React.FC<Props> = ({ selectedTime, onPress }) => {
  const updatedText = formatISOdate(selectedTime.datetime.toString())
  return (
    <ButtonContainer>
      <TextButton onPress={() => onPress(selectedTime.datetime)}>{`Выбрать ${updatedText}`}</TextButton>
    </ButtonContainer>
  )
}

export const ButtonContainer = styled.View`
  width: 100%;
  position: absolute;
  bottom: 24px;
`
