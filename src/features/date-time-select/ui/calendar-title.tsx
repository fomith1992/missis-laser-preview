import React from 'react'
import { TouchableOpacity } from 'react-native'
import { TMonthsData } from '@features/date-time-select/helpers/calendar'
import styled from '@emotion/native'
import { Title } from '@shared/ui'
import { color } from 'src/style/mixins/color'

interface Props {
  monthsData: TMonthsData
  selectedMonth: string
  setSelectedMonth: (id: string) => void
  onPress: (index: number) => void
}

export const CalendarTitle: React.FC<Props> = ({ monthsData, selectedMonth, onPress }) => {
  return (
    <CalendarTitleContainer>
      {monthsData.map((monthObj, index) => {
        return (
          <TouchableOpacity key={monthObj.monthName} onPress={() => onPress(index)}>
            <DateTitle isActive={monthObj.monthName === selectedMonth} isFirst={index === 0}>
              {monthObj.monthName}
            </DateTitle>
          </TouchableOpacity>
        )
      })}
    </CalendarTitleContainer>
  )
}

export const CalendarTitleContainer = styled.View`
  justify-content: flex-start;
  align-items: center;
  flex-direction: row;
`

export const DateTitle = styled(Title)<{ isFirst: boolean; isActive: boolean }>`
  text-transform: capitalize;
  margin-left: ${({ isFirst }) => (isFirst ? '0' : '20px')};
  color: ${({ isActive }) => isActive && color('primary')};
`
