import { TSection } from './types'

export const mockDataZones: TSection[] = [
  {
    title: 'Лицо и шея',
    data: [],
  },
  {
    title: 'Руки',
    data: [],
  },
  {
    title: 'Туловище',
    data: [],
  },
  {
    title: 'Бикини',
    data: [],
  },
  {
    title: 'Ноги',
    data: [],
  },
  {
    title: 'Все тело',
    data: [],
  },
  {
    title: 'Другие',
    data: [],
  },
]
