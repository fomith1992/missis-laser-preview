import { TZoneIcons } from '@shared/ui/icons/zone.icon'

export type TZone = {
  categoryName: string
  image: TZoneIcons | null
  text: string
  price: number
  id: number
}

export type TSection = {
  title: string
  data: TZone[]
}

export type THeadersTitles = {
  text: string
  id: number
}

export type ViewToken = {
  item: TZone
  key: string
  index: number | null
  isViewable: boolean
  section?: TSection
}
