import React from 'react'
import { Platform, StyleSheet } from 'react-native'
import useAsync from 'react-use/lib/useAsync'

import { AxiosError } from 'axios'

import { InteractiveSectionList } from '@features/interactive-list'
import { serviceAPI } from '@shared/api'
import { TResponse } from '@shared/api/types'
import { ContentLayout, TextButton } from '@shared/ui'
import { LoadingIndicator } from '@shared/ui/atoms/loading-indicator'

import { theme } from 'src/style/theme'

import { useRootStore } from '../../stores/lib/use-root-store'
import { dataParse } from './lib/zones-list-data-parse'
import { TZone } from './model/types'
import { ZoneItemView } from './ui/zone-item.view'

type ZonesListProps = {
  onConfirm: (items: number[]) => void
}

const isHaveInCart = (cart: TZone[], item: TZone) => {
  return cart.some((v) => v.text === item.text)
}

export const ZonesList = ({ onConfirm }: ZonesListProps) => {
  const [cart, setCart] = React.useState<TZone[]>([])
  const { currentClinicId } = useRootStore().geolocationReport
  const { setServiceListData } = useRootStore().serviceListReport
  const { currentCategoryId } = useRootStore().serviceCategoryListReport
  const { user_token } = useRootStore().user

  const isIos = Platform.OS === 'ios'

  const buyHandler = (item: TZone) => {
    const isHave = isHaveInCart(cart, item)
    if (isHave) {
      setCart(cart.filter((v) => v.text !== item.text))
    } else {
      setCart([...cart, item])
    }
  }

  const curPrice = cart.reduce((a, { price }) => a + price, 0)

  const serviceList = useAsync(async () => {
    if (currentClinicId && currentCategoryId) {
      return await serviceAPI
        .getServiceList({
          categoryId: currentCategoryId,
          clinicId: currentClinicId,
          userToken: user_token ?? '',
        })
        .then(({ data }) => setServiceListData(data.data))
        .catch((e: AxiosError<TResponse>) => console.log('getCategory error ', e))
    }
  }, [currentClinicId, currentCategoryId])

  if (serviceList.loading) {
    return <LoadingIndicator visible />
  }

  return (
    <>
      <InteractiveSectionList
        tabbarItemActiveColor={theme.colors.primaryText}
        tabbarItemTitleActiveColor={theme.colors.white}
        tabbarItemTitleInactiveColor={theme.colors.secondaryText}
        data={dataParse(serviceList.value ?? [])}
        itemHeight={88}
        separatorHeight={20}
        renderItem={({ item }) => <ZoneItemView item={item} onPress={buyHandler} />}
      />
      <ContentLayout mt="auto" mb={isIos ? 32 : 0}>
        {curPrice !== 0 && (
          <TextButton style={styles.button} onPress={() => onConfirm(cart.map((x) => x.id))}>
            Далее ({curPrice} ₽)
          </TextButton>
        )}
      </ContentLayout>
    </>
  )
}

const styles = StyleSheet.create({
  button: {
    marginBottom: 24,
    alignItems: 'center',
    height: 44,
  },
})
