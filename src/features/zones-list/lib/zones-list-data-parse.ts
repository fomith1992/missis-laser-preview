import { TZoneIcons } from '@shared/ui/icons/zone.icon'

import { ServiceItemInstance } from 'src/stores/domains/service.store'

import { mockDataZones } from '../model/index'
import { TSection, TZone } from '../model/types'

const mapInstanceToSectionData = (item: ServiceItemInstance): TZone => ({
  categoryName: item.categoryName ? item.categoryName : 'Другие',
  image: item.imageType ? (item.imageType as TZoneIcons) : null,
  text: item.title,
  price: item.price_max,
  id: item.id,
})

export const dataParse = (list: ServiceItemInstance[] | void): TSection[] => {
  if (typeof list === 'object') {
    const sectionData = list.map(mapInstanceToSectionData)

    return mockDataZones
      .map(({ title }) => ({
        title,
        data: sectionData.filter((item) => item.categoryName === title),
      }))
      .filter(({ data }) => data.length)
  }

  return mockDataZones
}
