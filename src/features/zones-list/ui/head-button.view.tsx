import React, { FC } from 'react'

import { TouchableOpacity } from 'react-native'
import styled from '@emotion/native'

import { BaseView, ContentLayout, Footnote } from '@shared/ui'

type Props = {
  isActive: boolean
  text: string
  onPress(): void
}

export const HeadButton: FC<Props> = ({ isActive, text, onPress }) => {
  return (
    <ContentLayout>
      <TouchableOpacity onPress={onPress}>
        <Container backgroundColor={isActive ? 'black' : 'transparent'}>
          <Text color={isActive ? 'white' : undefined}>{text}</Text>
        </Container>
      </TouchableOpacity>
    </ContentLayout>
  )
}

const Container = styled(BaseView)<{ backgroundColor: string }>`
  height: 36px;
  padding: 8px 12px;
  border-radius: 30px;
`
const Text = styled(Footnote)`
  line-height: 16px;
`
