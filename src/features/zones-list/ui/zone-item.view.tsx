import React, { memo, useState } from 'react'
import { Image } from 'react-native'

import styled, { css } from '@emotion/native'

import ImagePlaceholder from '@assets/image-placeholder.png'
import { BaseView, Body, ColView, ContentLayout, RowView, ZoneIcon } from '@shared/ui'

import { theme } from 'src/style/theme'

import { TZone } from '../model/types'

interface Props {
  item: TZone
  onPress?: (item: TZone) => void
}

export const ZoneItemView = memo(({ item, onPress }: Props) => {
  const [isBought, setBought] = useState(false)

  const pressHandler = () => {
    setBought((value) => !value)

    if (onPress !== undefined) {
      onPress(item)
    }
  }

  return (
    <ContentLayout>
      <RowView>
        <ImageContainer>
          {item.image ? <ZoneIcon name={item.image} /> : <Image source={ImagePlaceholder} />}
        </ImageContainer>
        <ColView ml={16}>
          <Body color="primaryText">{item.text}</Body>
          <Button isActive={isBought} onPress={pressHandler}>
            <Body color={isBought ? 'lightText' : 'primaryText'}>{`${item.price} ₽`}</Body>
          </Button>
        </ColView>
      </RowView>
    </ContentLayout>
  )
})

const ImageContainer = styled(BaseView)`
  min-width: 88px;
  height: 88px;
  align-items: center;
  justify-content: center;
  border-radius: 8px;
  border: 1px solid rgba(233, 233, 233, 0.9);
`

const Button = styled.TouchableOpacity<{ isActive: boolean }>`
  margin-top: 8px;
  padding: 8px 12px;
  align-self: flex-start;
  border-radius: 30px;

  ${({ isActive }) => (isActive ? activeStyle : defaultStyle)};
`

const activeStyle = css`
  border: 1px solid transparent;
  background-color: ${theme.colors.primary};
`

const defaultStyle = css`
  border: 1px solid rgba(0, 0, 0, 0.9);
  background-color: transparent;
`
