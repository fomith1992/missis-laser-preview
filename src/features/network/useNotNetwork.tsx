import React, { useEffect, useRef } from 'react'
import BottomSheet from '@gorhom/bottom-sheet'

import { BottomSheetGorhom } from '@features/bottom-sheet-gorhom/bottom-sheet'

import { BottomSheetNetwork } from '@features/network/model'
import { NotNetworkBottomSheetView } from '@features/network/not-network.view'

export const useNotNetwork = () => {
  const sheetRef = useRef<BottomSheet>(null)

  useEffect(() => {
    const sheet = (
      <BottomSheetGorhom sheetRef={sheetRef}>
        <NotNetworkBottomSheetView onSubmit={() => sheetRef.current?.close()} />
      </BottomSheetGorhom>
    )

    BottomSheetNetwork.create(sheet)
  }, [])

  const open = () => {
    sheetRef.current?.expand()
  }

  return { open }
}
