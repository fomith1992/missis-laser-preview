import React from 'react'
import RootSiblings from 'react-native-root-siblings'

class bottomSheetNetwork {
  sibling: RootSiblings | null
  component: React.ReactChild | null
  constructor() {
    this.sibling = null
    this.component = null
  }

  destroy() {
    this.sibling?.destroy()
    this.sibling = null
  }

  create(comp: React.ReactChild | null) {
    this.component = comp
    if (this.component && !this.sibling) {
      this.sibling = new RootSiblings(this.component)
    }
  }
}

export const BottomSheetNetwork = new bottomSheetNetwork()
