import React from 'react'
import styled from '@emotion/native'

import { BaseView, Body, ColView, ContentLayout, Headiline, TextButton } from '@shared/ui'
import { color } from 'src/style/mixins/color'
import { SadIcon80 } from '@shared/ui/icons/sad.icon-80'
import { TouchableWithoutFeedback } from '@gorhom/bottom-sheet'
import { Platform } from 'react-native'

type InfoBottomSheetViewProps = {
  onSubmit: () => void
}

export function NotNetworkBottomSheetView({ onSubmit }: InfoBottomSheetViewProps) {
  const isIos = Platform.OS === 'ios'
  return (
    <ContentLayout>
      <IconWrapper mt={20}>
        <SadIcon />
      </IconWrapper>
      <Title mt={20}>Нет сети</Title>
      <Description mt={12}>
        Не получается соединиться с сервером. {'\n'} Пожалуйста, проверьте качество связи.
      </Description>
      <ButtonContainer mt={20} mb={24}>
        <TouchableWithoutFeedback onPress={!isIos ? onSubmit : undefined}>
          <TextButton onPress={isIos ? onSubmit : undefined}>Понятно</TextButton>
        </TouchableWithoutFeedback>
      </ButtonContainer>
    </ContentLayout>
  )
}

const IconWrapper = styled(ColView)`
  padding: 8px;
  align-self: center;
  border-radius: 48px;
  background-color: ${color('error15')};
`

const SadIcon = styled(SadIcon80)`
  width: 80px;
  height: 80px;
  color: ${color('error')};
`

const Description = styled(Body)`
  text-align: center;
`

const Title = styled(Headiline)`
  text-align: center;
`

const ButtonContainer = styled(BaseView)`
  flex: 1;
`
