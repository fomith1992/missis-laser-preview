import { StyleSheet } from 'react-native'

export const tabbarViewStyles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentContainer: {},
  iconView: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 40,
    height: 40,
  },
  icon: {
    width: 26,
    height: 26,
  },
})

export const tabbarItemStyles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  innerContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 8,
    paddingBottom: 8,
    paddingLeft: 12,
    paddingRight: 12,
    borderRadius: 30,
  },
  itemTitle: {
    fontSize: 18,
  },
})

export const contentViewStyles = StyleSheet.create({
  contentContainer: {
    backgroundColor: 'white',
  },
  border: {
    height: StyleSheet.hairlineWidth,
    backgroundColor: 'gray',
  },
})
