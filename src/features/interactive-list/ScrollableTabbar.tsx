/* eslint-disable react-hooks/exhaustive-deps  */
/* eslint-disable @typescript-eslint/no-explicit-any  */

import React, { useEffect, useState, useRef } from 'react'
import { View, Image, ScrollView, Dimensions, ImageSourcePropType, StyleSheet } from 'react-native'

import { TabbarItem } from './TabbarItem'

import { tabbarViewStyles as styles } from './styles'
import { ColView, ContentLayout } from '@shared/ui'
import styled from '@emotion/native'

export interface Props {
  items: Array<string>
  onPress: (index: number) => void
  selectedIndex: number
  firstValueInView: string
  isManualSelect: boolean
  scrollDirection: string
  itemWidth?: number
  itemHeight?: number
  itemSpaceBetween?: number
  activeColor?: string
  inactiveColor?: string
  titleActiveColor?: string
  titleInactiveColor?: string
  fontSize?: number
  icon?: ImageSourcePropType
  isScrolling?: boolean
}

type positionType = {
  item: string
  x: number
  width: number
  index: number
}

const SCREEN_WIDTH = Dimensions.get('window').width

export const ScrollableTabbar: React.FC<Props> = ({
  items,
  onPress,
  selectedIndex,
  firstValueInView,
  isManualSelect,
  scrollDirection,
  itemWidth = 100,
  itemHeight = 48,
  itemSpaceBetween = 8,
  activeColor = 'yellow',
  inactiveColor = 'transparent',
  titleActiveColor = '#000000',
  titleInactiveColor = '#000000',
  fontSize = 14,
  icon,
  isScrolling,
}) => {
  const scrollViewRef = useRef<any>(null)
  const [itemPositions, setItemPositions] = useState<Array<positionType>>([])

  const getValues = () => {
    const onTitlePressHandler = (index: number) => {
      onPress(index)
      if (scrollViewRef && scrollViewRef.current) {
        scrollViewRef.current.scrollTo({ x: index * itemWidth })
      }
    }

    return items.map((item, index) => (
      <TabbarItem
        key={index}
        item={item}
        selected={index === selectedIndex}
        isFirstInView={firstValueInView === item}
        isManualSelect={isManualSelect}
        onPress={() => onTitlePressHandler(index)}
        onLayout={({ nativeEvent }) => {
          const positions = [...itemPositions]

          positions.push({
            item,
            x: nativeEvent.layout.x,
            width: nativeEvent.layout.width,
            index: index,
          })
          setItemPositions(positions)
        }}
        itemWidth={itemWidth}
        itemHeight={itemHeight}
        activeColor={activeColor}
        inactiveColor={inactiveColor}
        titleActiveColor={titleActiveColor}
        titleInactiveColor={titleInactiveColor}
        fontSize={fontSize}
      />
    ))
  }

  useEffect(() => {
    if (isManualSelect) return

    if (scrollViewRef && scrollViewRef.current) {
      itemPositions.forEach((itemPosition: positionType) => {
        if (
          firstValueInView === itemPosition.item &&
          scrollDirection === 'DOWN' &&
          itemPosition.x > SCREEN_WIDTH - itemWidth * 2
        ) {
          scrollViewRef.current.scrollTo({
            x: itemPosition.x - SCREEN_WIDTH + itemWidth * 2,
            animated: true,
          })
        } else if (firstValueInView === itemPosition.item && scrollDirection === 'UP') {
          scrollViewRef.current.scrollTo({
            x: itemPosition.x - itemWidth,
            animated: true,
          })
        }
      })
    }
  }, [firstValueInView])

  return (
    <ShadowContainer>
      <ColView style={isScrolling ? st.shadow : undefined}>
        <ContentLayout mb={16}>
          <View style={styles.container}>
            {icon && (
              <View style={styles.iconView}>
                <Image source={icon} style={styles.icon} />
              </View>
            )}
            <ScrollView
              horizontal
              ref={scrollViewRef}
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={styles.contentContainer}
              decelerationRate="fast"
              snapToInterval={itemWidth + itemSpaceBetween}
              snapToAlignment="center"
            >
              {getValues()}
            </ScrollView>
          </View>
        </ContentLayout>
      </ColView>
    </ShadowContainer>
  )
}

const ShadowContainer = styled.View`
  overflow: hidden;
  padding-bottom: 2px;
`

const st = StyleSheet.create({
  shadow: {
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.4,
    shadowRadius: 3,
    elevation: 5,
  },
  space: {
    marginBottom: 16,
  },
})
