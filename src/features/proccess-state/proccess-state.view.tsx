import styled from '@emotion/native'
import { BaseView, Body, ContentLayout, Headiline } from '@shared/ui'
import { TextButton } from '@shared/ui/buttons/text-button.view'
import { DoneIcon32 } from '@shared/ui/icons/done.icon-32'
import React from 'react'
import { color } from 'src/style/mixins/color'

interface ProccessStateViewProps {
  title: string
  description: string
  buttonText?: string
  onPress?: () => void
  SvgComponent?: React.ReactNode
  noButton?: boolean
}

export function ProccessStateView({
  title,
  description,
  buttonText,
  onPress,
  SvgComponent = <DoneSvg />,
  noButton,
}: ProccessStateViewProps) {
  return (
    <ContentLayout flex>
      <BodyContainer>
        <IconContainer>{SvgComponent}</IconContainer>
        <Headiline mt={20}>{title}</Headiline>
        <Description mt={8}>{description}</Description>
      </BodyContainer>
      {noButton ? null : (
        <BottomButtonContainer>
          <TextButton onPress={onPress}>{buttonText}</TextButton>
        </BottomButtonContainer>
      )}
    </ContentLayout>
  )
}

const BodyContainer = styled(BaseView)`
  flex: 1;
  justify-content: center;
  align-items: center;
`

const IconContainer = styled.View`
  width: 104px;
  height: 104px;
  border-radius: 52px;
  overflow: hidden;
  background-color: ${color('primaryLight')};
  justify-content: center;
  align-items: center;
`

const BottomButtonContainer = styled.View`
  margin-bottom: 24px;
  justify-content: center;
`

const DoneSvg = styled(DoneIcon32)`
  width: 64px;
  height: 64px;
  color: ${color('primary')};
`

const Description = styled(Body)`
  text-align: center;
`
