import React from 'react'

import { BottomSheetHandleProps } from '@gorhom/bottom-sheet'
import { ColView, RowView } from '@shared/ui'
import { color } from 'src/style/mixins/color'
import styled from '@emotion/native'

export const CustomHandle: React.FC<BottomSheetHandleProps> = () => {
  return (
    <Container>
      <Line />
    </Container>
  )
}

const Container = styled(RowView)`
  top: 1px;
  height: 36px;
  flex-shrink: 0;
  border-radius: 32px 32px 0px 0px;
  background-color: ${color('background')};
  align-items: center;
  justify-content: center;
`

const Line = styled(ColView)`
  height: 4px;
  width: 40px;
  border-radius: 4px;
  background-color: ${color('tertiaryText')};
`
