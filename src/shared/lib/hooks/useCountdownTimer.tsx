import { useEffect, useState } from 'react'

interface ICountdownTimerProps {
  seconds: number
  onExpire?: () => void
}

export const useCountdownTimer = ({ seconds, onExpire }: ICountdownTimerProps) => {
  const [value, setValue] = useState(seconds)
  const [isExpired, setExpired] = useState(false)
  const [isStoped, setStoped] = useState(false)

  const stop = () => {
    setStoped(true)
    setValue(0)
  }

  const reset = () => {
    setValue(seconds)
    setStoped(false)
    setExpired(false)
  }

  useEffect(() => {
    function tick() {
      if (value <= 0) {
        setStoped(true)
        setExpired(true)
        if (onExpire && typeof onExpire === 'function') {
          onExpire()
        }
      } else {
        setValue((v) => v - 1)
      }
    }

    let id: NodeJS.Timer
    if (!isStoped) {
      id = setInterval(tick, 1000)
    }

    return () => clearInterval(id)
  }, [value, onExpire, isStoped])

  return {
    value,
    stop,
    reset,
    expired: isExpired,
  }
}
