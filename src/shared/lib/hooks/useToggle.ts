import { useState } from 'react'

export function useToggle(initialState: boolean) {
  const [bool, setBool] = useState(initialState)

  const toggle = () => setBool((current) => !current)

  return [bool, toggle] as const
}
