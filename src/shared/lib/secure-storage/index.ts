import * as secureStore from 'expo-secure-store'
import * as SECURE_STORE_KEYS from './keys'

type SecureStoreOptions = secureStore.SecureStoreOptions

async function get<T>(key: string, options?: SecureStoreOptions): Promise<T | null> {
  try {
    const value = await secureStore.getItemAsync(key, options)
    if (value === null) return null
    return JSON.parse(value)
  } catch (error) {
    console.log(`secure-store: [${key}] not found. `, +error)
    return null
  }
}

async function set<T extends unknown>(key: string, value: T, options?: SecureStoreOptions) {
  await secureStore.setItemAsync(key, JSON.stringify(value), options)
}

async function remove(key: string, options?: SecureStoreOptions) {
  await secureStore.deleteItemAsync(key, options)
}

async function getSecureStoreAsync() {
  const canUse = await secureStore.isAvailableAsync()
  if (!canUse) return null
  return { get, set, remove }
}

export { getSecureStoreAsync, SECURE_STORE_KEYS }
