import { format, parseISO } from 'date-fns'
import ru from 'date-fns/locale/ru'

export const formatISOdate = (date: string) => {
  const dayOfWeekRegex = /\sпн\s|\sвт\s|\sср\s|\sчт\s|\sпт\s|\sсб\s|\sвс\s/gm

  const formattedDate = format(parseISO(date.slice(0, -5)), 'd MMMM EEEEEE k:mm', {
    locale: ru,
  })
  const dayOfWeek = formattedDate.match(dayOfWeekRegex)

  if (dayOfWeek) return formattedDate.replace(dayOfWeekRegex, dayOfWeek[0].toUpperCase())
  return formattedDate
}
