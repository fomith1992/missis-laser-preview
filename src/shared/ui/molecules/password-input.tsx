import React, { useState } from 'react'

import { TextInput, TextInputProps } from '../atoms/text-input'
import { EyeHide20 } from '../icons/eye-hide.icon-20'
import { EyeShow20 } from '../icons/eye-show.icon-20'

type PasswordInputProps = Omit<TextInputProps, 'RightIcon' | 'secureTextEntry'>

export const PasswordInput = (props: PasswordInputProps) => {
  const [showPassword, setShowPassword] = useState(false)

  return (
    <TextInput
      secureTextEntry={!showPassword}
      RightIcon={showPassword ? EyeHide20 : EyeShow20}
      onRightIconPress={() => setShowPassword(!showPassword)}
      {...props}
    />
  )
}
