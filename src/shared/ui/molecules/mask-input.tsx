import React, { useEffect, useRef, useState } from 'react'
import { TextInputMask, TextInputMaskProps } from 'react-native-masked-text'
import styled from '@emotion/native'

import { TextInputProps, Container } from '../atoms/text-input'

import { font } from 'src/style/mixins/font'
import { theme } from 'src/style/theme'
import { Body, ColView } from '../atoms'
import { NativeSyntheticEvent, StyleSheet, TextInput, TextInputFocusEventData } from 'react-native'

type BaseProps = TextInputProps & TextInputMaskProps

export type MaskInputProps = BaseProps & {
  prefix?: string
  focusOnMount?: boolean
}

export const MaskInput = ({
  errorMessage,
  onBlur,
  onFocus,
  focusOnMount,
  editable = true,
  prefix,
  mb,
  ml,
  mr,
  mt,
  pb,
  pl,
  pr,
  pt,
  ...rest
}: MaskInputProps) => {
  const [isFocused, setFocused] = useState(false)

  const inputRef = useRef<TextInputMask & { _inputElement: TextInput }>(null)

  const blurHandler = (e: NativeSyntheticEvent<TextInputFocusEventData>) => {
    setFocused(false)
    onBlur ? onBlur(e) : null
  }

  const focusHandler = (e: NativeSyntheticEvent<TextInputFocusEventData>) => {
    setFocused(true)
    onFocus ? onFocus(e) : null
  }

  useEffect(() => {
    if (inputRef.current && focusOnMount) {
      setTimeout(() => {
        inputRef.current?._inputElement?.focus()
      }, 200)
    }
  }, [focusOnMount])

  return (
    <MaskContainer isError={errorMessage != null} focused={isFocused} {...{ mb, ml, mr, mt, pb, pl, pr, pt }}>
      {prefix && (
        <ColView mr={8}>
          <Body color={editable ? 'primaryText' : 'tertiaryText'}>{prefix}</Body>
        </ColView>
      )}
      <TextInputMaskStyled
        ref={inputRef}
        style={editable ? undefined : styles.noEditable}
        onFocus={focusHandler}
        onBlur={blurHandler}
        selectionColor={theme.colors.selectionColor}
        editable={editable}
        {...rest}
      />
    </MaskContainer>
  )
}

const MaskContainer = styled(Container)`
  flex-direction: row;
  align-items: center;
`

const TextInputMaskStyled = styled(TextInputMask)`
  height: 19px;
  flex: 1;
  align-items: center;
  ${font({ variant: 'Body', color: 'primaryText' })};
`

const styles = StyleSheet.create({
  noEditable: {
    color: theme.colors.tertiaryText,
  },
})
