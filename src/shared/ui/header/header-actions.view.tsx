import styled from '@emotion/native'
import React from 'react'
import { BaseView } from '../atoms'
import { LocationIcon24 } from '../icons/location.icon-24'
import { SettingsIcon24 } from '../icons/settings.icon-24'
import { PromoIcon24 } from '@shared/ui/icons/promo.icon-24'

export interface HeaderActionsViewProps {
  actions?: HeaderActionProps[]
}

export type HeaderIconType = 'pointOnMap' | 'settings' | 'promo'

export interface HeaderActionProps {
  icon: HeaderIconType
  disabled?: boolean
  onPress?: () => void
}

function renderHeaderIcon(icon: HeaderIconType): React.ReactElement {
  switch (icon) {
    case 'pointOnMap':
      return <LocationIcon24 />
    case 'settings':
      return <SettingsIcon24 />
    case 'promo':
      return <PromoIcon24 />
  }
}

export function HeaderActionsView({ actions }: HeaderActionsViewProps): React.ReactElement {
  return (
    <ActionsContainer>
      {actions?.map(({ icon, onPress, disabled }, index) => (
        <ActionButton disabled={disabled} key={index} onPress={onPress}>
          {renderHeaderIcon(icon)}
        </ActionButton>
      ))}
    </ActionsContainer>
  )
}

const ActionsContainer = styled(BaseView)`
  margin-left: auto;
  flex-direction: row;
`

const ActionButton = styled.TouchableOpacity`
  align-items: center;
  flex-direction: row;
  padding: 16px;
`
