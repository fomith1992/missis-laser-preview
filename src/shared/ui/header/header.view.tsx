import styled from '@emotion/native'
import React from 'react'
import { SafeAreaView } from 'react-native-safe-area-context'
import { BaseView, Headiline } from '../atoms'
import { BackIcon24 } from '../icons/back.icon-24'
import { HeaderActionProps, HeaderActionsView } from './header-actions.view'

interface HeaderViewProps {
  title: string
  actions?: HeaderActionProps[]
  headerLeft?: React.ReactChild | null
  goBack?: () => void
}

export function HeaderView({ title, actions, goBack, headerLeft }: HeaderViewProps): React.ReactElement {
  return (
    <SafeAreaView edges={['top']}>
      <HeaderFirstLine>
        {goBack == null ? null : (
          <GoBackContainer onPress={goBack}>
            <BackIcon24 />
          </GoBackContainer>
        )}
        {headerLeft}
        <HeaderTitleContainer indentLeft={goBack == null}>
          <Headiline>{title}</Headiline>
        </HeaderTitleContainer>
        <HeaderActionsView actions={actions} />
      </HeaderFirstLine>
    </SafeAreaView>
  )
}

const HeaderFirstLine = styled(BaseView)`
  align-items: center;
  flex-direction: row;
  height: 56px;
`
const GoBackContainer = styled.TouchableOpacity`
  padding: 16px;
`
const HeaderTitleContainer = styled.View<{ indentLeft: boolean }>`
  flex: 1;
  margin-left: ${({ indentLeft }) => (indentLeft ? '16px' : '0')};
`
