import { StackActions } from '@react-navigation/native'
import { StackHeaderProps } from '@react-navigation/stack'
import React from 'react'
import { HeaderActionProps } from './header-actions.view'
import { HeaderView } from './header.view'

interface HeaderProps {
  headerProps: StackHeaderProps
  actions?: HeaderActionProps[]
  forceShowBack?: boolean
  headerLeft?: React.ReactChild | null
  renderTitle?: (title: string) => React.ReactNode
  goBackCustom?: () => void
}

export function Header({
  headerProps: { progress, options, navigation, route },
  actions,
  headerLeft,
  forceShowBack = false,
  goBackCustom,
}: HeaderProps): React.ReactElement {
  const title = typeof options.headerTitle === 'string' ? options.headerTitle : options.title ?? route.name

  const goBack = React.useCallback(() => {
    if (goBackCustom != null) {
      goBackCustom()
      return
    }
    if (navigation.isFocused() && navigation.canGoBack()) {
      navigation.dispatch({
        ...StackActions.pop(),
        source: route.key,
      })
    }
  }, [navigation, route.key, goBackCustom])

  return (
    <HeaderView
      title={title}
      actions={actions}
      goBack={progress.previous == null && !forceShowBack ? undefined : goBack}
      headerLeft={headerLeft}
    />
  )
}
