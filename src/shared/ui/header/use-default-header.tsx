import { ParamListBase, useNavigation } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack'
import React, { useLayoutEffect } from 'react'
import { Header } from './header'
import { HeaderActionProps } from './header-actions.view'

export interface DefaultHeaderOptions {
  title?: string
  actions?: HeaderActionProps[]
  headerLeft?: React.ReactChild | null
  goBackCustom?: () => void
}

export function useDefaultHeader({ title, actions, headerLeft, goBackCustom }: DefaultHeaderOptions): void {
  const navigation = useNavigation<StackNavigationProp<ParamListBase>>()
  useLayoutEffect(() => {
    navigation.setOptions({
      ...(title == null ? {} : { title }),
      header: (props) => (
        <Header goBackCustom={goBackCustom} headerProps={props} actions={actions} headerLeft={headerLeft} />
      ),
    })
  }, [navigation, title, actions, headerLeft, goBackCustom])
}
