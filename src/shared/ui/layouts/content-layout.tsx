import React from 'react'
import { ViewProps } from 'react-native'
import styled, { css } from '@emotion/native'

import { BaseView } from '../atoms'

import { SpaceProps } from 'src/style/mixins/space'
import { IfThen } from 'src/style/lib/IfThen'

type OptionalProps = Partial<{
  flex: boolean
}>

type ContentLayoutProps = SpaceProps &
  ViewProps &
  OptionalProps & {
    children: React.ReactNode
  }

export function ContentLayout(props: ContentLayoutProps) {
  return <Container {...props} />
}

const Container = styled(BaseView)<OptionalProps>`
  padding: 0 16px;

  ${IfThen<OptionalProps>('flex', css({ flex: 1 }))}
`
