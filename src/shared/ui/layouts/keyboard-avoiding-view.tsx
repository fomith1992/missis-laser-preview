import React from 'react'
import { KeyboardAvoidingView as KeyboardAvoidingViewLib, Platform } from 'react-native'
import { SafeAreaView } from './safe-area-view'

export const KeyboardAvoidingViews = (props: { children: React.ReactElement }) => {
  return (
    <KeyboardAvoidingViewLib
      style={{ flex: 1 }}
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      keyboardVerticalOffset={56}
    >
      <SafeAreaView>{props.children}</SafeAreaView>
    </KeyboardAvoidingViewLib>
  )
}
