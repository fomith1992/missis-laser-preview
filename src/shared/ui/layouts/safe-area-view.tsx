import React from 'react'
import { StyleSheet } from 'react-native'

import { Edge, SafeAreaView as SafeAreaViewLib } from 'react-native-safe-area-context'

type SafeAreaViewProps = {
  children: React.ReactNode
  topOff?: boolean
}

const withoutTop: ReadonlyArray<Edge> = ['right', 'bottom', 'left']

export const SafeAreaView = ({ topOff, ...rest }: SafeAreaViewProps) => {
  return <SafeAreaViewLib style={styles.safeArea} edges={topOff ? withoutTop : undefined} {...rest} />
}

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
  },
})
