import React from 'react'
import { Animated, StyleSheet } from 'react-native'
import styled from '@emotion/native'
import { BackIcon24 } from '../icons/back.icon-24'
import { color } from 'src/style/mixins/color'
import { SafeAreaView } from './safe-area-view'
import { BaseView } from '../atoms'

const AnimatedView = Animated.View

type LayoutWithBackButtonProps = {
  children: JSX.Element | JSX.Element[]
  onPress: () => void
}

export const LayoutWithBackButton = ({ children, onPress }: LayoutWithBackButtonProps) => {
  return (
    <SafeAreaView>
      <ModalContainer pointerEvents="auto">
        <SafeAreaView>
          <BackButtonWrapper onPress={onPress}>
            <BackIcon24 />
          </BackButtonWrapper>
        </SafeAreaView>
      </ModalContainer>
      <BaseView style={styles.flex}>{children}</BaseView>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
})

const ModalContainer = styled(AnimatedView)`
  position: absolute;
  top: 0;
  left: 0;
  height: 90px;
  width: 60px;
  z-index: 100;
`

const BackButtonWrapper = styled.TouchableOpacity`
  margin-top: 8px;
  margin-left: 4px;
  width: 48px;
  height: 48px;
  background-color: ${color('white')};
  border-radius: 24px;
  align-items: center;
  justify-content: center;
`
