import * as React from 'react'
import Svg, { Path, Rect } from 'react-native-svg'
import { StyleSheet } from 'react-native'
import { IconComponentProps } from './icon-component'

export function BlockedIcon64({ style, ...rest }: IconComponentProps): React.ReactElement {
  const { color = '#000000' } = StyleSheet.flatten(style) ?? {}

  return (
    <Svg style={style} width={64} height={64} viewBox="0 0 64 64" fill="none" {...rest}>
      <Path d="M12.3533 52.8964L53 12" stroke={color} strokeWidth="4.26667" />
      <Rect x="2" y="2" width="60" height="60" rx="30" stroke={color} strokeWidth="4" />
    </Svg>
  )
}
