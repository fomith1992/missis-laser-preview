import * as React from 'react'
import { StyleSheet } from 'react-native'
import Svg, { Path } from 'react-native-svg'
import { IconComponentProps } from './icon-component'

export function PromoIcon24({ style, ...rest }: IconComponentProps): React.ReactElement {
  const { width = 24, height = 24, color = '#000' } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg width={width} height={height} viewBox="0 0 24 24" fill="none" {...rest}>
      <Path
        fill={color}
        fillOpacity="0.9"
        fillRule="evenodd"
        d="M2.438 18.123c0 .948.73 1.65 1.549 1.65h16.989c.818 0 1.548-.702 1.548-1.65v-1.299a4.949 4.949 0 010-8.96v-2.11c0-.948-.73-1.65-1.548-1.65H3.986c-.818 0-1.548.702-1.548 1.65v2.111a4.949 4.949 0 010 8.959v1.299zM24.524 7.398V5.755c0-1.98-1.552-3.65-3.548-3.65H3.986c-1.996 0-3.548 1.67-3.548 3.65v1.643l-.104-.001v2.458a2.49 2.49 0 010 4.98v2.457h.104v.83c0 1.98 1.552 3.65 3.549 3.65h16.989c1.996 0 3.548-1.67 3.548-3.65v-.83h.104v-2.458a2.49 2.49 0 110-4.98V7.398h-.104zM6.96 9.468a1 1 0 011-1h9.043a1 1 0 110 2H7.96a1 1 0 01-1-1zm1 4.521a1 1 0 100 2h9.043a1 1 0 100-2H7.96z"
        clipRule="evenodd"
      />
    </Svg>
  )
}
