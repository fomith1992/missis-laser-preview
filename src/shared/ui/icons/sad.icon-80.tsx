import * as React from 'react'
import { StyleSheet } from 'react-native'
import Svg, { Path } from 'react-native-svg'
import { IconComponentProps } from './icon-component'

export function SadIcon80({ style, ...rest }: IconComponentProps): React.ReactElement {
  const { width = 80, height = 80, color = '#000' } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg width={width} height={height} viewBox="0 0 80 80" fill="none" {...rest}>
      <Path
        d="M30 38C32.2091 38 34 36.2091 34 34C34 31.7909 32.2091 30 30 30C27.7909 30 26 31.7909 26 34C26 36.2091 27.7909 38 30 38Z"
        fill={color}
      />
      <Path
        d="M30 53.9999C36 47 44 47 50 53.9999"
        stroke={color}
        strokeWidth="4"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path
        d="M50 38C52.2091 38 54 36.2091 54 34C54 31.7909 52.2091 30 50 30C47.7909 30 46 31.7909 46 34C46 36.2091 47.7909 38 50 38Z"
        fill={color}
      />
      <Path
        d="M40 70C56.5685 70 70 56.5685 70 40C70 23.4315 56.5685 10 40 10C23.4315 10 10 23.4315 10 40C10 56.5685 23.4315 70 40 70Z"
        stroke={color}
        strokeWidth="4"
        strokeMiterlimit="10"
      />
    </Svg>
  )
}
