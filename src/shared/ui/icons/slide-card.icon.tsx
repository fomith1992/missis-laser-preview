import * as React from 'react'
import { StyleSheet } from 'react-native'
import Svg, { G, Path } from 'react-native-svg'
import { IconComponentProps } from './icon-component'

export function SlideCardIcon24({ style, ...rest }: IconComponentProps): React.ReactElement {
  const { width = 108, height = 199, color = '#000' } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg width={width} height={height} viewBox="0 0 160 181" fill="none" {...rest}>
      <G
        transform="translate(0.000000,181.000000) scale(0.100000,-0.100000)"
        fill={color}
        stroke="none"
        strokeWidth={0.2}
      >
        <Path
          d="M737 1792 c-37 -16 -59 -51 -383 -614 -371 -644 -366 -633 -327 -706
15 -26 83 -70 383 -243 353 -203 367 -210 411 -206 26 2 60 13 76 25 20 14
132 200 366 606 359 623 355 614 323 689 -12 29 -59 60 -390 250 -207 119
-386 217 -399 217 -12 0 -39 -8 -60 -18z m118 -64 c14 -6 24 -15 23 -19 -10
-27 -724 -1259 -730 -1259 -4 0 -22 13 -40 29 -62 55 -67 44 295 670 208 362
334 569 349 577 29 17 70 17 103 2z m194 -114 c3 -2 -679 -1193 -718 -1252 -6
-11 -20 -7 -65 18 -51 29 -55 34 -44 53 17 32 688 1198 704 1224 l14 22 53
-30 c28 -17 54 -33 56 -35z m305 -174 c82 -47 155 -95 163 -107 34 -52 29 -64
-307 -648 -177 -308 -331 -568 -342 -578 -44 -38 -83 -25 -292 95 l-195 113
19 30 c10 16 172 298 360 625 188 327 344 598 347 602 2 4 25 -5 52 -20 26
-15 114 -65 195 -112z"
        />
        <Path
          d="M1027 1294 c-17 -17 1 -59 91 -219 98 -172 119 -195 145 -154 7 12
-13 57 -90 195 -96 172 -121 203 -146 178z"
        />
        <Path
          d="M1280 1289 c-61 -24 -84 -82 -56 -137 20 -38 44 -52 91 -52 50 0 95
44 95 94 0 65 -72 117 -130 95z m58 -90 c4 -30 -22 -46 -44 -28 -17 14 -18 46
-1 52 25 11 42 1 45 -24z"
        />
        <Path
          d="M924 1091 c-38 -23 -59 -76 -45 -113 14 -37 58 -68 98 -68 103 0 128
155 31 189 -42 15 -48 14 -84 -8z m75 -65 c6 -7 9 -20 5 -29 -15 -39 -72 -19
-60 21 8 25 38 29 55 8z"
        />
        <Path
          d="M871 499 c-66 -116 -121 -217 -121 -224 0 -18 25 -36 42 -30 7 3 69
102 137 220 102 178 121 218 111 230 -30 36 -46 17 -169 -196z"
        />
        <Path
          d="M757 563 c-13 -21 -48 -80 -77 -131 -49 -87 -52 -94 -36 -110 26 -26
44 -8 118 119 70 118 79 152 41 157 -16 2 -28 -7 -46 -35z"
        />
      </G>
    </Svg>
  )
}
