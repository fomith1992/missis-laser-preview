import React from 'react'
import * as Icons from './zones'

const IconsObject = {
  ['faceSmallZone']: Icons.ZoneFaceSmallZone,
  ['faceFaceFull']: Icons.ZoneFaceFaceFull,
  ['faceForeheadOrCheeks']: Icons.ZoneFaceForeheadOrCheeks,
  ['faceTempleOrSideburns']: Icons.ZoneFaceTempleOrSideburns,
  ['faceNeck']: Icons.ZoneFaceNeck,

  ['handsAboveElbow']: Icons.ZoneHandsAboveElbow,
  ['handsArmpit']: Icons.ZoneHandsArmpit,
  ['handsFingers']: Icons.ZoneHandsFingers,
  ['handsHandsFull']: Icons.ZoneHandsHandsFull,
  ['handsShoulders']: Icons.ZoneHandsShoulders,
  ['handsUpToElbow']: Icons.ZoneHandsUpToElbow,
  ['handsWrist']: Icons.ZoneHandsWrist,

  ['torsAreols']: Icons.ZoneTorsAreols,
  ['torsBackFull']: Icons.ZoneTorsBackFull,
  ['torsBackLowerThird']: Icons.ZoneTorsBackLowerThird,
  ['torsChestFull']: Icons.ZoneTorsChestFull,
  ['torsNeckline']: Icons.ZoneTorsNeckline,
  ['torsStomachFull']: Icons.ZoneTorsStomachFull,
  ['torsStomachWhiteLine']: Icons.ZoneTorsStomachWhiteLine,

  ['bikiniBikiniClassic']: Icons.ZoneBikiniBikiniClassic,
  ['bikiniBikiniDeep']: Icons.ZoneBikiniBikiniDeep,
  ['bikiniButtocks']: Icons.ZoneBikiniButtocks,
  ['bikiniInterBerryZone']: Icons.ZoneBikiniInterBerryZone,

  ['footsHips']: Icons.ZoneFootsHips,
  ['footsHipsFull']: Icons.ZoneFootsHipsFull,
  ['footsKnees']: Icons.ZoneFootsKnees,
  ['footsLegsFull']: Icons.ZoneFootsLegsFull,
  ['footsRaisingLegsWithToes']: Icons.ZoneFootsRaisingLegsWithToes,
  ['footsRaisingTheLegs']: Icons.ZoneFootsRaisingTheLegs,
  ['footsSheensWithKnees']: Icons.ZoneFootsSheensWithKnees,
  ['footsToes']: Icons.ZoneFootsToes,

  ['bodyBodyFull']: Icons.ZoneBodyBodyFull,
  ['bodyBodyFullAndHands']: Icons.ZoneBodyBodyFullAndHands,
}

export type TZoneIcons = keyof typeof IconsObject

export const ZoneIcon = ({ name }: { name: TZoneIcons }) => {
  const Component = IconsObject[name]
  return <Component />
}
