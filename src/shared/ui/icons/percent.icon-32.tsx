import * as React from 'react'
import { StyleSheet } from 'react-native'
import Svg, { Line, Path } from 'react-native-svg'
import { IconComponentProps } from './icon-component'

export function PercentIcon32({ style, ...rest }: IconComponentProps): React.ReactElement {
  const { width = 32, height = 32, color = '#000' } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg width={width} height={height} viewBox="0 0 32 32" fill="none" {...rest}>
      <Line
        x1="6.46661"
        y1="26.2979"
        x2="26.3857"
        y2="6.37882"
        stroke={color}
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path
        d="M10.6666 13.3333C12.8758 13.3333 14.6666 11.5424 14.6666 9.33325C14.6666 7.12411 12.8758 5.33325 10.6666 5.33325C8.45749 5.33325 6.66663 7.12411 6.66663 9.33325C6.66663 11.5424 8.45749 13.3333 10.6666 13.3333Z"
        stroke={color}
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path
        d="M22.6666 26.6665C24.8758 26.6665 26.6666 24.8756 26.6666 22.6665C26.6666 20.4574 24.8758 18.6665 22.6666 18.6665C20.4575 18.6665 18.6666 20.4574 18.6666 22.6665C18.6666 24.8756 20.4575 26.6665 22.6666 26.6665Z"
        stroke={color}
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  )
}
