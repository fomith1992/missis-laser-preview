import * as React from 'react'
import { StyleSheet } from 'react-native'
import Svg, { Path } from 'react-native-svg'
import { IconComponentProps } from './icon-component'

export function CloseIcon32({ style, ...rest }: IconComponentProps): React.ReactElement {
  const { width = 32, height = 32, color = '#000' } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg width={width} height={height} viewBox="0 0 32 32" fill="none" {...rest}>
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M3.70711 2.29289C3.31658 1.90237 2.68342 1.90237 2.29289 2.29289C1.90237 2.68342 1.90237 3.31658 2.29289 3.70711L14.612 16.0262L2.29289 28.3453C1.90237 28.7358 1.90237 29.369 2.29289 29.7595C2.68342 30.1501 3.31658 30.1501 3.70711 29.7595L16.0262 17.4404L28.3453 29.7595C28.7358 30.1501 29.369 30.1501 29.7595 29.7595C30.1501 29.369 30.1501 28.7358 29.7595 28.3453L17.4404 16.0262L29.7595 3.70711C30.1501 3.31658 30.1501 2.68342 29.7595 2.29289C29.369 1.90237 28.7358 1.90237 28.3453 2.29289L16.0262 14.612L3.70711 2.29289Z"
        fill={color}
        fillOpacity="0.9"
      />
    </Svg>
  )
}
