import * as React from 'react'
import { StyleSheet } from 'react-native'
import Svg, { Path } from 'react-native-svg'
import { IconComponentProps } from './icon-component'

export function RefreshIcon20({ style, ...rest }: IconComponentProps): React.ReactElement {
  const { width = 20, height = 20, color = '#000' } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg width={width} height={height} viewBox="0 0 20 20" fill="none" {...rest}>
      <Path
        fillOpacity="0.9"
        fillRule="evenodd"
        d="M10.702.96a1 1 0 00-1.414 1.414l1.465 1.465a33.87 33.87 0 00-.758-.008 7.662 7.662 0 107.662 7.662 1 1 0 10-2 0 5.662 5.662 0 11-5.662-5.662c.42 0 .772.007 1.065.018L9.288 7.622a1 1 0 101.414 1.414l3.331-3.331a1 1 0 000-1.414L10.703.96z"
        clipRule="evenodd"
        fill={color}
      />
    </Svg>
  )
}
