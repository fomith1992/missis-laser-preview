import * as React from 'react'
import Svg, { Path } from 'react-native-svg'
import { StyleSheet } from 'react-native'
import { IconComponentProps } from './icon-component'

export function BackIcon24({ style, ...rest }: IconComponentProps): React.ReactElement {
  const { color = '#000000' } = StyleSheet.flatten(style) ?? {}

  return (
    <Svg style={style} width={24} height={24} viewBox="0 0 24 24" fill="none" {...rest}>
      <Path d="M12.3533 52.8964L53 12" stroke={color} strokeWidth="4.26667" />
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        fillOpacity="0.9"
        d="M12.2367 4.70711C12.6272 4.31658 12.6272 3.68342 12.2367 3.29289C11.8462 2.90237 11.213 2.90237 10.8225 3.29289L3.29289 10.8225C2.90237 11.213 2.90237 11.8462 3.29289 12.2367L10.8225 19.7663C11.213 20.1568 11.8462 20.1568 12.2367 19.7663C12.6272 19.3757 12.6272 18.7426 12.2367 18.352L6.41425 12.5296H20.3141C20.8664 12.5296 21.3141 12.0819 21.3141 11.5296C21.3141 10.9773 20.8664 10.5296 20.3141 10.5296H6.41418L12.2367 4.70711Z"
        fill={color}
      />
    </Svg>
  )
}
