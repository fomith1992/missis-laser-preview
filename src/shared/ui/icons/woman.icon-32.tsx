import * as React from 'react'
import { StyleSheet } from 'react-native'
import Svg, { Path } from 'react-native-svg'
import { IconComponentProps } from './icon-component'

export function WomanIcon32({ style, ...rest }: IconComponentProps): React.ReactElement {
  const { width = 32, height = 32 } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg width={width} height={height} viewBox="0 0 32 32" fill="none" {...rest}>
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M16.073 1.883c5.647 0 15.059 2.823 15.059 15.059 0 12.235 0 15.059-2.824 15.059-2.823 0-6.588-2.824-12.235-2.824C10.426 29.177 6.741 32 3.838 32 .69 32 1.014 20.706 1.014 16.94c0-12.235 9.412-15.058 15.06-15.058z"
        fill="#F6FCB7"
      />
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        fill="#F7DECE"
        d="M4.779 16.529c0-8.025 5.057-14.53 11.294-14.53s11.294 6.505 11.294 14.53c0 8.024-5.057 14.53-11.294 14.53S4.779 24.553 4.779 16.529z"
      />
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        fill="#DF1F32"
        d="M16.073 27.294c-2.209 0-3.365-1.092-3.489-1.217a.94.94 0 011.322-1.34c.047.044.758.675 2.167.675 1.43 0 2.14-.65 2.17-.677a.949.949 0 011.325.027.935.935 0 01-.006 1.315c-.124.125-1.28 1.217-3.489 1.217z"
      />
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        fill="#C1694F"
        d="M17.014 22.588h-1.882a.941.941 0 110-1.882h1.882a.94.94 0 110 1.882z"
      />
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        fill="#F6FCB7"
        d="M2.016 21.647c-.028-.306-.06-.609-.06-.941 0-4.706 2.823.529 2.823-2.824 0-3.353 1.882-3.764 3.765-5.647l2.823-2.823s4.706 2.823 8.47 2.823c3.766 0 7.53 1.883 7.53 5.647 0 3.765 2.824-1.882 2.824 2.824 0 .334-.031.633-.055.941h.988c.008-1.39.008-2.948.008-4.706C31.132 4.706 21.72 0 16.073 0 10.426 0 1.014 4.706 1.014 16.941c0 1.09-.026 2.81.012 4.706h.99z"
      />
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        fill="#662113"
        d="M11.367 19.765a.94.94 0 01-.941-.942v-1.882a.941.941 0 011.882 0v1.883a.94.94 0 01-.941.94zm9.412 0a.94.94 0 01-.942-.942v-1.882a.941.941 0 111.883 0v1.883a.94.94 0 01-.941.94z"
      />
    </Svg>
  )
}
