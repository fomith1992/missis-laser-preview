import * as React from 'react'
import { StyleSheet } from 'react-native'
import Svg, { Path } from 'react-native-svg'

import { IconComponentProps } from './icon-component'

export function HealthIcon64({ style, ...rest }: IconComponentProps): React.ReactElement {
  const { width = 24, height = 24, color = '#000' } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg width={width} height={height} viewBox="0 0 64 64" fill="none" {...rest}>
      <Path
        d="M54 14H10C6.68629 14 4 16.6863 4 20V52C4 55.3137 6.68629 58 10 58H54C57.3137 58 60 55.3137 60 52V20C60 16.6863 57.3137 14 54 14Z"
        stroke={color}
        strokeWidth="4"
        strokeLinejoin="round"
      />
      <Path
        d="M18 14V10C18 8.93913 18.4214 7.92172 19.1716 7.17157C19.9217 6.42143 20.9391 6 22 6H42C43.0609 6 44.0783 6.42143 44.8284 7.17157C45.5786 7.92172 46 8.93913 46 10V14"
        stroke={color}
        strokeWidth="4"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path d="M32 26V46" stroke={color} strokeWidth="4" strokeLinecap="round" strokeLinejoin="round" />
      <Path d="M42 36H22" stroke={color} strokeWidth="4" strokeLinecap="round" strokeLinejoin="round" />
    </Svg>
  )
}
