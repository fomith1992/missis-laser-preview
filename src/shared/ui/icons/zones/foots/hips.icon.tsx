import * as React from 'react'
import { StyleSheet } from 'react-native'
import Svg, { Path } from 'react-native-svg'

import { IconComponentProps } from '../../icon-component'

export function ZoneFootsHips({ style, ...rest }: IconComponentProps): React.ReactElement {
  const { color = '#1F86FE' } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg viewBox="0 0 670.98 656.15" {...rest}>
      <Path
        fill={color}
        d="M452.29,190.84s59.21,30.37,80.17,85.71,8.83,92.19-12.13,150.88-6.22,170.32-6.22,170.32-24.63-83.25-1.57-172.18C534.27,341.73,567.48,275.47,452.29,190.84Z"
      />
      <Path fill={color} d="M264.31,288S142,426.3,117.68,596.48c0,0,1.52-73.69,44.83-170.18S264.31,288,264.31,288Z" />
      <Path
        fill={color}
        d="M205.19,114.73s.46,109.12,68,159c0,0-38.47-16.41-56-60.11C197.9,165.77,205.19,114.73,205.19,114.73Z"
      />
      <Path
        fill={color}
        d="M377.87,481.5s-6.47,57.62,20.51,116.25c0,0-16.28-18.21-22.26-54.8S377.87,481.5,377.87,481.5Z"
      />
      <Path
        fill={color}
        d="M392,46.45s-23.85,87.64,53.06,138.91c0,0-50.13-21.73-59.05-67.63C378.25,77.73,392,46.45,392,46.45Z"
      />
      <Path
        fill={color}
        d="M450.39,229.66s64.74,99.12,9.3,177.13c-48.47,68.19-172.21,80.4-234,191.15,0,0,12.23-40.94,70.8-88.49C347.8,467.77,535.06,419,450.39,229.66Z"
      />
    </Svg>
  )
}
