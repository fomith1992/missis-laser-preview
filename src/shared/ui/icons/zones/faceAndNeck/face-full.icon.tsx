import * as React from 'react'
import { StyleSheet } from 'react-native'
import Svg, { Path } from 'react-native-svg'

import { IconComponentProps } from '../../icon-component'

export function ZoneFaceFaceFull({ style, ...rest }: IconComponentProps): React.ReactElement {
  const { color = '#1F86FE' } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg viewBox="0 0 670.98 656.15" {...rest}>
      <Path
        fill={color}
        d="M434.63,80.22s-26.74,6.61-30.84,36.55,11.14,49.3,39.32,71.61,28.18,23.47,29.94,32.28c.62,3.13,1.55,6.57,2.35,10.11,1.44,6.41,2.47,13.19.58,19.24-2.93,9.39-7.63,10.56-7.63,10.56a28.58,28.58,0,0,0,4.92-27.22c-5.29-16.07-1.32-17.61-15.63-29s-51.51-33-56.2-67.87S418.12,79.56,434.63,80.22Z"
      />
      <Path
        fill={color}
        d="M465.33,235.46S459.51,246,459.6,248s4.16,3,6.47,1.94,3.78-9.42,2.95-11.83S466.44,233.4,465.33,235.46Z"
      />
      <Path fill={color} d="M419.89,241.94s-9.73.7-14.24,5.47.1,16,8.12,8.52S426.62,240.63,419.89,241.94Z" />
      <Path
        fill={color}
        d="M407.23,267.91s-21.13,7.92-30.37,0S373.78,248,373.78,248s-8.81,13.51,2.64,21.65S407.23,267.91,407.23,267.91Z"
      />
      <Path
        fill={color}
        d="M311.33,121.46s-37-8.8-54.29-4.7-59,24.29-73.07,58.37c0,0,8.8-22.56,24.06-41.34s21.72-29.35,51.07-34,57.23-5,61,4.4S318.38,124.4,311.33,121.46Z"
      />
      <Path
        fill={color}
        d="M463,79.49s-30.23-5-45.49,8.51c-9.78,8.66-1.35,14.38,2.17,16.44s17.91-5.58,30.23-9.1,14.26-4.08,19.55.88C469.41,96.22,467.06,83,463,79.49Z"
      />
      <Path
        fill={color}
        d="M358,350.38a153.26,153.26,0,0,0,36.86-20c20-14.82,38.75-31.84,54.3-28.91,0,0,8.95,2.48,14.53-1s13.64-6.16,18.63-1.17c7,7,13.62,22.89,12.74,27.29,0,0-9.1-26.41-18.05-27.58s-13.95,6.46-21.57,5.87-17-2.93-33.46,7.63S381.78,344.51,358,350.38Z"
      />
      <Path
        fill={color}
        d="M361.52,352.73s10.57-.88,36.4,4.4,40.72,16.58,69.43,7.63c27.3-8.52,27-27,22.6-30.82,0,0-6.34-8.51-2.23,0s2.51,15.34-15,25.24-38.16,4.7-50.78,1.47S375.91,349.5,361.52,352.73Z"
      />
      <Path
        fill={color}
        d="M480.68,327.49s-11.45,2.64-39.92,6.45-61.79,19.19-78,18.55c0,0,28.84-7.69,44.47-14.14S487.9,321.32,480.68,327.49Z"
      />
      <Path
        fill={color}
        d="M321.38,171.28S302,183.61,282.87,185.81s-41.6,7.7-50.85,12.32c0,0,10.13-1.32,15-2.42s-10.78,17.61-10.78,17.61l16.46-16.07v17.83l5.1-17.61,7.71,11-.22-13,9.24,9.69-2.2-12,7.93,7.37v-7.37l4.18,5.61.44-5.61,5.94,2.75-.22-5.94,5.51,3.19-.88-6.27,7.7,5.06.66-6.6,6.38,2.64-1.54-4.4,4.84.65.44-4.62,4,1.76,1.54-6.27s8.36-2.75,11.66-2.75S336.34,167.76,321.38,171.28Z"
      />
      <Path
        fill={color}
        d="M460.13,73.33S472.76,88,477,118.53s-1.69,50.47,17.42,71S520,255.15,520,293.08s-6.45,103.09-4.1,117.17-6.46,47.55-42.26,54-98,30.53-170.81,12.92c0,0,42.7,4.69,104.4-6.46s96.93-18.78,101-51.66,10-137.78,4.7-162.51-18.78-58.19-28.17-73.45-13.35-34-14-56.35S460.13,73.33,460.13,73.33Z"
      />
      <Path
        fill={color}
        d="M424,161.59h1.76s17.61.22,27.74,1.54,24.9,4.75,29.62,7.88,9.78,6.21,12.64,5.77c0,0-7.26,2.64-13.65-1.65l.56,4.21L477,175.13v4.21l-6.23-7.4v6.6l-4.34-8.07-1.54,5.87-5.06-7.7,1.1,6.49-6.38-5.54-1.32,6.75-4.41-7.7-3.14,3.52-6.86-4.4,1,4.18-4.62-4.18-4.41,1.83L429,166.22s-7.27-.66-7.71-2.2S422.2,161.59,424,161.59Z"
      />
      <Path
        fill={color}
        d="M481.32,164.15s-1,3-18,.65,1,2.19,2.91,2.63,19.15,5.83,19.15,5.83,7.77,1.88,14.26-3.11C499.63,170.15,483.15,175.47,481.32,164.15Z"
      />
      <Path fill={color} d="M379.17,484.49s-14.2,54.7,8.1,91.92C387.27,576.41,355.81,540.84,379.17,484.49Z" />
    </Svg>
  )
}
