import * as React from 'react'
import { StyleSheet } from 'react-native'
import Svg, { Path } from 'react-native-svg'

import { IconComponentProps } from '../../icon-component'

export function ZoneBikiniButtocks({ style, ...rest }: IconComponentProps): React.ReactElement {
  const { color = '#1F86FE' } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg viewBox="0 0 670.98 656.15" {...rest}>
      <Path
        fill={color}
        d="M439.55,102s26,165.13-126.13,184.64c0,0,55.67,41.89,41.59,293.54,0,0,22.87-174.22,11-279.8C366,300.41,469.34,259.13,439.55,102Z"
      />
      <Path
        fill={color}
        d="M472.91,109.47s101,72.57,31,229.21c-69.3,155.07-89.1,241.53-89.1,241.53s3.52-60.72,37.84-155.75S554.74,215.93,472.91,109.47Z"
      />
      <Path fill={color} d="M162.76,102s-58.37,179.9,36.51,478.18C199.27,580.21,148.24,278.49,162.76,102Z" />
      <Path fill={color} d="M371.78,284.32s14.28,30.91,56.16,35.15c0,0-36.37-20.33-39.15-49.5Z" />
    </Svg>
  )
}
