import * as React from 'react'
import { StyleSheet } from 'react-native'
import Svg, { Path } from 'react-native-svg'

import { IconComponentProps } from '../../icon-component'

export function ZoneTorsChestFull({ style, ...rest }: IconComponentProps): React.ReactElement {
  const { color = '#1F86FE' } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg viewBox="0 0 670.98 656.15" {...rest}>
      <Path
        fill={color}
        d="M248.92,207.1c11.61-19.85,20.71-63,14.27-125.28,0,0,.82,140.34-96,148.55C167.16,230.37,225.82,246.59,248.92,207.1Z"
      />
      <Path fill={color} d="M185.22,319s47.6,21.34,57.45,67.3l8.18-19.69S232.82,334.61,185.22,319Z" />
      <Path
        fill={color}
        d="M386.24,81.82s-13.64,91.62.93,106.19C402,202.85,423.93,198,468,194.77s73.41-3.77,104.17,32c0,0-22.38-22.38-64.36-25.31-44.71-3.12-107.11,18.51-130,9C344.6,196.74,386.24,81.82,386.24,81.82Z"
      />
      <Path
        fill={color}
        d="M257.49,344.72s-17.66,50,7.35,91c21.56,35.27,81.72,47.49,111.39,21.13,0,0-31.56,43.89-84.91,24.59-64.32-23.26-49.23-97.56-49.23-97.56A114.23,114.23,0,0,1,257.49,344.72Z"
      />
      <Path
        fill={color}
        d="M239.7,578.63S216.77,423.7,145.56,363c-38.83-33.12-75.84-26.69-75.84-26.69s39.22-23.49,93.55,24.43C247.56,435.05,239.7,578.63,239.7,578.63Z"
      />
      <Path fill={color} d="M503.71,463.56s3.4,53.25-11.52,115.07c0,0,20.66-40.88,28.07-120.83Z" />
      <Path
        fill={color}
        d="M518.78,274.34s3,14.27-4.84,32.25-14.54,25,9.85,50.7c26.57,28,31.22,70.71,4.31,90.56-22.43,16.55-61.77,11.6-73.68,5.54,0,0,30.72,28.71,71.79,10.35,45.75-20.45,31.86-81,6.21-109.53-19.76-22-15.91-19.91-10-37.62S523.89,281.4,518.78,274.34Z"
      />
    </Svg>
  )
}
