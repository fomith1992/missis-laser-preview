import * as React from 'react'
import { StyleSheet } from 'react-native'
import Svg, { Path } from 'react-native-svg'

import { IconComponentProps } from '../../icon-component'

export function ZoneTorsStomachWhiteLine({ style, ...rest }: IconComponentProps): React.ReactElement {
  const { color = '#1F86FE' } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg viewBox="0 0 670.98 656.15" {...rest}>
      <Path
        fill={color}
        d="M283.85,74.22s15.67,79-30.61,129.72c-45,49.32-244.88,148.51-139.77,361.49,0,0-35.73-85.88-13-165.57S194.48,269.2,227.51,245.28C285,203.64,302.05,152,283.85,74.22Z"
      />
      <Path fill={color} d="M409,474s-8.06,43.17-39.89,63.66C369.08,537.68,407.5,528.9,409,474Z" />
      <Path
        fill={color}
        d="M528.57,72.81s10,60.22-23.94,126.62S459.08,285.13,449,326.82c0,0,19.61-52,44.34-92.53S543.83,147,528.57,72.81Z"
      />
      <Path
        fill={color}
        d="M586.12,75.28s9,41.19-16.64,75.9c-25.13,34-41.06,45.33-61,126.39S472.09,388.18,419.87,439.7c0,0,44.2-45.57,64.81-98.69s20.61-86.57,44.42-139.24S586.64,142.87,586.12,75.28Z"
      />
      <Path
        fill={color}
        d="M297.07,429.1s49.69,39.86,67,136.67c0,0,1.56-37.16-19.34-79.21C325.28,447.36,297.07,429.1,297.07,429.1Z"
      />
      <Path
        fill={color}
        d="M519.35,245.8s-6.64,23.33-4.53,59.42c2.08,35.57-17.11,67.42-29.72,98.51s-16,82.56,1.63,162.4c0,0-18.79-111.74,5.1-160.86s34.1-77.94,30.3-103.17C516.4,264,519.35,245.8,519.35,245.8Z"
      />
    </Svg>
  )
}
