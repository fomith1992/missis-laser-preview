import * as React from 'react'
import { StyleSheet } from 'react-native'
import Svg, { Path } from 'react-native-svg'

import { IconComponentProps } from '../../icon-component'

export function ZoneHandsHandsFull({ style, ...rest }: IconComponentProps): React.ReactElement {
  const { color = '#1F86FE' } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg viewBox="0 0 670.98 656.15" {...rest}>
      <Path
        fill={color}
        d="M496.5,598.57a249.41,249.41,0,0,0-16.58-40.39C468.32,536.33,460.87,529.12,406,531c-57.71,1.92-173-8.18-234.24-19.58S98.57,494,102.17,465.18s68.38-275.93,135-347.31c0,0-33.6,50.39-70.78,148.76S103.87,466,119,483.17c15.47,17.57,123.33,35,183,37.8,106.65,5,148.5-18.11,170.23,8C493.15,554.13,496.5,598.57,496.5,598.57Z"
      />
      <Path
        fill={color}
        d="M553.81,391.54s-32.75-35.73-101.29-19.68C390.94,386.28,296.3,413.11,231,412.45c0,0,33.78,6.54,114.56-8.26S487.38,361.68,553.81,391.54Z"
      />
      <Path
        fill={color}
        d="M204.74,428.59s17-7.37,20-40.49c2.37-26.35,13.55-203,56.13-233,0,0-25.6-3.09-41,73.41C226.51,295.52,216.74,417.79,204.74,428.59Z"
      />
      <Path
        fill={color}
        d="M242.74,112.34s23.92-19.83,96-58.24c11-5.88,18-10.4,34.26,3C390.1,71.24,414,92.8,418.3,96.21s12.1,79.09,1.86,75.37S400,133.74,396.28,110.47A68.26,68.26,0,0,1,375.8,98.69S364.33,109.55,352.54,112c0,0,20.61-10.21,21.81-19.06,0,0,14.4,15.6,25,15a151,151,0,0,0,5.39,28.49c4.5,15.45,12.39,31.09,15.15,30.9,4.08-.29,2.25-62.84-3.9-70S367.3,55,357.86,54.43s-24,9.75-39.74,17.7S264.28,98.15,242.74,112.34Z"
      />
      <Path
        fill={color}
        d="M280.4,149.41S293.31,165,312,164.86c15.84-.09,42.49-10.68,59.48-8.39s30.35,4.39,35.32,3.63c0,0-24.51-4.26-32.65-5.73-9.46-1.7-17-1.14-26.53.77S307.72,170.15,280.4,149.41Z"
      />
      <Path
        fill={color}
        d="M343.2,90.42s-4.59,13.42,6,24.35C369.28,135.36,398,133,398,133s-33.64,6.21-51.66-15.09C341,111.6,339.53,97.82,343.2,90.42Z"
      />
      <Path
        fill={color}
        d="M361.43,112.43s7,9.27,14,11.38c0,0,1.46,3.74,2.92,4.72,0,0-1.3-5.85.33-8.62A28.1,28.1,0,0,1,361.43,112.43Z"
      />
      <Path
        fill={color}
        d="M386.2,160.61s-33.44,14.7-37.94,13.2,2.7-12.6,6.45-14.1c0,0-7.8,10.8-5.1,11.1S378.1,161.36,386.2,160.61Z"
      />
    </Svg>
  )
}
