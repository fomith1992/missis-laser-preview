import * as React from 'react'
import { StyleSheet } from 'react-native'
import Svg, { Path } from 'react-native-svg'
import { IconComponentProps } from './icon-component'

export function ExitIcon24({ style, ...rest }: IconComponentProps): React.ReactElement {
  const { width = 24, height = 24, color = '#000' } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg width={width} height={height} viewBox="0 0 24 24" fill="none" {...rest}>
      <Path
        d="M15.2212 8.18269V6.09135C15.2212 5.53669 15.0008 5.00474 14.6086 4.61254C14.2164 4.22034 13.6845 4 13.1298 4H3.09135C2.53669 4 2.00474 4.22034 1.61254 4.61254C1.22034 5.00474 1 5.53669 1 6.09135V18.6394C1 19.1941 1.22034 19.726 1.61254 20.1182C2.00474 20.5104 2.53669 20.7308 3.09135 20.7308H13.1298C13.6845 20.7308 14.2164 20.5104 14.6086 20.1182C15.0008 19.726 15.2212 19.1941 15.2212 18.6394V16.5481"
        stroke={color}
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path
        d="M18.5673 8.18262L22.75 12.3653L18.5673 16.548"
        stroke={color}
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path d="M8.47656 12.3655H22.75" stroke={color} strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
    </Svg>
  )
}
