import * as React from 'react'
import { StyleSheet } from 'react-native'
import Svg, { Path } from 'react-native-svg'
import { IconComponentProps } from './icon-component'

export function SaleIcon24({ style, ...rest }: IconComponentProps): React.ReactElement {
  const { width = 24, height = 24, color = '#000' } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg width={width} height={height} viewBox="0 0 24 24" fill="none" {...rest}>
      <Path
        d="M14.2888 2.04464L4.5 14.1429H10.2143L8.80759 21.8763C8.80491 21.8915 8.8056 21.9071 8.80959 21.922C8.81359 21.9368 8.8208 21.9507 8.83072 21.9625C8.84063 21.9743 8.85302 21.9837 8.86699 21.9902C8.88097 21.9967 8.8962 22 8.91161 22V22C8.928 22 8.94416 21.9961 8.95881 21.9888C8.97346 21.9814 8.98619 21.9707 8.99598 21.9576L18.7857 9.85714H13.0714L14.4848 2.12277C14.4867 2.10736 14.4853 2.09172 14.4807 2.0769C14.4761 2.06207 14.4684 2.0484 14.4581 2.03678C14.4478 2.02517 14.4351 2.01588 14.4209 2.00954C14.4068 2.0032 14.3914 1.99995 14.3759 2V2C14.3589 2.00007 14.3421 2.00415 14.327 2.0119C14.3119 2.01966 14.2988 2.03088 14.2888 2.04464V2.04464Z"
        stroke={color}
        strokeOpacity="0.9"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  )
}
