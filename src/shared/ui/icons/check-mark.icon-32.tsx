import * as React from 'react'
import { StyleSheet } from 'react-native'
import Svg, { Path } from 'react-native-svg'
import { IconComponentProps } from './icon-component'

export function CheckMarkIcon32({ style, ...rest }: IconComponentProps): React.ReactElement {
  const { width = 33, height = 32, color = '#000' } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg width={width} height={height} viewBox="0 0 33 32" fill="none" {...rest}>
      <Path
        d="M30.8333 4L11 26.6667L2.5 18.1667"
        stroke={color}
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  )
}
