import * as React from 'react'
import { StyleSheet } from 'react-native'
import Svg, { Path } from 'react-native-svg'

import { IconComponentProps } from './icon-component'

export function TimeIcon64({ style, ...rest }: IconComponentProps): React.ReactElement {
  const { width = 24, height = 24, color = '#000' } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg width={width} height={height} viewBox="0 0 64 64" fill="none" {...rest}>
      <Path
        d="M32 8C18.75 8 8 18.75 8 32C8 45.25 18.75 56 32 56C45.25 56 56 45.25 56 32C56 18.75 45.25 8 32 8Z"
        stroke={color}
        strokeWidth="4"
        strokeMiterlimit="10"
      />
      <Path d="M32 16V34H44" stroke={color} strokeWidth="4" strokeLinecap="round" strokeLinejoin="round" />
    </Svg>
  )
}
