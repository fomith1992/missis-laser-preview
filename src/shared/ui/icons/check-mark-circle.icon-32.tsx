import * as React from 'react'
import { StyleSheet } from 'react-native'
import Svg, { Path } from 'react-native-svg'
import { IconComponentProps } from './icon-component'

export function CheckMarkCircleIcon32({ style, ...rest }: IconComponentProps): React.ReactElement {
  const { width = 32, height = 32, color = '#000' } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg width={width} height={height} viewBox="0 0 32 32" fill="none" {...rest}>
      <Path
        d="M28 16C28 9.375 22.625 4 16 4C9.375 4 4 9.375 4 16C4 22.625 9.375 28 16 28C22.625 28 28 22.625 28 16Z"
        stroke={color}
        strokeWidth="2"
        strokeMiterlimit="10"
      />
      <Path d="M22 11L13.6 21L10 17" stroke={color} strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
    </Svg>
  )
}
