import * as React from 'react'
import { StyleSheet } from 'react-native'
import Svg, { Path } from 'react-native-svg'
import { IconComponentProps } from './icon-component'

export function DefenceIcon32({ style, ...rest }: IconComponentProps): React.ReactElement {
  const { width = 32, height = 32, color = '#000' } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg width={width} height={height} viewBox="0 0 32 32" fill="none" {...rest}>
      <Path
        d="M29.2756 6.79303C23.5436 5.76483 21.1737 5.00329 16 2.66675C10.8262 5.00329 8.45638 5.76483 2.72432 6.79303C1.68586 23.252 15.0115 28.9353 16 29.3334C16.9884 28.9353 30.3141 23.252 29.2756 6.79303Z"
        stroke={color}
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  )
}
