import * as React from 'react'
import { StyleSheet } from 'react-native'
import Svg, { Path } from 'react-native-svg'
import { IconComponentProps } from './icon-component'

export function DefenceIcon24({ style, ...rest }: IconComponentProps): React.ReactElement {
  const { width = 24, height = 24, color = '#000' } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg width={width} height={height} viewBox="0 0 24 24" fill="none" {...rest}>
      <Path
        d="M21.9567 5.09471C17.6577 4.32356 15.8802 3.7524 12 2C8.11967 3.7524 6.34227 4.32356 2.04323 5.09471C1.26438 17.4389 11.2586 21.7014 12 22C12.7413 21.7014 22.7355 17.4389 21.9567 5.09471Z"
        stroke={color}
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  )
}
