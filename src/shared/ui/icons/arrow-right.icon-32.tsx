import * as React from 'react'
import { StyleSheet } from 'react-native'
import Svg, { Path } from 'react-native-svg'
import { IconComponentProps } from './icon-component'

export function ArrowRightIcon32({ style, ...rest }: IconComponentProps): React.ReactElement {
  const { width = 32, height = 32, color = '#000' } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg width={width} height={height} viewBox="0 0 32 32" fill="none" {...rest}>
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M16 3C8.82272 3 3 8.82272 3 16C3 23.1773 8.82272 29 16 29C23.1773 29 29 23.1773 29 16C29 8.82272 23.1773 3 16 3ZM5 16C5 9.92728 9.92728 5 16 5C22.0727 5 27 9.92728 27 16C27 22.0727 22.0727 27 16 27C9.92728 27 5 22.0727 5 16ZM15.7094 10.2901C16.1015 9.90115 16.7346 9.90361 17.1236 10.2956L22.0849 15.2956C22.4718 15.6855 22.4718 16.3145 22.0849 16.7044L17.1236 21.7043C16.7346 22.0964 16.1015 22.0989 15.7094 21.7099C15.3174 21.3209 15.3149 20.6877 15.7039 20.2957L18.974 17H10.625C10.0727 17 9.625 16.5523 9.625 16C9.625 15.4477 10.0727 15 10.625 15H18.974L15.7039 11.7044C15.3149 11.3123 15.3174 10.6792 15.7094 10.2901Z"
        fill={color}
        fillOpacity="0.5"
      />
    </Svg>
  )
}
