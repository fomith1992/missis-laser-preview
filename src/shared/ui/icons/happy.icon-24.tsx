import * as React from 'react'
import { StyleSheet } from 'react-native'
import Svg, { Path } from 'react-native-svg'

import { IconComponentProps } from './icon-component'

export function HappyIcon24({ style, ...rest }: IconComponentProps): React.ReactElement {
  const { width = 24, height = 24, color = '#000' } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg width={width} height={height} viewBox="0 0 24 24" fill="none" {...rest}>
      <Path
        d="M8.66665 11.3334C9.40303 11.3334 9.99998 10.7365 9.99998 10.0001C9.99998 9.2637 9.40303 8.66675 8.66665 8.66675C7.93027 8.66675 7.33331 9.2637 7.33331 10.0001C7.33331 10.7365 7.93027 11.3334 8.66665 11.3334Z"
        fill={color}
      />
      <Path
        d="M15.3333 14.9165C13.3333 17.2498 10.6667 17.2498 8.66667 14.9165"
        stroke={color}
        strokeWidth="2.22222"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path
        d="M15.3333 11.3334C16.0697 11.3334 16.6667 10.7365 16.6667 10.0001C16.6667 9.2637 16.0697 8.66675 15.3333 8.66675C14.597 8.66675 14 9.2637 14 10.0001C14 10.7365 14.597 11.3334 15.3333 11.3334Z"
        fill={color}
      />
      <Path
        d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"
        stroke={color}
        strokeWidth="2.22222"
        strokeMiterlimit="10"
      />
    </Svg>
  )
}
