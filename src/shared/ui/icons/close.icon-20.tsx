import * as React from 'react'
import { StyleSheet } from 'react-native'
import Svg, { Path } from 'react-native-svg'
import { IconComponentProps } from './icon-component'

export function CloseIcon20({ style, ...rest }: IconComponentProps): React.ReactElement {
  const { width = 20, height = 20, color = '#000' } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg width={width} height={height} viewBox="0 0 20 20" fill="none" {...rest}>
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M2.58211 1.16789C2.19158 0.777369 1.55842 0.777369 1.16789 1.16789C0.777369 1.55842 0.777369 2.19158 1.16789 2.58211L8.60217 10.0164L1.16789 17.4507C0.777369 17.8412 0.777369 18.4743 1.16789 18.8649C1.55842 19.2554 2.19158 19.2554 2.58211 18.8649L10.0164 11.4306L17.4507 18.8649C17.8412 19.2554 18.4743 19.2554 18.8649 18.8649C19.2554 18.4743 19.2554 17.8412 18.8649 17.4507L11.4306 10.0164L18.8649 2.58211C19.2554 2.19158 19.2554 1.55842 18.8649 1.16789C18.4743 0.777369 17.8412 0.777369 17.4507 1.16789L10.0164 8.60217L2.58211 1.16789Z"
        fill={color}
        fillOpacity="0.28"
      />
    </Svg>
  )
}
