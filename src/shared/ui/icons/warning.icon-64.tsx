import * as React from 'react'
import Svg, { Path, Rect } from 'react-native-svg'
import { StyleSheet } from 'react-native'
import { IconComponentProps } from './icon-component'

export function WarningIcon64({ style, ...rest }: IconComponentProps): React.ReactElement {
  const { color = '#000000' } = StyleSheet.flatten(style) ?? {}

  return (
    <Svg style={style} width={64} height={64} viewBox="0 0 64 64" fill="none" {...rest}>
      <Path
        d="M34.468 38.3077H29.6162L29.0822 14.4666H35.0251L34.468 38.3077ZM28.9197 45.597C28.9197 44.7148 29.2061 43.9874 29.7787 43.4148C30.3513 42.8267 31.1328 42.5327 32.1233 42.5327C33.1138 42.5327 33.8953 42.8267 34.468 43.4148C35.0406 43.9874 35.3269 44.7148 35.3269 45.597C35.3269 46.4481 35.0483 47.1601 34.4912 47.7327C33.934 48.3053 33.1447 48.5916 32.1233 48.5916C31.1019 48.5916 30.3126 48.3053 29.7555 47.7327C29.1983 47.1601 28.9197 46.4481 28.9197 45.597Z"
        fill={color}
      />
      <Rect x="2" y="2" width="60" height="60" rx="30" stroke={color} strokeWidth="4" />
    </Svg>
  )
}
