import styled from '@emotion/native'
import React from 'react'
import { color } from 'src/style/mixins/color'
import { Footnote } from '../atoms'
import { MaskInput, MaskInputProps } from '../molecules/mask-input'

type ExludedFields = 'maxLength' | 'prefix' | 'type' | 'options'

type BaseProps = Omit<MaskInputProps, ExludedFields>

type PhoneMaskInputProps = BaseProps & {
  placeholder?: string
  errorMessage?: string
}

const mask = '(999) 999-99-99'
export const PHONE_MASK_LENGTH = mask.length

export const PhoneMaskInput = ({
  keyboardType = 'numeric',
  placeholder = '(900) 123-45-67',
  errorMessage,
  ...rest
}: PhoneMaskInputProps) => {
  return (
    <>
      <MaskInput
        prefix="+7"
        type="custom"
        keyboardType={keyboardType}
        options={{ mask }}
        placeholder={placeholder}
        maxLength={PHONE_MASK_LENGTH}
        errorMessage={errorMessage}
        {...rest}
      />
      {errorMessage != null && <PhoneNumberErrorMessage mt={12}>{errorMessage}</PhoneNumberErrorMessage>}
    </>
  )
}

const PhoneNumberErrorMessage = styled(Footnote)`
  color: ${color('error')};
`
