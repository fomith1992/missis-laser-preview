import styled, { css } from '@emotion/native'
import { IfThen } from 'src/style/lib/IfThen'

import { space, SpaceProps } from 'src/style/mixins/space'

type Props = SpaceProps &
  Partial<{
    flex: boolean
    aiCenter: boolean
  }>

export const BaseView = styled.View<Props>`
  flex-shrink: 1;

  ${space};

  ${IfThen('flex', css({ flex: 1 }))};
  ${IfThen('aiCenter', css({ alignItems: 'center' }))};
`
