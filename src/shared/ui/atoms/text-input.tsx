import React, { useEffect, useRef, useState } from 'react'
import styled, { css } from '@emotion/native'
import { NativeSyntheticEvent, StyleSheet, TextInput as BaseInput, TextInputFocusEventData } from 'react-native'
import { RowView } from './flex'
import { SpaceProps } from 'src/style/mixins/space'
import { font } from 'src/style/mixins/font'
import { theme } from 'src/style/theme'
import { IfThen } from 'src/style/lib/IfThen'
import { Footnote } from './text'
import { color } from 'src/style/mixins/color'
import { hitSlopParams } from '../buttons/hit-slop-params'

export type BaseProps = SpaceProps & React.ComponentProps<typeof BaseInput>

export type TextInputProps = BaseProps & {
  errorMessage?: string
  RightIcon?: React.ElementType
  onRightIconPress?: () => void
  focusOnMount?: boolean
}

export const TextInput = ({
  errorMessage,
  RightIcon,
  onRightIconPress,
  editable = true,
  mb,
  ml,
  mr,
  mt,
  pb,
  pl,
  pr,
  pt,
  onBlur,
  onFocus,
  focusOnMount,
  ...rest
}: TextInputProps) => {
  const [isFocused, setFocused] = useState(false)

  const inputRef = useRef<BaseInput>(null)

  const blurHandler = (e: NativeSyntheticEvent<TextInputFocusEventData>) => {
    setFocused(false)
    onBlur ? onBlur(e) : null
  }

  const focusHandler = (e: NativeSyntheticEvent<TextInputFocusEventData>) => {
    setFocused(true)
    onFocus ? onFocus(e) : null
  }

  useEffect(() => {
    if (inputRef.current && focusOnMount) {
      setTimeout(() => {
        inputRef.current?.focus()
      }, 200)
    }
  }, [focusOnMount])

  return (
    <>
      <Container isError={errorMessage != null} focused={isFocused} {...{ mb, ml, mr, mt, pb, pl, pt, pr }}>
        <TextInputStyled
          ref={inputRef}
          style={editable ? undefined : styles.noEditable}
          hasIcon={Boolean(RightIcon)}
          onFocus={focusHandler}
          onBlur={blurHandler}
          editable={editable}
          selectionColor={theme.colors.selectionColor}
          {...rest}
        />
        {RightIcon !== undefined && (
          <IconContainer hitSlop={hitSlopParams('XL')} onPress={onRightIconPress}>
            <RightIcon />
          </IconContainer>
        )}
      </Container>
      {errorMessage != null && <ErrorMessage mt={12}>{errorMessage}</ErrorMessage>}
    </>
  )
}

export type ContainerProps = {
  focused: boolean
  isError?: boolean
}

export const Container = styled(RowView)<ContainerProps>`
  padding-top: 12px;
  padding-bottom: 12px;
  border-bottom-color: ${({ theme: { colors }, focused }) => (focused ? colors.primary : colors.surface)};
  border-bottom-width: 1px;
  justify-content: space-between;

  ${IfThen('isError', css({ borderBottomColor: theme.colors.error }))}
`

const ErrorMessage = styled(Footnote)`
  color: ${color('error')};
`

const IconContainer = styled.TouchableOpacity`
  margin-right: 12px;
  margin-left: 12px;
`

const TextInputStyled = styled.TextInput<{ hasIcon: boolean }>`
  flex: 1;
  height: 19px;
  ${font({ variant: 'Body', color: 'primaryText' })};
`

const styles = StyleSheet.create({
  noEditable: {
    color: theme.colors.tertiaryText,
  },
})
