import styled from '@emotion/native'

import { color } from 'src/style/mixins/color'
import { BaseView } from './view'

export const HLine = styled(BaseView)`
  background-color: ${color('surface')};
  height: 1px;
`
