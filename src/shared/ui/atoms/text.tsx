import styled, { css } from '@emotion/native'
import { IfThen } from 'src/style/lib/IfThen'
import { font, TFontParams } from 'src/style/mixins/font'
import { SpaceProps, space } from 'src/style/mixins/space'

type BaseParams = Partial<{
  bold: boolean
  strikethrough: boolean
  wrap: boolean
  flex: boolean
  center: boolean
}>

type FontProps = Partial<Omit<TFontParams, 'variant'>>

export const BaseText = styled.Text<SpaceProps & BaseParams>`
  ${space};

  ${IfThen('bold', css({ fontWeight: 'bold' }))}
  ${IfThen('wrap', css({ flexWrap: 'wrap' }))}
  ${IfThen('strikethrough', css({ textDecorationLine: 'line-through' }))}
  ${IfThen('flex', css({ flex: 1 }))}
  ${IfThen('center', css({ textAlign: 'center' }))}
`

export const Headiline = styled(BaseText)<FontProps>`
  ${({ color }) => font({ variant: 'Headline', color })}
`

export const Title = styled(BaseText)<FontProps>`
  ${({ color }) => font({ variant: 'Title', color })}
`

export const SubTitle = styled(BaseText)<FontProps>`
  ${({ color }) => font({ variant: 'SubTitle', color })}
`

export const Body = styled(BaseText)<FontProps>`
  ${({ color }) => font({ variant: 'Body', color })}
`

export const Footnote = styled(BaseText)<FontProps>`
  ${({ color }) => font({ variant: 'footnote', color: color ?? 'secondaryText' })}
`

export const Caption = styled(BaseText)<FontProps>`
  ${({ color }) => font({ variant: 'caption', color })}
`

export const Link = styled(BaseText)<FontProps>`
  ${({ color }) => font({ variant: 'footnote', color: color ?? 'primary' })}
  text-decoration: underline;
`
