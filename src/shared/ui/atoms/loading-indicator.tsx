import styled from '@emotion/native'
import React from 'react'
import { ActivityIndicator, Modal } from 'react-native'

type LoadingIndicatorProps = {
  visible: boolean
}

export const LoadingIndicator = ({ visible }: LoadingIndicatorProps): React.ReactElement | null => {
  if (visible) {
    return (
      <Modal transparent statusBarTranslucent>
        <ModalContainer>
          <ActivityIndicator size="large" color="white" />
        </ModalContainer>
      </Modal>
    )
  }

  return null
}

const ModalContainer = styled.View`
  background-color: rgba(0, 0, 0, 0.5);
  flex: 1;
  align-items: center;
  justify-content: center;
`
