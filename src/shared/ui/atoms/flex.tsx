import styled from '@emotion/native'

import { BaseView } from './view'

export const RowView = styled(BaseView)`
  flex-direction: row;
`

export const ColView = styled(BaseView)`
  flex-direction: column;
`
