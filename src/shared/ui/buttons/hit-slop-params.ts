import { Insets } from 'react-native'

type hitSlopParamsType = 'M' | 'L' | 'XL' | number

export const hitSlopParams = (indent: hitSlopParamsType): Insets => {
  switch (indent) {
    case 'XL':
      return { top: 12, bottom: 12, left: 12, right: 12 }
    case 'L':
      return { top: 8, bottom: 8, left: 8, right: 8 }
    case 'M':
      return { top: 4, bottom: 4, left: 4, right: 4 }
    default:
      return { bottom: indent, left: indent, right: indent, top: indent }
  }
}
