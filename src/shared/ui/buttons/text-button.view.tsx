import React, { useState } from 'react'
import styled, { css, ReactNativeStyle } from '@emotion/native'

import { TTheme } from 'src/style/theme'
import { Body } from '../atoms'

type ButtonText = 'primary' | 'secondary' | 'error'

export interface TextButtonProps {
  children?: React.ReactNode
  onPress?: () => void
  type?: ButtonText
  disabled?: boolean
  style?: ReactNativeStyle
}

export const TextButton = ({
  children,
  onPress,
  style,
  type = 'primary',
  disabled = false,
}: TextButtonProps): React.ReactElement => {
  const [isPressed, setPressed] = useState(false)
  return (
    <Button
      type={type}
      disabled={disabled}
      isPressed={isPressed}
      onPress={onPress}
      style={style}
      onPressIn={() => setPressed(true)}
      onPressOut={() => setPressed(false)}
    >
      <Body center color={type === 'primary' || type === 'error' ? 'lightText' : 'primaryText'}>
        {children}
      </Body>
    </Button>
  )
}

interface ButtonStyledProps {
  type: ButtonText
  disabled: boolean
  isPressed: boolean
}

type TColors = TTheme['colors']

const commonStyles = {
  padding: css`
    padding: 12px 16px;
  `,
  radius: css`
    border-radius: 30px;
  `,
}

const buttonStyles = {
  primary: {
    base: (colors: TColors) => css`
      ${commonStyles.padding}
      ${commonStyles.radius}
      background-color: ${colors.primary};
    `,
    disabled: (colors: TColors) => css`
      background-color: ${colors.inactive};
    `,
    pressed: (colors: TColors) => css`
      background-color: ${colors.primaryVariant};
    `,
  },
  secondary: {
    base: css`
      padding: 10px 16px;
      border: 2px solid;
      ${commonStyles.radius}
    `,
    disabled: (colors: TColors) => css`
      border: none;
      background-color: ${colors.inactive};
    `,
    pressed: css`
      border: 2px solid rgba(0, 0, 0, 0.28);
    `,
  },
  error: {
    base: (colors: TColors) => css`
      ${commonStyles.padding};
      ${commonStyles.radius};
      background-color: ${colors.error};
    `,
    disabled: (colors: TColors) => css`
      background-color: ${colors.inactive};
    `,
    pressed: (colors: TColors) => css`
      background-color: ${colors.errorDark};
    `,
  },
}

const makeButtonStyles = () => {
  return ({ type, disabled, isPressed, theme: { colors } }: ButtonStyledProps & { theme: TTheme }) => {
    const styles: ReactNativeStyle[] = []
    switch (type) {
      case 'primary': {
        styles.push(buttonStyles.primary.base(colors))
        if (disabled) styles.push(buttonStyles.primary.disabled(colors))
        if (isPressed) styles.push(buttonStyles.primary.pressed(colors))
        break
      }
      case 'secondary': {
        styles.push(buttonStyles.secondary.base)
        if (disabled) styles.push(buttonStyles.secondary.disabled(colors))
        if (isPressed) styles.push(buttonStyles.secondary.pressed)
        break
      }
      case 'error': {
        styles.push(buttonStyles.error.base(colors))
        if (disabled) styles.push(buttonStyles.error.disabled(colors))
        if (isPressed) styles.push(buttonStyles.error.pressed(colors))
        break
      }
    }
    return styles
  }
}

const Button = styled.Pressable<ButtonStyledProps>`
  ${makeButtonStyles}
`
