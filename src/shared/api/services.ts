import { TResponse } from './types'
import { ServiceCategoryItemInstance } from '../../stores/domains/service-categories.store'
import { backend_instance, instance, PARTHER_TOKEN } from '@shared/api/base'
import { ServiceItemInstance } from '../../stores/domains/service.store'

export const getServiceCategoryList = async (clinicId: number, userToken = '') => {
  return await instance.get<TResponse<ServiceCategoryItemInstance[]>>(`company/${clinicId}/service_categories`, {
    headers: {
      Authorization: `Bearer ${PARTHER_TOKEN}, User ${userToken}`,
    },
  })
}

export const getServiceList = async (data: { clinicId: number; categoryId: number; userToken: string }) => {
  return await backend_instance.post<TResponse<ServiceItemInstance[]>>(
    `yclients/company/${data.clinicId}/services/?category_id=${data.categoryId}`,
    {
      userToken: data.userToken,
    },
  )
}
