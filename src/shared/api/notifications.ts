import { backend_instance } from './base'

interface ExponentPushTokenProps {
  phone: string
  userToken: string
  pushToken: string
}

export const sendExponentPushToken = async ({ phone, userToken, pushToken }: ExponentPushTokenProps) =>
  await backend_instance.post(`notify/update`, { userToken, pushToken, phone })
