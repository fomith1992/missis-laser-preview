import * as authAPI from './auth'
import * as backendAPI from './backend'
import * as bookAPI from './book'
import * as geoAPI from './geolocation'
import * as notificationsAPI from './notifications'
import * as promocodeAPI from './promocodes'
import * as recordsAPI from './records'
import * as serviceAPI from './services'
import * as buyCardAPI from './buy-card'

export { recordsAPI, bookAPI, geoAPI, authAPI, notificationsAPI, serviceAPI, promocodeAPI, backendAPI, buyCardAPI }
