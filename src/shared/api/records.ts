import { StaffItemInstance } from 'src/stores/domains/appointment.store'
import { CompanyItemInstance } from 'src/stores/domains/geolocation.store'
import { IUser } from 'src/stores/domains/user/types'
import { instance, PARTHER_TOKEN } from './base'
import { TResponse } from './types'

type TService = {
  id: number
  title: string
  first_cost: number
  discount: number
  cost: number
}

export type TRecord = {
  id: number
  datetime: string
  seance_length: number
  create_date: string
  services: TService[]
  staff: Pick<StaffItemInstance, 'id' | 'name' | 'specialization' | 'avatar'>
  client: Pick<IUser, 'id' | 'phone' | 'name' | 'email'>
  company: CompanyItemInstance
  deleted?: boolean
  comment?: string
}

export const getHistory = (user_token: string, companyId: number) => {
  return instance.get<TResponse<TRecord[]>>(`user/records/?company_id=${companyId}`, {
    headers: {
      Authorization: `Bearer ${PARTHER_TOKEN}, User ${user_token}`,
    },
  })
}

interface IRequestRecordById {
  companyId: number
  recordId: number
  user_token: string
}

export const getRecordById = ({ recordId, user_token, companyId }: IRequestRecordById) => {
  return instance.get<TResponse<TRecord>>(`book_record/${companyId}/${recordId}`, {
    headers: {
      Authorization: `Bearer ${PARTHER_TOKEN}, User ${user_token}`,
    },
  })
}

interface IUpdateRecordById extends IRequestRecordById {
  body: Partial<TRecord>
}

export const updateRecordById = async ({ companyId, recordId, user_token, body }: IUpdateRecordById) => {
  return await instance.put<TResponse<TRecord>>(
    `book_record/${companyId}/${recordId}`,
    { ...body },
    {
      headers: {
        Authorization: `Bearer ${PARTHER_TOKEN}, User ${user_token}`,
      },
    },
  )
}

type TDeleteRecordById = {
  recordId: number
  user_token: string
}

export const deleteRecordById = async ({ recordId, user_token }: TDeleteRecordById) => {
  return await instance.delete(`user/records/${recordId}`, {
    headers: {
      Authorization: `Bearer ${PARTHER_TOKEN}, User ${user_token}`,
    },
  })
}
