import axios from 'axios'

export const GROUP_ID = 211908
export const COMPANY_ID = 291422

const API_BACKEND = 'http://localhost:5000'
const API_PREFIX = 'https://api.yclients.com/api/v1/'

export const PARTHER_TOKEN = '5kda3hw6us9wkds3neyc'

export const instance = axios.create({
  baseURL: API_PREFIX,
  headers: {
    Accept: 'application/vnd.yclients.v2+json',
    Authorization: `Bearer ${PARTHER_TOKEN}`,
  },
})

export const backend_instance = axios.create({
  baseURL: API_BACKEND,
})
