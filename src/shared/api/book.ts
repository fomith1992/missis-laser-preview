import { TZoneIcons } from '@shared/ui/icons/zone.icon'
import { ServiceCategoryItemInstance } from 'src/stores/domains/service-categories.store'
import { instance, PARTHER_TOKEN } from './base'
import { TResponse } from './types'

export interface StaffsResponse {
  avatar: string
  avatar_big: string
  id: number
  name: string
  rating: number
  schedule_till: string
  specialization: string
  status: number
  bookable: boolean
}

interface DatesResponse {
  working_dates: string[]
}

export interface ServiceByServiceIdResponse {
  imageType?: TZoneIcons
  prepaid: string
  image_group: {
    images: {
      basic: {
        path: string
      }
    }
  }
  category_id: number
  id: number
  price_max: number
  price_min: number
  title: string
  staff: {
    id: number
    seance_length: number
  }[][]
}
export interface TimesResponse {
  datetime: string
  seance_length: number
  sum_length: number
  time: string
}

export interface СreateAppointmentResponse {
  0: {
    record_id: number
    record_hash: string
  }
  charge?: {
    url: string
  }
}

export const getStaffs = async (values: number[], clinic_id?: number) =>
  await instance.get<TResponse<StaffsResponse[]>>(
    `book_staff/${clinic_id}?service_ids%5B%5D=${values.join('&service_ids%5B%5D=')}&datetime=&without_seances=1`,
  )

interface IGetDatesByStaffId {
  selectedServices: number[] | null
  companyId: number
  workerId: number
  excludeRecordId?: number
}

export const getDatesByStaffId = async ({
  selectedServices,
  companyId,
  workerId,
  excludeRecordId,
}: IGetDatesByStaffId) => {
  const params = []

  params.push(`service_ids%5B%5D=${selectedServices && selectedServices.join('&service_ids%5B%5D=')}`)
  params.push(`staff_id=${workerId}`)
  if (excludeRecordId !== undefined) params.push(`exclude_record_id=${excludeRecordId}`)

  return await instance.get<TResponse<DatesResponse>>(`book_dates/${companyId}?${params.join('&')}`)
}

interface IGetTimesByStaffId extends IGetDatesByStaffId {
  date: string
}

export const getTimesByStaffId = async ({
  workerId,
  date,
  companyId,
  selectedServices,
  excludeRecordId,
}: IGetTimesByStaffId) => {
  const params = []

  if (selectedServices) params.push(`service_ids%5B%5D=${selectedServices.join('&service_ids%5B%5D=')}`)
  if (excludeRecordId !== undefined) params.push(`exclude_record_id=${excludeRecordId}`)

  return await instance.get<TResponse<TimesResponse[]>>(
    `book_times/${companyId}/${workerId}/${date}?${params.join('&')}`,
  )
}

export const getServiceByServiceId = async (data: { serviceId: number; user_token: string; clinic_id?: number }) =>
  await instance.get<TResponse<ServiceByServiceIdResponse>>(`company/${data.clinic_id}/services/${data.serviceId}`, {
    headers: {
      Authorization: `Bearer ${PARTHER_TOKEN}, User ${data.user_token}`,
    },
  })

interface getServicesProps {
  category: ServiceCategoryItemInstance[]
}

export const getServices = async (data: { clinic_id?: number }) =>
  await instance.get<TResponse<getServicesProps>>(`book_services/${data.clinic_id}`, {})

export const createAppointment = async (data: {
  selectedServices: number[]
  workerId: number
  date: string
  name: string
  phone: string
  email: string | null
  clinic_id?: number
  chargesItems: number[] | null
}) =>
  await instance.post<TResponse<СreateAppointmentResponse>>(`book_record/${data.clinic_id}`, {
    phone: data.phone,
    fullname: data.name,
    email: data.email ?? '',
    notify_by_sms: 24,
    appointments: [
      {
        id: 0,
        services: data.selectedServices,
        staff_id: data.workerId,
        datetime: data.date,
      },
    ],
    appointments_charges: [
      {
        id: 0,
        services: data.chargesItems,
      },
    ],
    is_support_charge: data.chargesItems && data.chargesItems.length > 0,
    isMobile: true,
  })
