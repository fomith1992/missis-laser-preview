import { IUser } from 'src/stores/domains/user/types'

import { COMPANY_ID, instance, PARTHER_TOKEN } from './base'
import { TResponse } from './types'

type TVerifySMSCode = {
  phone: string
  fullname?: string
}

export const verifySMSCode = async (body: TVerifySMSCode) => {
  return await instance.post<TResponse>(`book_code/${COMPANY_ID}`, { ...body })
}

type TAuthByCode = {
  phone: string
  code: string
}

export const authByCode = async ({ phone, code }: TAuthByCode) => {
  return await instance.post<TResponse<IUser>>(`user/auth`, { phone, code })
}

export const changePersonalInfo = async (name: string, user_token: string) => {
  return await instance.put<TResponse<IUser>>(
    `booking/user`,
    { name },
    {
      headers: {
        Authorization: `Bearer ${PARTHER_TOKEN}, User ${user_token}`,
        Accept: 'application/vnd.yclients.v2+json',
      },
    },
  )
}

export const getPersonalInfo = async (user_token: string) => {
  return await instance.get<TResponse<IUser>>(`booking/user/data`, {
    headers: {
      Authorization: `Bearer ${PARTHER_TOKEN}, User ${user_token}`,
      Accept: 'application/vnd.yclients.v2+json',
    },
  })
}
