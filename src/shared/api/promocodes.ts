import { PromocodeLandingInstance } from 'src/stores/domains/appointment.store'

import { backend_instance } from './base'
import { TResponse } from './types'

interface checkSAvailabilityPromocodeProps {
  phone: string
  user_token: string
  key: string
}

interface Promocode {
  landing: PromocodeLandingInstance
  services: number[]
}

export const checkSAvailabilityPromocode = async ({ phone, user_token, key }: checkSAvailabilityPromocodeProps) =>
  await backend_instance.post<TResponse<Promocode>>(`promocode/check`, { userToken: user_token, key, phone })

export const activatePromocode = async ({ phone, user_token, key }: checkSAvailabilityPromocodeProps) => {
  await backend_instance.post<TResponse<Promocode>>(`promocode/activate`, { userToken: user_token, key, phone })
}
