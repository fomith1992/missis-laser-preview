import { TPromoSlide } from '@screens/home/ui/promotion-slider/mock/data'
import { backend_instance, GROUP_ID, instance, PARTHER_TOKEN } from './base'
import { TResponse } from './types'

export interface СreateAppointmentResponse {
  0: {
    record_id: number
    record_hash: string
  }
  charge?: {
    url: string
  }
}

export interface AvailabilityCardData {
  id: number
  is_frozen: boolean
  type: {
    id: number
    title: string
    salon_group_id: number
  }
  programs: [
    {
      id: number
      title: string
    },
  ]
}

export const requestForLink = async (data: {
  name: string
  phone: string
  email: string | null
  chargesItemId: number
}) =>
  await instance.post<TResponse<СreateAppointmentResponse>>(`chain/${GROUP_ID}/online_sale`, {
    chain_bookform_id: 996,
    client: {
      phone: data.phone,
      name: data.name,
      email: data.email ?? 'without@cards.ru',
    },
    items: [
      {
        id: data.chargesItemId,
        type: 'abonement',
        quantity: 1,
      },
    ],
  })

export const availabilityCard = async (data: { user_token: string }) =>
  await instance.get<TResponse<AvailabilityCardData[]>>(`user/loyalty/abonements/?${GROUP_ID}`, {
    headers: {
      Authorization: `Bearer ${PARTHER_TOKEN}, User ${data.user_token}`,
    },
  })

export const getHomeCards = async () => await backend_instance.get<TPromoSlide[]>(`cards/all`)
