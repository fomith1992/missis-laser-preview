import { CompanyItemInstance } from 'src/stores/domains/geolocation.store'

import { GROUP_ID, instance } from './base'
import { TResponse } from './types'

export const getAllCompanies = async () =>
  await instance.get<TResponse<CompanyItemInstance[]>>(`companies/?group_id=${GROUP_ID}&count=1000&forBooking=1`)
/* await instance.get<TResponse<CompanyItemInstance[]>>(`companies/?count=1000&forBooking=1`) */ // if there is only one clinic

export const getCompany = async (companyId?: number) =>
  await instance.get<TResponse<CompanyItemInstance>>(`company/${companyId}`)
