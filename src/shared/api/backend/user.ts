import { IUser } from 'src/stores/domains/user/types'

import { backend_instance } from '../base'

interface IUpdateData extends Partial<IUser> {
  username: string
  userToken: string
}

interface UserBackend {
  username: string
  sex: 0 | 1 | 2
  phone: string
  pushToken: string
  userToken: string
}

export const updateUser = async (data: IUpdateData) => {
  return backend_instance.put('user/data', data)
}

export const fetchUserData = async (username: string) => {
  return backend_instance.post<UserBackend>('user/data', { username })
}
