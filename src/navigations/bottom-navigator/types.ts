import { RoutesTabs } from './routes'

export type BottomTabParamList = {
  [RoutesTabs.TabHome]: undefined
  [RoutesTabs.TabAppointments]: undefined
}
