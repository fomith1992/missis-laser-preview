import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'

import { BottomTabParamList } from './types'
import { HomeIcon24 } from '@shared/ui/icons/home.icon-24'
import { CalendarIcon24 } from '@shared/ui/icons/calendar.icon-24'
import { HomeStackNavigator } from '@navigations/stack-home'
import { AppointmentsStackNavigator } from '@navigations/stack-appointments'
import { RoutesTabs } from './routes'

const Tab = createBottomTabNavigator<BottomTabParamList>()

export function BottomNavigator(): React.ReactElement {
  return (
    <Tab.Navigator
      tabBarOptions={{
        style: {},
        tabStyle: { marginTop: 6 },
        labelStyle: {
          fontWeight: '400',
          lineHeight: 16,
          fontSize: 12,
          marginTop: 2,
          marginBottom: 4,
        },
        showLabel: true,
      }}
    >
      <Tab.Screen
        name={RoutesTabs.TabHome}
        options={{
          tabBarLabel: 'Главная',
          tabBarIcon: (props) => <HomeIcon24 style={{ color: props.color }} />,
        }}
      >
        {() => <HomeStackNavigator />}
      </Tab.Screen>
      <Tab.Screen
        name={RoutesTabs.TabAppointments}
        options={{
          tabBarLabel: 'Мои записи',
          tabBarIcon: (props) => <CalendarIcon24 style={{ color: props.color }} />,
        }}
      >
        {() => <AppointmentsStackNavigator />}
      </Tab.Screen>
    </Tab.Navigator>
  )
}
