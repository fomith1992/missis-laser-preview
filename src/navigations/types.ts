import { StackActionType } from '@react-navigation/routers'
import { RoutesRoot } from 'src/enums/routes'

export type RootStackParamList = {
  [RoutesRoot.BottomNavigator]: undefined

  [RoutesRoot.Contraindications]: { promocode?: boolean }
  [RoutesRoot.ShoppingCart]: undefined
  [RoutesRoot.Login]: undefined
  [RoutesRoot.ZonesSelect]: undefined
  [RoutesRoot.SignUp]: undefined
  [RoutesRoot.SignUpPassword]: undefined
  [RoutesRoot.SexSelect]: undefined
  [RoutesRoot.SignIn]: undefined
  [RoutesRoot.RecoveryPhone]: undefined
  [RoutesRoot.RecoverySMSConfirm]: undefined
  [RoutesRoot.RecoveryPassword]: undefined
  [RoutesRoot.SuccessPasswordRecovery]: undefined

  [RoutesRoot.ProcedureSelectDiod]: undefined
  [RoutesRoot.ProcedureSelectAlexandrit]: undefined
  [RoutesRoot.ProcedureListCosmetology]: undefined

  [RoutesRoot.SuccessAppointmentDelete]: undefined
  [RoutesRoot.SuccessRegistration]: undefined
  [RoutesRoot.PromoBanner]: undefined
  [RoutesRoot.EventScreen]: { itemId: number; minPrice: number }

  [RoutesRoot.SuccessAppointment]: { companyId?: number; appointmentId?: number }
  [RoutesRoot.SuccessBuyingCard]: undefined
  [RoutesRoot.SelectClinic]: { city: number; isShortcut: boolean }
  [RoutesRoot.WorkerSelect]: { selectedServices: number[] }
  [RoutesRoot.DiodeLaserProcedure]: { minPrice: number }
  [RoutesRoot.AlexLaserProcedure]: { minPrice: number }
  [RoutesRoot.PromoCodeLanding]: undefined
  [RoutesRoot.MillerCardLanding]: { type: string; itemId: number; itemPrice: number }
  [RoutesRoot.PromoCode]: { isFromMain: boolean }
  [RoutesRoot.SelectCity]: { isShortcut: boolean }
  [RoutesRoot.SignUpSMSConfirm]: {
    phoneNumber: string
    fullname?: string
    stackAction: Extract<StackActionType, { type: 'REPLACE' }>
  }
  [RoutesRoot.DateTimeSelect]: {
    selectedServices: number[]
    workerId: number
    isMoveRecord?: boolean
    excludeRecordId?: number
    companyId?: number
    appointmentId?: number
  }
}
