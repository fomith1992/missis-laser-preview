import React from 'react'

import { createStackScreen, RootStack } from '@navigations/stack-root/creatorScreens'
import * as Screens from '@screens/index'
import { Header } from '@shared/ui/header/header'

import { RoutesRoot } from 'src/enums/routes'
import { useRootStore } from 'src/stores/lib/use-root-store'

import { BottomNavigator } from '../bottom-navigator'

export function RootNavigator(): React.ReactElement {
  const isAuthenticated = useRootStore().user.user_token

  console.log(isAuthenticated)

  return (
    <RootStack.Navigator
      screenOptions={{ header: (props) => <Header headerProps={props} /> }}
      initialRouteName={isAuthenticated ? RoutesRoot.BottomNavigator : RoutesRoot.PromoBanner}
    >
      {createStackScreen(RoutesRoot.PromoBanner, Screens.PromoBannerScreen, {
        headerShown: false,
      })}
      {createStackScreen(RoutesRoot.BottomNavigator, BottomNavigator, {
        headerShown: false,
      })}
      {createStackScreen(RoutesRoot.DiodeLaserProcedure, Screens.DiodeLaserProcedureScreen, {
        headerShown: false,
      })}
      {createStackScreen(RoutesRoot.AlexLaserProcedure, Screens.AlexLaserProcedureScreen, {
        headerShown: false,
      })}
      {createStackScreen(RoutesRoot.PromoCodeLanding, Screens.PromoCodeLandingScreen, {
        headerShown: false,
      })}
      {createStackScreen(RoutesRoot.MillerCardLanding, Screens.MillerCardScreen, {
        headerShown: false,
      })}
      {createStackScreen(RoutesRoot.ProcedureSelectDiod, Screens.ProcedureSelectDiodScreen, {
        title: 'Выбор процедуры',
      })}
      {createStackScreen(RoutesRoot.ProcedureSelectAlexandrit, Screens.ProcedureSelectAlexandritScreen, {
        title: 'Выбор процедуры',
      })}
      {createStackScreen(RoutesRoot.ProcedureListCosmetology, Screens.ProcedureListCosmetologyScreen, {
        title: 'Выбор процедуры',
      })}
      {createStackScreen(RoutesRoot.SuccessAppointment, Screens.SuccessAppointmentScreen, { headerShown: false })}
      {createStackScreen(RoutesRoot.SuccessBuyingCard, Screens.SuccessBuyingCardScreen, { headerShown: false })}
      {createStackScreen(RoutesRoot.Login, Screens.LoginScreen, {
        headerShown: false,
      })}
      {createStackScreen(RoutesRoot.SelectCity, Screens.SelectCityScreen, {
        title: 'Выберите город',
      })}
      {createStackScreen(RoutesRoot.SuccessRegistration, Screens.SuccessRegistrationScreen, { headerShown: false })}
      {createStackScreen(RoutesRoot.SexSelect, Screens.SexSelectScreen, {
        title: 'Выберите пол',
      })}
      {createStackScreen(RoutesRoot.Contraindications, Screens.ContraindicationsScreen, {
        title: 'Противопоказания',
      })}
      {createStackScreen(RoutesRoot.ZonesSelect, Screens.ZonesSelectScreen, {
        title: 'Выберите зоны',
      })}
      {createStackScreen(RoutesRoot.WorkerSelect, Screens.WorkerSelectScreen, {
        title: 'Выберите сотрудника',
      })}
      {createStackScreen(RoutesRoot.DateTimeSelect, Screens.DateTimeSelectScreen, {
        title: 'Дата и время',
      })}
      {createStackScreen(RoutesRoot.ShoppingCart, Screens.ShoppingCartScreen, {
        title: 'Ваша запись',
      })}
      {createStackScreen(RoutesRoot.SignUp, Screens.SignUpScreen, {
        title: 'Регистрация',
      })}
      {createStackScreen(RoutesRoot.SignUpPassword, Screens.SignUpPasswordScreen, {
        title: 'Придумайте пароль',
      })}
      {createStackScreen(RoutesRoot.SignUpSMSConfirm, Screens.SignUpSMSConfirmScreen, {
        title: 'Введите код из смс',
      })}
      {createStackScreen(RoutesRoot.SignIn, Screens.SignInScreen, {
        title: 'Вход в аккаунт',
      })}
      {createStackScreen(RoutesRoot.PromoCode, Screens.PromoCodeScreen, {
        title: 'У вас есть промокод?',
      })}
      {createStackScreen(RoutesRoot.SelectClinic, Screens.SelectClinicScreen, {
        title: 'Выберите клинику',
      })}

      {createStackScreen(RoutesRoot.RecoveryPhone, Screens.RecoveryPhoneScreen, {
        title: 'Восстановить пароль',
      })}

      {createStackScreen(RoutesRoot.RecoverySMSConfirm, Screens.RecoverySMSConfirmScreen, {
        title: 'Введите код из смс',
      })}

      {createStackScreen(RoutesRoot.RecoveryPassword, Screens.RecoveryPasswordScreen, {
        title: 'Придумайте пароль',
      })}
      {createStackScreen(RoutesRoot.EventScreen, Screens.EventScreen, {
        headerShown: false,
      })}

      {createStackScreen(RoutesRoot.SuccessPasswordRecovery, Screens.SuccessRecoveryScreen, { headerShown: false })}
      {createStackScreen(RoutesRoot.SuccessAppointmentDelete, Screens.SuccessAppoinmentDeleteScreen, {
        headerShown: false,
      })}
    </RootStack.Navigator>
  )
}
