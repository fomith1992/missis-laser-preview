import React from 'react'

import { RoutesRoot } from 'src/enums/routes'
import { createStackNavigator, StackNavigationOptions } from '@react-navigation/stack'
import { RootStackParamList } from '../types'

export const RootStack = createStackNavigator<RootStackParamList>()

export function createStackScreen(
  Route: keyof RootStackParamList,
  Component: React.ElementType,
  options: StackNavigationOptions = {},
) {
  return (
    <RootStack.Screen options={options} name={RoutesRoot[Route]}>
      {(props) => <Component {...props} />}
    </RootStack.Screen>
  )
}
