import React, { FC } from 'react'

import { DefaultTheme, NavigationContainer as LibContainer, NavigationContainerRef } from '@react-navigation/native'
import { RootStackParamList } from '@navigations/types'

const AppTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: 'white',
  },
}

type Props = {
  navigationRef?: React.Ref<NavigationContainerRef<RootStackParamList>> | undefined
}

export const NavigationContainer: FC<Props> = ({ children, navigationRef }): React.ReactElement => {
  return (
    <LibContainer theme={AppTheme} ref={navigationRef}>
      {children}
    </LibContainer>
  )
}
