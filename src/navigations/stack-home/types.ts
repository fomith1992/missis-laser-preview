import { RoutesHome } from './routes'

export type HomeStackParamList = {
  [RoutesHome.Home]: undefined
  [RoutesHome.Settings]: undefined
  [RoutesHome.ProfileSettings]: undefined
}
