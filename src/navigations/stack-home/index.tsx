import React from 'react'

import { createStackNavigator } from '@react-navigation/stack'

import { HomeStackParamList } from './types'
import { RoutesHome } from './routes'
import { Header } from '@shared/ui/header/header'

import * as Screens from '@screens/index'

export const HomeStack = createStackNavigator<HomeStackParamList>()

export const HomeStackNavigator = () => {
  return (
    <HomeStack.Navigator screenOptions={{ header: (props) => <Header headerProps={props} /> }}>
      <HomeStack.Screen options={{ title: '' }} name={RoutesHome.Home}>
        {() => <Screens.HomeScreen />}
      </HomeStack.Screen>
      <HomeStack.Screen options={{ title: 'Настройки' }} name={RoutesHome.Settings}>
        {() => <Screens.SettingsScreen />}
      </HomeStack.Screen>
      <HomeStack.Screen options={{ title: 'Профиль' }} name={RoutesHome.ProfileSettings}>
        {() => <Screens.ProfileSettings />}
      </HomeStack.Screen>
    </HomeStack.Navigator>
  )
}
