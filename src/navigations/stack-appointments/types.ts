import { TRecord } from '@shared/api/records'
import { RoutesAppointments } from './routes'

export type AppointmentStackParamList = {
  [RoutesAppointments.AppointmentsHistory]: undefined
  [RoutesAppointments.Appointment]: {
    companyId: number
    appointmentId: number
    appointment?: TRecord
    showBottomSheet?: boolean
  }
}
