import React from 'react'

import { createStackNavigator } from '@react-navigation/stack'

import { AppointmentStackParamList } from './types'
import { RoutesAppointments } from './routes'
import { Header } from '@shared/ui/header/header'

import * as Screens from '@screens/index'

export const AppointmentsStack = createStackNavigator<AppointmentStackParamList>()

export const AppointmentsStackNavigator = () => {
  return (
    <AppointmentsStack.Navigator screenOptions={{ header: (props) => <Header headerProps={props} /> }}>
      <AppointmentsStack.Screen options={{ title: 'Записи' }} name={RoutesAppointments.AppointmentsHistory}>
        {() => <Screens.AppointmentsHistoryScreen />}
      </AppointmentsStack.Screen>
      <AppointmentsStack.Screen options={{ title: '' }} name={RoutesAppointments.Appointment}>
        {(props) => <Screens.AppoinmentScreen {...props} />}
      </AppointmentsStack.Screen>
    </AppointmentsStack.Navigator>
  )
}
