import React from 'react'

import { ProcedureListDiod } from '@screens/procedure-select/lib/procedure‎-diod.list.container'
import { ProcedureListAlexandrit } from './lib/procedure‎-alexandrit.list.container'
import { ProcedureListCosmetology } from './lib/procedure-cosmetology.list.container'

export const ProcedureSelectDiodScreen = () => {
  return <ProcedureListDiod />
}

export const ProcedureSelectAlexandritScreen = () => {
  return <ProcedureListAlexandrit />
}

export const ProcedureListCosmetologyScreen = () => {
  return <ProcedureListCosmetology />
}
