import { ServiceCategoryItemInstance } from 'src/stores/domains/service-categories.store'

export const detectIsAlexLaser = (service: ServiceCategoryItemInstance) => {
  return service.title.toLowerCase().includes('александритовый')
}

export const detectIsCosmetology = (service: ServiceCategoryItemInstance) => {
  return service.title.toLowerCase().includes('косметология')
}
