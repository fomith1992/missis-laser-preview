import React, { useMemo } from 'react'
import useAsync from 'react-use/lib/useAsync'
import useEffectOnce from 'react-use/lib/useEffectOnce'

import { AxiosError } from 'axios'
import { observer } from 'mobx-react'

import { ProcedureListView } from '@screens/procedure-select/ui/procedure.list.view'
import { bookAPI, buyCardAPI, recordsAPI, serviceAPI } from '@shared/api'
import { TResponse } from '@shared/api/types'
import { LoadingIndicator } from '@shared/ui/atoms/loading-indicator'

import { useRootStore } from '../../../stores/lib/use-root-store'
import { historyToSectionList } from '@screens/appointments-history/lib/data.transform'

export const ProcedureListAlexandrit = observer((): React.ReactElement => {
  const { setServiceCategoryListData, serviceCategoryListData, clearStore } = useRootStore().serviceCategoryListReport
  const { currentClinicId } = useRootStore().geolocationReport
  const { user_token, sex, phone } = useRootStore().user
  const { reset } = useRootStore().appointmentReport

  useEffectOnce(() => reset())

  const rawHistory = useAsync(async () => {
    if (!user_token || sex !== 2) return
    return await recordsAPI
      .getHistory(user_token, currentClinicId ?? 0)
      .then(({ data: { data } }) => data)
      .catch((e: AxiosError<TResponse>) => console.log(e.response?.data))
  }, [user_token, sex])

  const historyLenght = useMemo(() => historyToSectionList(rawHistory.value || []), [rawHistory.value]).length

  console.log(historyLenght) //! del

  const servicesState = useAsync(
    async () =>
      await Promise.all(
        serviceCategoryListData.map(
          async (item) =>
            (
              await serviceAPI.getServiceList({
                categoryId: item.id,
                clinicId: currentClinicId ?? 0,
                userToken: user_token ?? '',
              })
            ).data.data.sort((a, b) => a.price_min - b.price_min)[0],
        ),
      ),
    [serviceCategoryListData.toString()],
  )

  const queryState = useAsync(async () => {
    if (currentClinicId) {
      clearStore()
      return await bookAPI
        .getServices({ clinic_id: currentClinicId })
        .then(({ data: { data } }) => {
          setServiceCategoryListData(
            data.category.filter((item) => !/акция/i.test(item.title) && /DEKA/i.test(item.title)),
          )
        })
        .catch((e: AxiosError<TResponse>) => console.log('getServices error ', e))
    }
  }, [currentClinicId])

  const availabilityCard = useAsync(async () => {
    if (currentClinicId && phone && user_token) {
      return await buyCardAPI.availabilityCard({ user_token })
    } else return null
  }, [currentClinicId, phone, user_token])

  const isThereLoyalityCard = (): boolean => {
    return false
    /* if (
      availabilityCard?.value?.data.data &&
      availabilityCard.value.data.data.length > 0 &&
      availabilityCard.value.data.data.find((x) => (x.type.id === 265847 || x.type.id === 265860) && !x.is_frozen) !=
        null
    ) {
      return true
    } else {
      return false
    } */
  }

  if (queryState.loading || servicesState.loading || availabilityCard.loading || rawHistory.loading) {
    return <LoadingIndicator visible />
  } else {
    return (
      <ProcedureListView
        categoryData={serviceCategoryListData
          .filter((item) => (isThereLoyalityCard() ? /карте/i.test(item.title) : !/карте/i.test(item.title)))
          .filter((item) =>
            sex === 0
              ? item.sex !== undefined
              : sex === 2
              ? historyLenght > 0
                ? /жен/i.test(item.title) && !/75/i.test(item.title)
                : /жен/i.test(item.title) && /75/i.test(item.title)
              : /муж/i.test(item.title),
          )}
        categoriesPriceMin={
          servicesState.value?.map((item) => ({
            categoryId: item.category_id,
            price_min: item.price_min,
          })) ?? []
        }
      />
    )
  }
})
