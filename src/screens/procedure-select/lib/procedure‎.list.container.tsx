import React from 'react'
import useAsync from 'react-use/lib/useAsync'
import useEffectOnce from 'react-use/lib/useEffectOnce'

import { AxiosError } from 'axios'
import { observer } from 'mobx-react'

import { ProcedureListView } from '@screens/procedure-select/ui/procedure.list.view'
import { bookAPI, buyCardAPI, serviceAPI } from '@shared/api'
import { TResponse } from '@shared/api/types'
import { LoadingIndicator } from '@shared/ui/atoms/loading-indicator'

import { useRootStore } from '../../../stores/lib/use-root-store'

export const ProcedureList = observer((): React.ReactElement => {
  const { setServiceCategoryListData, serviceCategoryListData, clearStore } = useRootStore().serviceCategoryListReport
  const { currentClinicId } = useRootStore().geolocationReport
  const { user_token, sex, phone } = useRootStore().user
  const { reset } = useRootStore().appointmentReport

  useEffectOnce(() => reset())

  const servicesState = useAsync(
    async () =>
      await Promise.all(
        serviceCategoryListData.map(
          async (item) =>
            (
              await serviceAPI.getServiceList({
                categoryId: item.id,
                clinicId: currentClinicId ?? 0,
                userToken: user_token ?? '',
              })
            ).data.data.sort((a, b) => a.price_min - b.price_min)[0],
        ),
      ),
    [serviceCategoryListData.toString()],
  )

  const queryState = useAsync(async () => {
    if (currentClinicId) {
      clearStore()
      return await bookAPI
        .getServices({ clinic_id: currentClinicId })
        .then(({ data: { data } }) => {
          setServiceCategoryListData(data.category.filter((item) => !/акция/i.test(item.title)))
        })
        .catch((e: AxiosError<TResponse>) => console.log('getServices error ', e))
    }
  }, [currentClinicId])

  const availabilityCard = useAsync(async () => {
    if (currentClinicId && phone && user_token) {
      return await buyCardAPI.availabilityCard({ user_token })
    } else return null
  }, [currentClinicId, phone, user_token])

  const isThereLoyalityCard = (): boolean => {
    if (
      availabilityCard?.value?.data.data &&
      availabilityCard.value.data.data.length > 0 &&
      availabilityCard.value.data.data.find((x) => (x.type.id === 265847 || x.type.id === 265860) && !x.is_frozen) !=
        null
    ) {
      return true
    } else {
      return false
    }
  }

  if (queryState.loading || servicesState.loading || availabilityCard.loading) {
    return <LoadingIndicator visible />
  } else {
    return (
      <ProcedureListView
        categoryData={serviceCategoryListData
          .filter((item) => (isThereLoyalityCard() ? /карте/i.test(item.title) : !/карте/i.test(item.title)))
          .filter((item) => (sex === 0 ? item.sex !== undefined : item.sex === sex || item.sex === 0))}
        categoriesPriceMin={
          servicesState.value?.map((item) => ({
            categoryId: item.category_id,
            price_min: item.price_min,
          })) ?? []
        }
      />
    )
  }
})
