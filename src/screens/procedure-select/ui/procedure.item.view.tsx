import React, { ReactElement } from 'react'
import { TouchableOpacity } from 'react-native'
import styled from '@emotion/native'

import { Body, Footnote, RowView } from '@shared/ui'
import { ServiceCategoryItemInstance } from 'src/stores/domains/service-categories.store'

import alexLaserImage from '@entities/procedure/ui/alex-laser.large.png'
import diodeLaserImage from '@entities/procedure/ui/diode-laser.large.png'
import cosmetologyImage from '@entities/procedure/ui/missis-laser.large.png'
import { detectIsAlexLaser, detectIsCosmetology } from '../lib/helpers'
import { RoutesRoot } from 'src/enums/routes'
import { StackActions, useNavigation } from '@react-navigation/native'
import { useRootStore } from 'src/stores/lib/use-root-store'

interface ProcedureItemProps {
  item: ServiceCategoryItemInstance
  categoryPriceMin: number
}

const diodeLaserDescription = 'Менее болезненно и подходит для любого цвета кожи'
const alexLaserDescription = 'Моментальный эффект. Для тёмных волос и светлой кожи'
const cosmetologyDescription = 'Красота без хирургического вмешательства'

export const ProcedureItem = ({ item, categoryPriceMin }: ProcedureItemProps): ReactElement => {
  const nav = useNavigation()
  const { setCurrentCategoryId } = useRootStore().serviceCategoryListReport

  const isAlexLaser = detectIsAlexLaser(item)
  const isCosmetology = detectIsCosmetology(item)

  const pressHandler = () => {
    setCurrentCategoryId(item.id)
    const nextRoute = isAlexLaser
      ? RoutesRoot.AlexLaserProcedure
      : isCosmetology
      ? RoutesRoot.ZonesSelect
      : RoutesRoot.DiodeLaserProcedure
    nav.dispatch(StackActions.push(nextRoute, { minPrice: categoryPriceMin }))
  }

  return (
    <TouchableOpacity onPress={pressHandler}>
      <RowView mt={24}>
        <Image source={isAlexLaser ? alexLaserImage : isCosmetology ? cosmetologyImage : diodeLaserImage} />
        <Info>
          <Body bold>{item.title}</Body>
          <Footnote>
            {isAlexLaser ? alexLaserDescription : isCosmetology ? cosmetologyDescription : diodeLaserDescription}
          </Footnote>
          <PriceButton>
            <Footnote color="primaryText">{`от ${categoryPriceMin} ₽`}</Footnote>
          </PriceButton>
        </Info>
      </RowView>
    </TouchableOpacity>
  )
}

const Image = styled.Image`
  width: 88px;
  height: 88px;
  border-radius: 16px;
`

const Info = styled.View`
  margin-left: 16px;
  flex: 1;
  justify-content: space-between;
`

const PriceButton = styled.View`
  margin-top: 8px;
  padding: 8px 12px;
  border: 1px solid #000;
  border-radius: 30px;
  align-self: flex-start;
`
