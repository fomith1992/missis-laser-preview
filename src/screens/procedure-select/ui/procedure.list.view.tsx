import React from 'react'
import { ServiceCategoryItemInstance } from '../../../stores/domains/service-categories.store'
import { ProcedureItem } from '@screens/procedure-select/ui/procedure.item.view'
import { ScrollView } from 'react-native'
import { ContentLayout } from '@shared/ui'

interface ProcedureListProps {
  categoryData?: ServiceCategoryItemInstance[]
  categoriesPriceMin: {
    categoryId: number
    price_min: number
  }[]
}

export const ProcedureListView = ({ categoryData, categoriesPriceMin }: ProcedureListProps): React.ReactElement => {
  const categoryPriceMin = (item: ServiceCategoryItemInstance) => {
    const value = categoriesPriceMin?.find((x) => x.categoryId === item.id)
    return value != null ? value.price_min : 0
  }

  return (
    <ScrollView>
      <ContentLayout flex mb={24}>
        {categoryData?.map((item, id) => (
          <ProcedureItem key={id} item={item} categoryPriceMin={categoryPriceMin(item)} />
        ))}
      </ContentLayout>
    </ScrollView>
  )
}
