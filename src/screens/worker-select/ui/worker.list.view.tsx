import React from 'react'
import { ScrollView } from 'react-native'

import { IWorker } from '@entities/worker/model/types'
import { BaseView, ContentLayout } from '@shared/ui'

import { WorkerCardView } from '@entities/worker'

interface Props {
  data: IWorker[]
  onItemPress?: (item: IWorker) => void
}

export const WorkerListView = ({ data, onItemPress }: Props) => {
  return (
    <ScrollView>
      <ContentLayout mt={20}>
        {data.map((item, idx) => (
          <BaseView key={idx} mb={20}>
            <WorkerCardView imageSize={88} {...item} onPress={onItemPress} />
          </BaseView>
        ))}
      </ContentLayout>
    </ScrollView>
  )
}
