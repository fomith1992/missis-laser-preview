import { RootStackParamList } from '@navigations/types'
import { StackScreenProps } from '@react-navigation/stack'
import React from 'react'
import { RoutesRoot } from 'src/enums/routes'

import { WorkerSelect } from './lib/worker-select.container'

export const WorkerSelectScreen = ({ route }: StackScreenProps<RootStackParamList, RoutesRoot.WorkerSelect>) => {
  const { selectedServices } = route.params

  return <WorkerSelect selectedServices={selectedServices} />
}
