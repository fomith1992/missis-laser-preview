import React from 'react'
import { IWorker } from '@entities/worker/model/types'

import { StackActions, useNavigation } from '@react-navigation/core'
import { RoutesRoot } from 'src/enums/routes'
import { WorkerListView } from '../ui/worker.list.view'
import useAsync from 'react-use/lib/useAsync'
import { bookAPI } from '@shared/api'
import { LoadingIndicator } from '@shared/ui/atoms/loading-indicator'
import { useRootStore } from 'src/stores/lib/use-root-store'
import { observer } from 'mobx-react'
import * as Analytics from 'expo-firebase-analytics'

interface WorkerSelectProps {
  selectedServices: number[]
}

export const WorkerSelect = observer(({ selectedServices }: WorkerSelectProps) => {
  const nav = useNavigation()
  const { setStaffsData } = useRootStore().appointmentReport
  const { currentClinicId } = useRootStore().geolocationReport

  useAsync(
    async () =>
      await Analytics.logEvent('WorkerSelect', {
        screen: 'WorkerSelect',
      }),
    [],
  )

  const queryState = useAsync(async () => {
    return await (
      await bookAPI.getStaffs(selectedServices, currentClinicId)
    ).data.data
  }, [selectedServices])

  const onPress = (worker: IWorker) => {
    setStaffsData(queryState.value ?? [])
    nav.dispatch(
      StackActions.push(RoutesRoot.DateTimeSelect, {
        selectedServices: selectedServices,
        workerId: worker.id,
      }),
    )
  }

  if (queryState.loading) {
    return <LoadingIndicator visible />
  }

  return (
    <WorkerListView
      data={
        queryState.value?.map(({ avatar_big, id, name, specialization, bookable }) => ({
          name,
          id,
          image: avatar_big,
          specialization,
          bookable,
        })) ?? []
      }
      onItemPress={onPress}
    />
  )
})
