import React from 'react'
import { StackActions, useNavigation } from '@react-navigation/core'
import styled from '@emotion/native'

import { BaseView, ColView, Headiline, RowView } from '@shared/ui'
import { ContentLayout } from '@shared/ui/layouts/content-layout'
import { TextButton } from '@shared/ui/buttons/text-button.view'

import { RoutesRoot } from 'src/enums/routes'
import { useRootStore } from 'src/stores/lib/use-root-store'
import useAsync from 'react-use/lib/useAsync'
import * as Analytics from 'expo-firebase-analytics'
import { PersonCircleIcon260 } from '@shared/ui/icons/person-circle.icon-260'
import { color } from 'src/style/mixins/color'

export const LoginScreen = () => {
  const nav = useNavigation()

  useAsync(
    async () =>
      await Analytics.logEvent('LoginScreen', {
        screen: 'LoginScreen',
      }),
    [],
  )

  const {
    registerReport: { clearRegisterStore },
    smsVerify: { clearSMSVefifyStore },
  } = useRootStore()

  const signInHandler = () => {
    clearSMSVefifyStore()
    nav.dispatch(StackActions.push(RoutesRoot.SignIn))
  }

  const signUpHandler = () => {
    clearRegisterStore()
    clearSMSVefifyStore()
    nav.dispatch(StackActions.push(RoutesRoot.SignUp))
  }

  return (
    <Container>
      <LogoContainer>
        <PersonCircleSvg />
      </LogoContainer>
      <Headiline>
        Вы новый клиент{'\n'}или уже были у нас{'\n'}на процедурах?
      </Headiline>
      <ButtonsContainer mt={28}>
        <ButtonContainer>
          <TextButton type="secondary" onPress={signInHandler}>
            Уже была у вас
          </TextButton>
        </ButtonContainer>
        <BaseView mr={12} />
        <ButtonContainer>
          <TextButton onPress={signUpHandler}>Новый клиент</TextButton>
        </ButtonContainer>
      </ButtonsContainer>
    </Container>
  )
}

const Container = styled(ContentLayout)`
  flex: 1;
  justify-content: center;
  margin-top: 16px;
  margin-bottom: 60px;
`

const LogoContainer = styled(ColView)`
  margin-top: auto;
  margin-bottom: auto;
  align-items: center;
`

const ButtonsContainer = styled(RowView)`
  width: 100%;
`

const PersonCircleSvg = styled(PersonCircleIcon260)`
  flex: 1;
  color: ${color('primary')};
`

const ButtonContainer = styled(BaseView)`
  flex: 1;
`
