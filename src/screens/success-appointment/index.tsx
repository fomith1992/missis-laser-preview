import React from 'react'
import useAsync from 'react-use/lib/useAsync'

import * as Analytics from 'expo-firebase-analytics'

import { useNavigation } from '@react-navigation/core'
import { CommonActions } from '@react-navigation/native'
import { StackScreenProps } from '@react-navigation/stack'

import { ProccessStateView } from '@features/proccess-state/proccess-state.view'
import { RoutesAppointments } from '@navigations/stack-appointments/routes'
import { RootStackParamList } from '@navigations/types'
import { SafeAreaView } from '@shared/ui'

import { RoutesRoot } from 'src/enums/routes'

import { RoutesTabs } from '../../navigations/bottom-navigator/routes'

export function SuccessAppointmentScreen({
  route: { params },
}: StackScreenProps<RootStackParamList, RoutesRoot.SuccessAppointment>): React.ReactElement {
  const navigation = useNavigation()

  useAsync(
    async () =>
      await Analytics.logEvent('SuccessAppointmentScreen', {
        screen: 'SuccessAppointmentScreen',
      }),
    [],
  )

  return (
    <SafeAreaView>
      <ProccessStateView
        title="Вы записаны"
        description={`Мы пришлем вам напоминание за${'\n'}день до процедуры`}
        buttonText="Спасибо, понятно"
        onPress={() =>
          navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [
                {
                  name: RoutesRoot.BottomNavigator,
                  params: {
                    screen: RoutesTabs.TabAppointments,
                    initial: false,
                    params: {
                      initial: false,
                      screen: RoutesAppointments.Appointment,
                      params: {
                        appointmentId: params.appointmentId,
                        companyId: params.companyId,
                        showBottomSheet: true,
                      },
                    },
                  },
                },
              ],
            }),
          )
        }
      />
    </SafeAreaView>
  )
}
