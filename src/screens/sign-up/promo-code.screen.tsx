import React, { useState } from 'react'
import useAsyncFn from 'react-use/lib/useAsyncFn'

import { AxiosError } from 'axios'
import { observer } from 'mobx-react'

import { StackActions, useNavigation } from '@react-navigation/core'
import { StackScreenProps } from '@react-navigation/stack'

import { RootStackParamList } from '@navigations/types'
import { PromoCodeView } from '@screens/sign-up/ui/promo-code.view'
import { promocodeAPI } from '@shared/api'
import { LoadingIndicator } from '@shared/ui/atoms/loading-indicator'

import { RoutesRoot } from 'src/enums/routes'
import { useRootStore } from 'src/stores/lib/use-root-store'

export const PromoCodeScreen = observer(({ route }: StackScreenProps<RootStackParamList, RoutesRoot.PromoCode>) => {
  const { setCurrentServices, setPromocodeLanding, setPromocode } = useRootStore().appointmentReport
  const { phone, user_token } = useRootStore().user
  const navigation = useNavigation()
  const [promocodeError, setPromocodeError] = useState<string | undefined>(undefined)

  const [queryState, onSendQuery] = useAsyncFn(
    async (key: string) => {
      if (phone && user_token) {
        await promocodeAPI
          .checkSAvailabilityPromocode({
            phone,
            user_token,
            key,
          })
          .then(({ data: { data } }) => {
            setCurrentServices(data.services)
            setPromocodeLanding(data.landing)
            setPromocode(key)
            navigation.dispatch(StackActions.push(RoutesRoot.PromoCodeLanding))
          })
          .catch((e: AxiosError<{ error: string; success: boolean }>) => {
            const msg = e?.response?.data?.error
            if (msg === 'Promocode not exist') {
              setPromocodeError('Введённый промокод не существует')
            } else {
              setPromocodeError('Введённый промокод уже активирован')
            }

            console.log('checkSAvailabilityPromocode error ', e.response?.data)
          })
      } else {
        console.log('Недостаточно данных для отправки запроса')
      }
    },
    [user_token, phone],
  )

  const rejectHandler = () => {
    if (route?.params?.isFromMain) {
      navigation.dispatch(StackActions.push(RoutesRoot.BottomNavigator))
    } else {
      navigation.dispatch(StackActions.push(RoutesRoot.SuccessRegistration))
    }
  }

  return (
    <>
      <PromoCodeView
        setPromocodeError={() => setPromocodeError(undefined)}
        error={promocodeError}
        onSubmit={onSendQuery}
        onReject={rejectHandler}
      />
      <LoadingIndicator visible={queryState.loading} />
    </>
  )
})
