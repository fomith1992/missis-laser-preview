import { useNavigation } from '@react-navigation/core'
import { StackActions } from '@react-navigation/routers'
import React from 'react'
import { RoutesRoot } from 'src/enums/routes'
import { SignUpFioPhoneView } from '../ui/fio-phone.view'
import { observer } from 'mobx-react'
import { useRootStore } from 'src/stores/lib/use-root-store'

export const SignUpFio = observer(() => {
  const navigation = useNavigation()
  const { userName, phoneNumber, setUserName, setPhoneNumber } = useRootStore().registerReport

  return (
    <SignUpFioPhoneView
      userName={userName}
      phoneNumber={phoneNumber}
      onLogin={() => navigation.dispatch(StackActions.push(RoutesRoot.SignIn))}
      onPress={(values) => {
        setUserName(values.userName)
        setPhoneNumber(values.phoneNumber)

        navigation.dispatch(
          StackActions.push(RoutesRoot.SignUpSMSConfirm, {
            phoneNumber: values.phoneNumber,
            fullname: values.userName,
            stackAction: StackActions.replace(RoutesRoot.SexSelect),
          }),
        )
      }}
    />
  )
})
