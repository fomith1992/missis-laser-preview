import { StackActions, useNavigation } from '@react-navigation/core'
import { observer } from 'mobx-react'
import React from 'react'
import { RoutesRoot } from 'src/enums/routes'
import { useRootStore } from 'src/stores/lib/use-root-store'
import { SignUpPasswordView } from '../ui/password.view'

export const SignUpPassword = observer(() => {
  const navigation = useNavigation()
  const { password, setPassword, phoneNumber } = useRootStore().registerReport

  return (
    <SignUpPasswordView
      initPassword={password}
      onPress={(pass) => {
        setPassword(pass)
        navigation.dispatch(StackActions.push(RoutesRoot.SignUpSMSConfirm, { phoneNumber }))
      }}
    />
  )
})
