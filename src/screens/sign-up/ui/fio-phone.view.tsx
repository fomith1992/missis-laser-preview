import React from 'react'
import * as yup from 'yup'
import { ColView, ContentLayout, PhoneMaskInput, TextButton, TextInput } from '@shared/ui'
import { Credentionals } from './credentionals'
import { Formik } from 'formik'
import { KeyboardAvoidingViews } from '@shared/ui/layouts/keyboard-avoiding-view'

interface InitialValues {
  userName?: string
  phoneNumber?: string
}

interface SignUpFioPhoneViewProps {
  userName?: string
  phoneNumber?: string
  onPress: (data: InitialValues) => void
  onLogin: () => void
}

const ERROR_PHONE_MESSAGE = 'Введенный номер некорректен. Проверьте и исправьте его, пожалуйста.'

const signinSchema = yup
  .object({
    phoneNumber: yup
      .string()
      .matches(/\(\d{3}\) \d{3}-\d\d-\d\d/, ERROR_PHONE_MESSAGE)
      .required(ERROR_PHONE_MESSAGE),
    userName: yup.string().required('Введите имя и фамилию пользователя'),
  })
  .required()

export const SignUpFioPhoneView = ({ onPress, userName, phoneNumber /* onLogin */ }: SignUpFioPhoneViewProps) => {
  return (
    <KeyboardAvoidingViews>
      <Formik
        initialValues={{ userName, phoneNumber }}
        onSubmit={onPress}
        validationSchema={signinSchema}
        isInitialValid={false}
      >
        {({ handleSubmit, handleChange, setFieldError, values, errors, isValid, submitCount }) => (
          <ContentLayout flex mb={36}>
            <ColView mt="auto" mb="auto">
              <TextInput
                focusOnMount
                maxLength={30}
                value={values.userName}
                onChangeText={handleChange('userName')}
                onFocus={() => setFieldError('userName', undefined)}
                placeholder="Имя и фамилия"
                errorMessage={submitCount > 0 && errors.userName ? errors.userName : undefined}
              />
              <PhoneMaskInput
                mt={28}
                onFocus={() => setFieldError('phoneNumber', undefined)}
                value={values.phoneNumber}
                onChangeText={handleChange('phoneNumber')}
                errorMessage={submitCount > 0 && errors.phoneNumber ? errors.phoneNumber : undefined}
              />
              {/*  errors.phoneNumber != null && (
              <PhoneNumberErrorMessage mt={12}>
                Данный номер уже зарегистрирован. Уточните {'\n'}
                номер или войдите в свой аккаунт. <LoginLink onPress={onLogin}>Войти</LoginLink>
              </PhoneNumberErrorMessage>
            ) */}
            </ColView>
            <Credentionals mb={36} />
            <TextButton disabled={!isValid} onPress={handleSubmit}>
              Далее
            </TextButton>
          </ContentLayout>
        )}
      </Formik>
    </KeyboardAvoidingViews>
  )
}

/* const PhoneNumberErrorMessage = styled(Footnote)`
  color: ${color('error')};
`

const LoginLink = styled(Link)`
  text-decoration: none;
`
 */
