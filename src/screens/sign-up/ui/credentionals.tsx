import React from 'react'

import { ColView, Footnote, Link } from '@shared/ui'
import { SpaceProps } from 'src/style/mixins/space'

export const Credentionals = ({ mb, ml, mr, mt, pb, pl, pr, pt }: SpaceProps) => {
  const offerClick = () => {
    console.log('offer')
  }

  return (
    <ColView {...{ mb, ml, mr, mt, pb, pl, pr, pt }}>
      <Footnote>
        Нажимая кнопку «Далее», я соглашаюсь с <Link onPress={offerClick}>офертой</Link> и{' '}
        <Link onPress={offerClick}>политикой конфиденциальности</Link>
      </Footnote>
    </ColView>
  )
}
