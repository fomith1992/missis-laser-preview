import React from 'react'
import * as yup from 'yup'
import { Footnote, ColView, ContentLayout, PasswordInput, TextButton } from '@shared/ui'
import { Formik } from 'formik'
import { KeyboardAvoidingViews } from '@shared/ui/layouts/keyboard-avoiding-view'

const MIN_LENGTH = 8
const MAX_LENGTH = 20
const ERROR_MIN_MESSAGE = 'Минимальная длина пароля - 8 символов. Пожалуйста удлините пароль'
const ERROR_MAX_MESSAGE = 'Максимальная длина пароля - 20 символов. Пожалуйста, сократите ваш пароль. '
const ERROR_SAME_PASSWORDS = 'В обоих строках должны быть одинаковые пароли. Пожалуйста, проверьте введенные данные.'

interface SignUpPasswordViewProps {
  initPassword?: string
  onPress: (password?: string) => void
}

const passwordSchema = yup
  .object({
    password: yup
      .string()
      .required(ERROR_MIN_MESSAGE)
      .matches(/^[a-zA-Z0-9!@#$%^&_-]+$/, 'Пароль может содержать только латинские буквы, цифры и символы !@#$%^&_-')
      .min(MIN_LENGTH, ERROR_MIN_MESSAGE)
      .max(MAX_LENGTH, ERROR_MAX_MESSAGE),
    repeatPassword: yup
      .string()
      .oneOf([yup.ref('password')], ERROR_SAME_PASSWORDS)
      .required(ERROR_SAME_PASSWORDS),
  })
  .required()

export const SignUpPasswordView = ({ initPassword, onPress }: SignUpPasswordViewProps) => {
  return (
    <KeyboardAvoidingViews>
      <Formik
        initialValues={{ password: initPassword, repeatPassword: initPassword }}
        onSubmit={(value) => onPress(value.password)}
        validationSchema={passwordSchema}
        validateOnChange={false}
      >
        {({ handleSubmit, handleChange, values, errors, setFieldError }) => (
          <ContentLayout flex mb={36}>
            <ColView mt="auto" mb="auto">
              <PasswordInput
                autoFocus
                placeholder="Введите ваш пароль"
                value={values.password}
                onChangeText={(event) => {
                  setFieldError('password', undefined)
                  handleChange('password')(event)
                }}
                errorMessage={errors.password}
                maxLength={30}
              />
              <PasswordInput
                mt={28}
                placeholder="Повторите ваш пароль"
                value={values.repeatPassword}
                onChangeText={(event) => {
                  setFieldError('repeatPassword', undefined)
                  handleChange('repeatPassword')(event)
                }}
                errorMessage={errors.repeatPassword}
                maxLength={30}
              />
            </ColView>
            <Footnote mb={24} center>
              Пароль должен содержать от 8 до 20 символов
            </Footnote>
            <TextButton onPress={handleSubmit}>Далее</TextButton>
          </ContentLayout>
        )}
      </Formik>
    </KeyboardAvoidingViews>
  )
}
