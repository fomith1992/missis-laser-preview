import React, { useState } from 'react'
import styled from '@emotion/native'

import { BaseView, ColView, ContentLayout, RowView, TextButton, TextInput } from '@shared/ui'
import { KeyboardAvoidingViews } from '@shared/ui/layouts/keyboard-avoiding-view'

type PromoCodeProps = {
  error?: string
  onSubmit: (code: string) => void
  setPromocodeError: () => void
  onReject: () => void
}

export const PromoCodeView = ({ onSubmit, onReject, error, setPromocodeError }: PromoCodeProps) => {
  const [code, setCode] = useState('')

  const submitHandler = () => {
    onSubmit(code)
  }

  return (
    <KeyboardAvoidingViews>
      <ContentLayout flex mb={24}>
        <ColView mt="auto" mb="auto">
          <TextInput
            onChange={setPromocodeError}
            errorMessage={error}
            autoFocus
            placeholder="Введите промокод"
            value={code}
            onChangeText={setCode}
          />
        </ColView>
        <RowView>
          <BaseView flex>
            <TextButton onPress={submitHandler} disabled={!code.length}>
              Активировать
            </TextButton>
          </BaseView>
          <Spring />
          <BaseView flex>
            <TextButton type="secondary" onPress={onReject}>
              Нет промокода
            </TextButton>
          </BaseView>
        </RowView>
      </ContentLayout>
    </KeyboardAvoidingViews>
  )
}

const Spring = styled(BaseView)`
  width: 12px;
`
