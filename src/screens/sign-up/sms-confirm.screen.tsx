import React, { useCallback, useEffect, useState } from 'react'
import useAsyncFn from 'react-use/lib/useAsyncFn'

import { AxiosError } from 'axios'
import { observer } from 'mobx-react'

import { StackActions, useNavigation } from '@react-navigation/core'
import { StackScreenProps } from '@react-navigation/stack'

import { RootStackParamList } from '@navigations/types'
import { authAPI } from '@shared/api'
import { TResponse } from '@shared/api/types'
import { sleep } from '@shared/lib/promises'
import { ColView, ContentLayout, Footnote, Link, TextButton, TextInput } from '@shared/ui'

import { RoutesRoot } from 'src/enums/routes'
import { useRootStore } from 'src/stores/lib/use-root-store'

import { Formik } from 'formik'
import * as yup from 'yup'
import { KeyboardAvoidingViews } from '@shared/ui/layouts/keyboard-avoiding-view'

const codeSchema = yup.object({
  code: yup.number().required().typeError('Код может содержать только цифры!'),
})

type SignUpSMSConfirmScreenProps = StackScreenProps<RootStackParamList, RoutesRoot.SignUpSMSConfirm>

export const SignUpSMSConfirmScreen = observer(({ route }: SignUpSMSConfirmScreenProps) => {
  //hooks
  const nav = useNavigation()
  const { setUserData, getUserDataFromBackend } = useRootStore().user
  const { resetTimer, startTimer } = useRootStore().timer
  const {
    phone: storePhone,
    setPhone,
    fullname: storeFullname,
    setFullname,
    serverError,
    setServerError,
    codeRequested,
    setCodeRequested,
  } = useRootStore().smsVerify

  const [state, submitCode] = useAsyncFn(async (code: string) => {
    return authAPI
      .authByCode({ code, phone: requestedPhone })
      .then(async ({ data: { data } }) => {
        const user = { ...data }
        if (data.phone) {
          const res = await getUserDataFromBackend(data.phone)
          if (res?.data && res.data?.sex) {
            user.sex = res.data.sex
          }
        }
        setUserData(user)
        /* nav.dispatch(CommonActions.reset({ routes: [stackAction.payload] })) */
        nav.dispatch(StackActions.push(RoutesRoot.SelectCity, { isShortcut: false }))
      })
      .catch(catchError)
  })

  //variables
  const { phoneNumber: routePhone, fullname: routeFullname } = route?.params
  const requestedPhone = '7' + routePhone?.replace(/[()-\s]/g, '')

  //callbacks
  const catchError = useCallback(
    (e: AxiosError<TResponse>) => {
      const msg = e.response?.data.meta?.message
      if (typeof msg === 'string') {
        setServerError(msg)
      }
    },
    [setServerError],
  )

  const sendCodeOnPhone = useCallback(() => {
    if (!requestedPhone) return
    authAPI
      .verifySMSCode({ phone: requestedPhone, fullname: routeFullname })
      .then(() => {
        setCodeRequested(true)
        setServerError(undefined)
      })
      .catch((e: AxiosError<TResponse>) => {
        setCodeRequested(false)
        catchError(e)
      })
  }, [requestedPhone, routeFullname, setServerError, catchError, setCodeRequested])

  //effects
  useEffect(() => {
    if (storePhone !== routePhone || storeFullname !== routeFullname) {
      setPhone(routePhone)
      setFullname(routeFullname)
      setServerError()
      startTimer(60000)
      resetTimer()
      sendCodeOnPhone()
    }
  }, [routePhone, routeFullname]) // eslint-disable-line react-hooks/exhaustive-deps

  //handlers
  const changePhonePress = () => {
    nav.dispatch(StackActions.pop())
  }

  return (
    <KeyboardAvoidingViews>
      <Formik
        initialValues={{ code: '' }}
        onSubmit={({ code }) => submitCode(code)}
        validationSchema={codeSchema}
        validateOnChange={false}
      >
        {({ handleSubmit, handleChange, values, errors }) => (
          <ContentLayout flex mb={36}>
            <ColView mt="auto" mb="auto">
              <TextInput
                focusOnMount
                errorMessage={errors.code || serverError}
                value={values.code}
                onChangeText={(e) => {
                  setServerError(undefined)
                  handleChange('code')(e)
                }}
                placeholder="Код из смс"
              />
            </ColView>
            <ColView>
              <ColView aiCenter>
                <Footnote mb={12}>
                  Код отправлен на 8 {storePhone} <Link onPress={changePhonePress}>Изменить</Link>
                </Footnote>
                <Countdown canRetry={!codeRequested} onReset={sendCodeOnPhone} />
              </ColView>
              <TextButton disabled={state.loading || values.code.length === 0} onPress={handleSubmit}>
                Далее
              </TextButton>
            </ColView>
          </ContentLayout>
        )}
      </Formik>
    </KeyboardAvoidingViews>
  )
})

type CountdownProps = {
  onReset: () => void
  canRetry: boolean
}

const Countdown = observer(({ onReset, canRetry }: CountdownProps) => {
  const [blocked, setBlocked] = useState(false)

  const timer = useRootStore().timer

  const handleReset = () => {
    onReset()
    timer.resetTimer()
    setBlocked(true)
    sleep(1000).then(() => setBlocked(false))
  }

  if (blocked) {
    return <Footnote mb={12}> Код отправлен </Footnote>
  }

  if (timer.value <= 0 || canRetry) {
    return (
      <Link mb={12} onPress={handleReset}>
        Отправить еще раз
      </Link>
    )
  }

  return <Footnote mb={12}>Отправить еще раз (через {timer.value / 1000} секунд)</Footnote>
})
