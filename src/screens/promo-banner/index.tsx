import React from 'react'
import { PromoBanner } from '@features/promo-banner/promo-banner.view'

import image1 from './assets/1.png'
import image2 from './assets/2.png'
import image3 from './assets/3.png'
import { StackActions, useNavigation } from '@react-navigation/core'
import { RoutesRoot } from 'src/enums/routes'

const data = [
  {
    title: 'Сеть популярного блогера -\nЛизы Миллер',
    icon: image1,
    buttonText: 'Круто, а что еще?',
  },
  {
    title: 'Гарантируем высокую эффективность процедуры',
    icon: image2,
    buttonText: 'Вау, давайте дальше',
  },
  {
    title: 'Медицинские лицензии и опытные сотрудницы',
    description: 'с медобразованием',
    icon: image3,
    buttonText: 'Ок, понятно',
  },
]

export const PromoBannerScreen = () => {
  const navigation = useNavigation()

  return <PromoBanner data={data} onEndAnimated={() => navigation.dispatch(StackActions.replace(RoutesRoot.Login))} />
}
