import React, { useEffect, useState } from 'react'
import { PhoneMaskInput } from '@shared/ui'

type SignUpFioPhoneProps = {
  onComplete?: (result: boolean) => void
}

const mask = '(999) 999-99-99'

export const RecoverPhone = ({ onComplete }: SignUpFioPhoneProps) => {
  const [phone, setPhone] = useState('')

  const [validPhone, setValidPhone] = useState(false)

  useEffect(() => {
    if (onComplete) {
      onComplete(validPhone)
    }
  }, [validPhone, onComplete])

  const changeTextPhone = (text: string) => {
    setPhone(text)
    validate(text, 'phone')
  }

  const validate = (text: string, field: 'phone') => {
    if (field === 'phone') {
      setValidPhone(text.length === mask.length)
    }
  }

  return <PhoneMaskInput value={phone} onChangeText={changeTextPhone} />
}
