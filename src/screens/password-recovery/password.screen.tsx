import React, { useState } from 'react'
import * as yup from 'yup'

import styled from '@emotion/native'
import { ColView, ContentLayout, Footnote, PasswordInput, TextButton } from '@shared/ui'
import { StackActions, useNavigation } from '@react-navigation/core'
import { RoutesRoot } from 'src/enums/routes'

const MIN_LENGTH = 8
const MAX_LENGTH = 20
const ERROR_MIN_MESSAGE = 'Пожалуйста удлините пароль'
const ERROR_MAX_MESSAGE = 'Максимальная длина пароля - 20 символов. Пожалуйста, сократите ваш пароль. '
const ERROR_SAME_PASSWORDS = 'В обоих строках должны быть одинаковые пароли. Пожалуйста, проверьте введенные данные.'

const passwordSchema = yup
  .object({
    password: yup
      .string()
      .required()
      .matches(/^[a-zA-Z0-9!@#$%^&-]+$/, 'Пароль может содержать только латинские буквы, цифры и символы !@#$%^&-')
      .min(MIN_LENGTH, ERROR_MIN_MESSAGE)
      .max(MAX_LENGTH, ERROR_MAX_MESSAGE),
    repeatPassword: yup.string().oneOf([yup.ref('password')], ERROR_SAME_PASSWORDS),
  })
  .required()
  .test({
    name: 'passwords match',
    message: ERROR_SAME_PASSWORDS,
    test: ({ password, repeatPassword }) => password === repeatPassword,
  })

export const RecoveryPasswordScreen = () => {
  const [password, setPassword] = useState('')
  const [repeatPassword, setRepeatPassword] = useState('')

  const [error, setError] = useState<{ path?: string; msg: string } | null>(null)

  const nav = useNavigation()

  const onChangeTextPassword = (text: string) => {
    setPassword(text)
    setError(null)
  }

  const onChangeTextRepeatPassword = (text: string) => {
    setRepeatPassword(text)
    setError(null)
  }

  const onSumbit = () => {
    setError(null)
    passwordSchema
      .validate({ password, repeatPassword })
      .then(() => {
        nav.dispatch(StackActions.push(RoutesRoot.SuccessPasswordRecovery))
      })
      .catch((e: yup.ValidationError) => setError({ path: e.path, msg: e.message }))
  }

  return (
    <ContentLayout flex mb={36}>
      <ColView mt="auto" mb="auto">
        <PasswordInput
          autoFocus
          placeholder="Введите ваш пароль"
          value={password}
          onChangeText={onChangeTextPassword}
          errorMessage={error?.path === 'password' ? error?.msg : undefined}
          maxLength={30}
        />
        <PasswordInput
          mt={28}
          placeholder="Повторите ваш пароль"
          value={repeatPassword}
          onChangeText={onChangeTextRepeatPassword}
          errorMessage={error?.path === '' ? error.msg : undefined}
          maxLength={30}
        />
      </ColView>
      <Center mb={24}>
        <Footnote>
          Пароль должен содержать от {MIN_LENGTH} до {MAX_LENGTH} символов
        </Footnote>
      </Center>
      <TextButton disabled={!password.length || !repeatPassword.length} onPress={onSumbit}>
        Далее
      </TextButton>
    </ContentLayout>
  )
}

const Center = styled(ColView)`
  align-items: center;
`
