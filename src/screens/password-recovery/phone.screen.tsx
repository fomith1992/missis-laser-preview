import React, { useEffect, useState } from 'react'

import { ColView, ContentLayout, TextButton } from '@shared/ui'

import { StackActions, useNavigation } from '@react-navigation/core'
import { RoutesRoot } from 'src/enums/routes'
import { RecoverPhone } from '@screens/password-recovery/ui/recover-phone'

export const RecoveryPhoneScreen = () => {
  const [isComplete, setComplete] = useState(false)

  const nav = useNavigation()

  useEffect(() => {
    // TODO check phone is avaiable if true go to next step
  }, [isComplete])

  const onNextPress = () => {
    nav.dispatch(StackActions.push(RoutesRoot.RecoverySMSConfirm))
  }

  return (
    <ContentLayout flex mb={36}>
      <ColView mt="auto" mb="auto">
        <RecoverPhone onComplete={setComplete} />
      </ColView>
      <TextButton disabled={!isComplete} onPress={onNextPress}>
        Далее
      </TextButton>
    </ContentLayout>
  )
}
