import React, { useState } from 'react'
import { StackActions, useNavigation } from '@react-navigation/core'
import styled from '@emotion/native'

import { ColView, ContentLayout, Footnote, Link, TextButton, TextInput } from '@shared/ui'
import { useCountdownTimer } from '@shared/lib/hooks/useCountdownTimer'
import { RoutesRoot } from '../../enums/routes'

export const RecoverySMSConfirmScreen = () => {
  const [code, setCode] = useState('')
  const nav = useNavigation()

  const changePhonePress = () => {
    nav.dispatch(StackActions.pop(2))
  }

  const onNextPress = () => {
    nav.dispatch(StackActions.push(RoutesRoot.RecoveryPassword))
  }
  return (
    <ContentLayout flex mb={36}>
      <ColView mt="auto" mb="auto">
        <TextInput autoFocus value={code} onChangeText={setCode} placeholder="Код из смс" />
      </ColView>
      <ColView>
        <Center>
          <Footnote mb={12}>
            Код отправлен на 8 (916) 454-56-34 <Link onPress={changePhonePress}>Изменить</Link>
          </Footnote>
          <Countdown />
        </Center>
        <TextButton disabled={!code} onPress={onNextPress}>
          Далее
        </TextButton>
      </ColView>
    </ContentLayout>
  )
}

const Countdown = () => {
  const { value, expired, reset } = useCountdownTimer({
    seconds: 60,
    onExpire: () => console.log('expire'),
  })

  if (expired) {
    return (
      <Link mb={12} onPress={reset}>
        Отправить еще раз
      </Link>
    )
  }

  return <Footnote mb={12}>Отправить еще раз (через {value} секунд)</Footnote>
}

const Center = styled(ColView)`
  align-items: center;
`
