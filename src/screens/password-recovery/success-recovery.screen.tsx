import React from 'react'
import { SafeAreaView } from '@shared/ui'
import { ProccessStateView } from '@features/proccess-state/proccess-state.view'

export function SuccessRecoveryScreen(): React.ReactElement {
  return (
    <SafeAreaView>
      <ProccessStateView
        title="Пароль изменен!"
        description={``}
        buttonText="Спасибо, понятно"
        onPress={() => console.log('Success Recovery')}
      />
    </SafeAreaView>
  )
}
