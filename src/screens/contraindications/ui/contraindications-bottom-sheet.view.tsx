import React from 'react'
import styled from '@emotion/native'
import { BaseView, Body, ContentLayout, Title } from '@shared/ui'
import { TextButton } from '@shared/ui/buttons/text-button.view'
import { Linking } from 'react-native'
import { TouchableWithoutFeedback } from '@gorhom/bottom-sheet'
import { Platform } from 'react-native'

interface ContraindicationsBottomSheetViewProps {
  onSubmit: () => void
  numberToCall: string
}

export const ContraindicationsBottomSheetView = ({
  onSubmit,
  numberToCall,
}: ContraindicationsBottomSheetViewProps): React.ReactElement => {
  const isIos = Platform.OS === 'ios'
  return (
    <ContentLayout>
      <Title>Есть ли у вас противопоказания и насколько они повлияют на результат процедуры?</Title>
      <Body mt={12}>
        После того как вы придете в салон, врач осмотрит вас и проконсультирует на тему противопоказаний именно в вашем
        случае
      </Body>
      <BottomButtonsContainer>
        <BaseView flex mr="auto">
          <TouchableWithoutFeedback onPress={() => (!isIos ? Linking.openURL(`tel:${numberToCall}`) : undefined)}>
            <TextButton onPress={() => (isIos ? Linking.openURL(`tel:${numberToCall}`) : undefined)} type="secondary">
              Позвонить нам
            </TextButton>
          </TouchableWithoutFeedback>
        </BaseView>
        <BaseView flex ml={12} pb={isIos ? 32 : 0}>
          <TouchableWithoutFeedback onPress={!isIos ? onSubmit : undefined}>
            <TextButton onPress={isIos ? onSubmit : undefined}>Понятно</TextButton>
          </TouchableWithoutFeedback>
        </BaseView>
      </BottomButtonsContainer>
    </ContentLayout>
  )
}

const BottomButtonsContainer = styled.View`
  margin-top: 20px;
  margin-bottom: 24px;
  flex-direction: row;
  justify-content: space-between;
`
