import { BlockedIcon64 } from '@shared/ui/icons/blocked.icon-64'
import { BaseView, Body, ContentLayout, Footnote, RowView, Title } from '@shared/ui'
import React, { useRef } from 'react'
import { WarningIcon64 } from '@shared/ui/icons/warning.icon-64'
import { color } from 'src/style/mixins/color'
import styled from '@emotion/native'
import { TextButton } from '@shared/ui/buttons/text-button.view'
import { ContraindicationsBottomSheetView } from './contraindications-bottom-sheet.view'
import { ScrollView } from 'react-native-gesture-handler'

import BottomSheet from '@gorhom/bottom-sheet'
import { BottomSheetGorhom } from '@features/bottom-sheet-gorhom/bottom-sheet'

interface ContraindicationsProps {
  onConfirmation: () => void
  numberToCall: string
}

export function Contraindications({ onConfirmation, numberToCall }: ContraindicationsProps): React.ReactElement {
  const sheetRef = useRef<BottomSheet>(null)

  return (
    <>
      <ScrollView>
        <ContentLayout flex>
          <RowView mt={24}>
            <BlockedSvg />
            <BaseView ml={16}>
              <Title>Абсолютные</Title>
              <Footnote>Процедура противопоказана{'\n'}в любом случае</Footnote>
            </BaseView>
          </RowView>
          <BaseView mt={16}>
            <Body>• онкологический диагноз</Body>
            <Body>• декомпенсированный сахарный диабет</Body>
            <Body>• индивидуальная непереносимость процедуры</Body>
            <Body>• очень смуглая кожа</Body>
          </BaseView>
          <RowView mt={24}>
            <WarningSvg />
            <BaseView ml={16}>
              <Title>Относительные</Title>
              <Footnote>Процедура неблагоприятна, но{'\n'}возможна под контролем врача</Footnote>
            </BaseView>
          </RowView>
          <BaseView mt={16}>
            <Body>• хронические кожные заболевания</Body>
            <Body>• множественные родинки в зоне воздействия лазером</Body>
            <Body>• тату в зоне воздействия лазером</Body>
            <Body>• склонность к образованию келоидных рубцов</Body>
          </BaseView>
        </ContentLayout>
      </ScrollView>
      <ContentLayout>
        <BottomButtonsContainer>
          <DoubtButton onPress={() => sheetRef.current?.snapTo(1)} type="secondary">
            Есть сомнения
          </DoubtButton>
          <ContinueButton onPress={onConfirmation}>Продолжить</ContinueButton>
        </BottomButtonsContainer>
      </ContentLayout>
      <BottomSheetGorhom sheetRef={sheetRef}>
        <ContraindicationsBottomSheetView numberToCall={numberToCall} onSubmit={() => sheetRef.current?.snapTo(0)} />
      </BottomSheetGorhom>
    </>
  )
}

const BlockedSvg = styled(BlockedIcon64)`
  color: ${color('error')};
`

const WarningSvg = styled(WarningIcon64)`
  color: ${color('warning')};
`

const BottomButtonsContainer = styled(RowView)`
  padding: 20px 0 24px;
  flex-shrink: 0;
  justify-content: space-between;
`

const ContinueButton = styled(TextButton)`
  flex: 1;
  margin-left: 12px;
`

const DoubtButton = styled(TextButton)`
  flex: 1;
`
