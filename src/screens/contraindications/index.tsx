import { RootStackParamList } from '@navigations/types'
import { StackActions, useNavigation } from '@react-navigation/core'
import { StackScreenProps } from '@react-navigation/stack'
import { SafeAreaView } from '@shared/ui'
import React from 'react'
import { RoutesRoot } from 'src/enums/routes'
import { useRootStore } from 'src/stores/lib/use-root-store'

import { Contraindications } from './ui/contraindications.view'

const numberToCall = '88005515257'

export function ContraindicationsScreen({ route }: StackScreenProps<RootStackParamList, RoutesRoot.Contraindications>) {
  const { promocode = false } = route.params
  const { setCurrentStaff, currentServices } = useRootStore().appointmentReport
  const navigation = useNavigation()

  return (
    <SafeAreaView topOff>
      <Contraindications
        numberToCall={numberToCall}
        onConfirmation={() => {
          if (promocode) {
            setCurrentStaff(0)
            navigation.dispatch(
              StackActions.push(RoutesRoot.DateTimeSelect, {
                selectedServices: currentServices,
                workerId: 0,
              }),
            )
          } else {
            navigation.dispatch(StackActions.push(RoutesRoot.ZonesSelect))
          }
        }}
      />
    </SafeAreaView>
  )
}
