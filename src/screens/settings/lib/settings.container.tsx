import React, { useRef } from 'react'
import * as WebBrowser from 'expo-web-browser'
import * as MailComposer from 'expo-mail-composer'
import { SettingsView } from '../ui/settings.view'
import { LogOutBottomSheetView } from '../ui/log-out-bottom-sheet-view'
import { StackActions, useNavigation } from '@react-navigation/core'

import { BottomSheetGorhom } from '@features/bottom-sheet-gorhom/bottom-sheet'
import BottomSheet from '@gorhom/bottom-sheet'
import { RoutesRoot } from 'src/enums/routes'
import { useRootStore } from 'src/stores/lib/use-root-store'
import { RoutesHome } from '@navigations/stack-home/routes'

const privacyPolicyRef = 'https://podruge.ru/yuridicheskaya-informatsiya/politic/politic.pdf'
const medLicenseRef = 'https://podruge.ru/details/?sphrase_id=7640'

export const Settings = (): React.ReactElement => {
  const sheetRef = useRef<BottomSheet>(null)
  const navigation = useNavigation()

  const rootStore = useRootStore()

  const handlePressPrivacyPolicy = async () => {
    await WebBrowser.openBrowserAsync(privacyPolicyRef)
  }
  const handlePressMedLicense = async () => {
    await WebBrowser.openBrowserAsync(medLicenseRef)
  }

  const sendFeedBackHandler = async () => {
    const available = await MailComposer.isAvailableAsync()
    if (available) {
      await MailComposer.composeAsync({
        recipients: ['info@podruge.ru'],
      })
    } else {
      console.log('iOS: MDM profile is setup to block outgoing mail')
    }
  }

  const handleLogOut = () => {
    rootStore.user.reset()
    rootStore.geolocationReport.reset()
    navigation.dispatch(StackActions.push(RoutesRoot.Login))
  }

  return (
    <>
      <SettingsView
        onProfileSettingsPress={() => navigation.dispatch(StackActions.push(RoutesHome.ProfileSettings))}
        onSendFeedBack={sendFeedBackHandler}
        onPressMedLicense={handlePressMedLicense}
        onPressPrivacyPolicy={handlePressPrivacyPolicy}
        onLogOut={() => sheetRef.current?.expand()}
      />
      <BottomSheetGorhom sheetRef={sheetRef}>
        <LogOutBottomSheetView onSubmit={handleLogOut} sheetRef={sheetRef} />
      </BottomSheetGorhom>
    </>
  )
}
