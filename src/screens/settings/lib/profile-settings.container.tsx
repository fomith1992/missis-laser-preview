import React from 'react'
import useAsyncFn from 'react-use/lib/useAsyncFn'

import { observer } from 'mobx-react'

import { useNavigation } from '@react-navigation/native'

import { authAPI } from '@shared/api'
import { LoadingIndicator } from '@shared/ui/atoms/loading-indicator'

import { UserSexInstance } from 'src/stores/domains/user/user.store'
import { useRootStore } from 'src/stores/lib/use-root-store'

import { ProfileSettingsView } from '../ui/profile-settings.view'

export const ProfileSettings = observer((): React.ReactElement => {
  const { phone, name, setUserData, loading, user_token, sex } = useRootStore().user
  const navigation = useNavigation()

  const [queryState, onSendQuery] = useAsyncFn(async (newName: string, sex: UserSexInstance) => {
    return await authAPI.changePersonalInfo(newName, user_token ?? '').then(() => {
      setUserData({ sex, name: newName })
      navigation.goBack()
    })
  }, [])

  return (
    <>
      <ProfileSettingsView
        onSendQuery={onSendQuery}
        userSex={sex as UserSexInstance}
        fullname={name}
        phone={phone ? phone.slice(1) : null}
      />
      <LoadingIndicator visible={queryState.loading || loading} />
    </>
  )
})
