import React from 'react'
import { Settings } from './lib/settings.container'

export const SettingsScreen = () => {
  return <Settings />
}
