import styled from '@emotion/native'
import { Body, ContentLayout, RowView } from '@shared/ui'
import { ExitIcon24 } from '@shared/ui/icons/exit.icon-24'
import React from 'react'
import { TouchableOpacity } from 'react-native'
import { UserIcon24 } from 'src/shared/ui/icons/user.icon-24'
import { color } from 'src/style/mixins/color'

interface SettingsTabProps {
  children: React.ReactNode
  title: string
  onPress: () => void
}

const SettingsTab = ({ children, title, onPress }: SettingsTabProps): React.ReactElement => {
  return (
    <TouchableOpacity onPress={onPress}>
      <RowView aiCenter mt={16} mb={16}>
        {children}
        <Body ml={12}>{title}</Body>
      </RowView>
    </TouchableOpacity>
  )
}
interface SettingsViewProps {
  onPressPrivacyPolicy: () => void
  onPressMedLicense: () => void
  onProfileSettingsPress: () => void
  onSendFeedBack: () => void
  onLogOut: () => void
}

export const SettingsView = ({ onProfileSettingsPress, onLogOut }: SettingsViewProps): React.ReactElement => {
  return (
    <ContentLayout flex mt={8}>
      <SettingsTab title="Профиль" onPress={onProfileSettingsPress}>
        <UserSvg />
      </SettingsTab>
      {/* <SettingsTab title="Написать отзыв или идею" onPress={onSendFeedBack}>
        <SaleSvg />
      </SettingsTab>
      <SettingsTab title="Медицинские лицензии" onPress={onPressMedLicense}>
        <DocumentSvg />
      </SettingsTab>
      <SettingsTab title="Правила и соглашения" onPress={onPressPrivacyPolicy}>
        <InfoSvg />
      </SettingsTab> */}
      <SettingsTab title="Выйти из аккаунта" onPress={onLogOut}>
        <ExitSvg />
      </SettingsTab>
    </ContentLayout>
  )
}

const UserSvg = styled(UserIcon24)`
  color: ${color('primary')};
`
/* const SaleSvg = styled(SaleIcon24)`
  color: ${color('primary')};
`
const DocumentSvg = styled(DocumentIcon24)`
  color: ${color('primary')};
`
const InfoSvg = styled(InfoIcon24)`
  color: ${color('primary')};
` */
const ExitSvg = styled(ExitIcon24)`
  color: ${color('error')};
`
