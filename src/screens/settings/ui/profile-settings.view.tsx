import styled from '@emotion/native'
import { BaseView, Body, ContentLayout, PhoneMaskInput, RowView, TextButton, TextInput } from '@shared/ui'
import React, { useState } from 'react'
import { KeyboardAvoidingView, Platform } from 'react-native'
import { UserSexInstance } from 'src/stores/domains/user/user.store'

// todo Принадлежность к полу (1 - мужской, 2 - женский, 0 - не задано)

interface ProfileSettingsViewProps {
  userSex: UserSexInstance
  phone: string | null
  fullname: string | null
  onSendQuery: (newName: string, sex: UserSexInstance) => void
}

export const ProfileSettingsView = ({
  phone,
  fullname,
  userSex,
  onSendQuery,
}: ProfileSettingsViewProps): React.ReactElement => {
  const [sex, setSex] = useState<UserSexInstance>(userSex)
  const [name, setName] = useState<string>(fullname ?? '')

  return (
    <ContentLayout flex mt={8}>
      <KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS === 'ios' ? 'padding' : undefined}>
        <PhoneMaskInput editable={false} value={phone ?? 'Телефона нет'} />
        <TextInput mt={28} value={name ?? ''} onChangeText={setName} />
        <RowView mt={36} mb={24}>
          <BaseView flex mr={4}>
            <Button active={sex === 2} onPress={() => setSex(2)}>
              <ButtonText mt={12} mb={12} active={sex === 2}>
                Женщина
              </ButtonText>
            </Button>
          </BaseView>
          <BaseView flex ml={4}>
            <Button active={sex === 1} onPress={() => setSex(1)}>
              <ButtonText mt={12} mb={12} active={sex === 1}>
                Мужчина
              </ButtonText>
            </Button>
          </BaseView>
        </RowView>
        <BottomButtonContainer>
          <TextButton disabled={name === fullname && userSex === sex} onPress={() => onSendQuery(name, sex)}>
            Сохранить
          </TextButton>
        </BottomButtonContainer>
      </KeyboardAvoidingView>
    </ContentLayout>
  )
}

interface ButtonProps {
  active: boolean
}

const Button = styled.TouchableOpacity<ButtonProps>`
  border-radius: 30px;
  background-color: ${({ active, theme: { colors } }) => (active ? colors.black : null)};
`
const ButtonText = styled(Body)<ButtonProps>`
  text-align: center;
  color: ${({ active, theme: { colors } }) => (active ? colors.lightText : colors.secondaryText)};
`
const BottomButtonContainer = styled.View`
  margin-top: auto;
  margin-bottom: 24px;
`
