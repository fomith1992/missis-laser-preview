import React from 'react'

import { BaseView, Body, ContentLayout, Headiline, RowView, TextButton } from '@shared/ui'
import BottomSheet, { TouchableWithoutFeedback } from '@gorhom/bottom-sheet'
import { Platform } from 'react-native'

type LogOutBottomSheetViewProps = {
  onSubmit: () => void
  sheetRef: React.RefObject<BottomSheet>
}

export const LogOutBottomSheetView = ({ onSubmit, sheetRef }: LogOutBottomSheetViewProps) => {
  const isIos = Platform.OS === 'ios'
  return (
    <ContentLayout>
      <Headiline center mt={4}>
        Выйти из аккаунта?
      </Headiline>
      <Body center mt={12}>
        Чтобы снова войти, вам понадобится {'\n'} ввести свои данные
      </Body>
      <RowView mt={20} mb={24}>
        <BaseView flex mr={4}>
          <TouchableWithoutFeedback onPress={() => (!isIos ? sheetRef.current?.snapTo(0) : undefined)}>
            <TextButton onPress={() => (isIos ? sheetRef.current?.snapTo(0) : undefined)} type="secondary">
              Отмена
            </TextButton>
          </TouchableWithoutFeedback>
        </BaseView>
        <BaseView flex ml={4}>
          <TouchableWithoutFeedback onPress={!isIos ? onSubmit : undefined}>
            <TextButton onPress={isIos ? onSubmit : undefined} type="error">
              Да
            </TextButton>
          </TouchableWithoutFeedback>
        </BaseView>
      </RowView>
    </ContentLayout>
  )
}
