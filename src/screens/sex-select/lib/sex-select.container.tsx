import React from 'react'

import { StackActions, useNavigation } from '@react-navigation/core'

import { RoutesRoot } from 'src/enums/routes'
import { useRootStore } from 'src/stores/lib/use-root-store'

import { SexSelectView } from '../ui/sex-select.view'

export function SexSelect(): React.ReactElement {
  const navigation = useNavigation()
  const { setUserData } = useRootStore().user

  return (
    <SexSelectView
      onSelectPressed={(sex) => {
        setUserData({ sex })
        navigation.dispatch(StackActions.push(RoutesRoot.PromoCode))
      }}
    />
  )
}
