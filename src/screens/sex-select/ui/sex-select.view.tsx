import React from 'react'
import styled from '@emotion/native'

import { Body, ColView, ContentLayout, RowView } from '@shared/ui'

import { ManIcon32 } from '@shared/ui/icons/man.icon-32'
import { WomanIcon32 } from '@shared/ui/icons/woman.icon-32'
import { color } from 'src/style/mixins/color'
import { UserSexInstance } from 'src/stores/domains/user/user.store'

// todo Принадлежность к полу (1 - мужской, 2 - женский, 0 - не задано)

interface SexSelectViewProps {
  onSelectPressed: (sex: UserSexInstance) => void
}

export function SexSelectView({ onSelectPressed }: SexSelectViewProps): React.ReactElement {
  return (
    <Container flex>
      <ColView mt={40}>
        <SexButton onPress={() => onSelectPressed(2)}>
          <ButtonContent>
            <WomanIcon32 />
            <Body ml={8} color="lightText">
              Женщина
            </Body>
          </ButtonContent>
        </SexButton>
      </ColView>
      <ColView mt={16}>
        <SexButton onPress={() => onSelectPressed(1)}>
          <ButtonContent>
            <ManIcon32 />
            <Body ml={8} color="lightText">
              Мужчина
            </Body>
          </ButtonContent>
        </SexButton>
      </ColView>
    </Container>
  )
}

const Container = styled(ContentLayout)`
  justify-content: center;
`
const SexButton = styled.TouchableOpacity`
  padding-top: 12px;
  padding-bottom: 12px;
  align-items: center;
  justify-content: center;
  background-color: ${color('primary')};
  border-radius: 30px;
`

const ButtonContent = styled(RowView)`
  margin: auto;
  align-items: center;
  justify-content: center;
`
