import { useDefaultHeader } from '@shared/ui/header/use-default-header'
import React, { useRef } from 'react'
import { SelectCity } from './select-city.container'
import BottomSheet from '@gorhom/bottom-sheet'
import { StackScreenProps } from '@react-navigation/stack'
import { RootStackParamList } from '@navigations/types'
import { RoutesRoot } from 'src/enums/routes'

export const SelectCityScreen = ({ route }: StackScreenProps<RootStackParamList, RoutesRoot.SelectCity>) => {
  const sheetRef = useRef<BottomSheet>(null)
  useDefaultHeader({
    actions: [
      {
        icon: 'pointOnMap',
        onPress: () => sheetRef.current?.expand(),
      },
    ],
  })

  return <SelectCity sheetRef={sheetRef} isShortcut={route?.params?.isShortcut} />
}
