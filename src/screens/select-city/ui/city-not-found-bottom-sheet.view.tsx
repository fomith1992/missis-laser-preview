import React from 'react'
import { TouchableWithoutFeedback } from '@gorhom/bottom-sheet'
import { color } from 'src/style/mixins/color'
import styled from '@emotion/native'
import { BaseView, ContentLayout, Headiline, RowView } from '@shared/ui'
import { TextButton } from '@shared/ui/buttons/text-button.view'
import { container } from 'src/style/mixins/space'
import { Platform } from 'react-native'

interface ChooseDoctorBottomSheetViewProps {
  onPress: () => void
}

export const CityNotFoundBottomSheetView = ({ onPress }: ChooseDoctorBottomSheetViewProps): React.ReactElement => {
  const isIos = Platform.OS === 'ios'
  return (
    <RenderContentView>
      <CenteredHeadline numberOfLines={2} mt={20}>
        Не удалось определить{'\n'}ваш город
      </CenteredHeadline>
      <RowView mt={20} mb={32} ml={4}>
        <ButtonContaner ml={4}>
          <TouchableWithoutFeedback onPress={!isIos ? onPress : undefined}>
            <TextButton onPress={isIos ? onPress : undefined}>Выбрать город вручную</TextButton>
          </TouchableWithoutFeedback>
        </ButtonContaner>
      </RowView>
    </RenderContentView>
  )
}

export const RenderContentView = styled(ContentLayout)`
  background-color: ${color('background')};
  ${container('padding')}
`

const CenteredHeadline = styled(Headiline)`
  text-align: center;
`

export const ButtonContaner = styled(BaseView)`
  flex: 1;
`
