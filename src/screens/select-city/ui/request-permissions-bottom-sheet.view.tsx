import React from 'react'
import { color } from 'src/style/mixins/color'
import styled from '@emotion/native'
import { BaseView, Body, Headiline, RowView } from '@shared/ui'
import { TextButton } from '@shared/ui/buttons/text-button.view'
import { container } from 'src/style/mixins/space'
import { LocationIcon24 } from '@shared/ui/icons/location.icon-24'
import { TouchableWithoutFeedback } from '@gorhom/bottom-sheet'
import { Platform } from 'react-native'

interface ShoppingCartBottomSheetViewProps {
  onSubmit: () => void
  onSelectCity: () => void
}

export const RequestPermissionsBottomSheetView = ({
  onSubmit,
  onSelectCity,
}: ShoppingCartBottomSheetViewProps): React.ReactElement => {
  const isIos = Platform.OS === 'ios'
  return (
    <RenderContentView>
      <IconContainer>
        <LocationSvg />
      </IconContainer>
      <Headiline mt={20}>Доступ к геолокации</Headiline>
      <Body mt={12}>Покажем салоны и услуги в вашем городе</Body>
      <RowView mt={20} mb={24}>
        <BaseView flex mr={4}>
          <TouchableWithoutFeedback onPress={!isIos ? onSelectCity : undefined}>
            <TextButton onPress={isIos ? onSelectCity : undefined} type="secondary">
              Выбрать город
            </TextButton>
          </TouchableWithoutFeedback>
        </BaseView>
        <BaseView flex ml={4}>
          <TouchableWithoutFeedback onPress={!isIos ? onSubmit : undefined}>
            <TextButton onPress={isIos ? onSubmit : undefined} type="primary">
              Разрешить
            </TextButton>
          </TouchableWithoutFeedback>
        </BaseView>
      </RowView>
    </RenderContentView>
  )
}

export const RenderContentView = styled.View`
  align-items: center;
  background-color: ${color('background')};
  ${container('padding')}
`
const IconContainer = styled.View`
  margin-top: 20px;
  width: 96px;
  height: 96px;
  border-radius: 48px;
  overflow: hidden;
  align-items: center;
  justify-content: center;
  background-color: ${color('primaryLight')};
`
const LocationSvg = styled(LocationIcon24)`
  width: 80px;
  height: 80px;
  color: ${color('primary')};
`
export const ButtonContaner = styled(BaseView)`
  flex: 1;
`
