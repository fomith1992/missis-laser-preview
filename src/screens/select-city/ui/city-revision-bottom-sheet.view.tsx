import React from 'react'
import { TouchableWithoutFeedback } from '@gorhom/bottom-sheet'
import { color } from 'src/style/mixins/color'
import styled from '@emotion/native'
import { BaseView, Body, ContentLayout, Headiline } from '@shared/ui'
import { TextButton } from '@shared/ui/buttons/text-button.view'
import { Platform } from 'react-native'

interface ShoppingCartBottomSheetViewProps {
  city: string
  onSubmit: () => void
  onSelectCity: () => void
}

export const CityRevisionBottomSheetView = ({
  city,
  onSubmit,
  onSelectCity,
}: ShoppingCartBottomSheetViewProps): React.ReactElement => {
  const isIos = Platform.OS === 'ios'
  return (
    <RenderContentView>
      <Headiline mt={4}>{`Ваш город - ${city}?`}</Headiline>
      <Body mt={12}>Если нет, то тогда выберите город вручную</Body>
      <BottomButtonsContainer>
        <ButtonContaner mr={4}>
          <TouchableWithoutFeedback onPress={!isIos ? onSelectCity : undefined}>
            <TextButton onPress={isIos ? onSelectCity : undefined} type="secondary">
              Нет, другой
            </TextButton>
          </TouchableWithoutFeedback>
        </ButtonContaner>
        <ButtonContaner ml={4}>
          <TouchableWithoutFeedback onPress={!isIos ? onSubmit : undefined}>
            <TextButton onPress={isIos ? onSubmit : undefined} type="primary">
              Да
            </TextButton>
          </TouchableWithoutFeedback>
        </ButtonContaner>
      </BottomButtonsContainer>
    </RenderContentView>
  )
}

export const RenderContentView = styled(ContentLayout)`
  align-items: center;
  background-color: ${color('background')};
`
const BottomButtonsContainer = styled.View`
  margin-top: 20px;
  margin-bottom: 24px;
  flex-direction: row;
`
export const ButtonContaner = styled(BaseView)`
  flex: 1;
`
