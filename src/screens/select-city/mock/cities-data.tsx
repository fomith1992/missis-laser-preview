export const citiesData = [
  {
    title: null,
    data: [
      {
        city: 'Москва',
        city_id: 2,
      },
      {
        city: 'Санкт-Петербург',
        city_id: 1,
      },
    ],
  },
  {
    title: 'А',
    data: [],
  },
  {
    title: 'Б',
    data: [],
  },
  {
    title: 'В',
    data: [],
  },
  {
    title: 'Г',
    data: [],
  },
  {
    title: 'Д',
    data: [],
  },
  {
    title: 'Е',
    data: [],
  },
  {
    title: 'Ё',
    data: [],
  },
  {
    title: 'Ж',
    data: [],
  },
  {
    title: 'З',
    data: [],
  },
  {
    title: 'И',
    data: [],
  },
  {
    title: 'К',
    data: [],
  },
  {
    title: 'Л',
    data: [],
  },
  {
    title: 'М',
    data: [],
  },
  {
    title: 'Н',
    data: [],
  },
  {
    title: 'О',
    data: [],
  },
  {
    title: 'П',
    data: [],
  },
  {
    title: 'Р',
    data: [],
  },
  {
    title: 'С',
    data: [],
  },
  {
    title: 'Т',
    data: [],
  },
  {
    title: 'У',
    data: [],
  },
  {
    title: 'Ф',
    data: [],
  },
  {
    title: 'Х',
    data: [],
  },
  {
    title: 'Ц',
    data: [],
  },
  {
    title: 'Ч',
    data: [],
  },
  {
    title: 'Ш',
    data: [],
  },
  {
    title: 'Э',
    data: [],
  },
  {
    title: 'Ю',
    data: [],
  },
  {
    title: 'Я',
    data: [],
  },
]
