import { LocationGeocodedAddress } from 'expo-location'
import { TLocationError } from 'src/processes/location/use-location'
import { CityItemInstance } from 'src/stores/domains/geolocation.store'
import { citiesData } from '../mock/cities-data'

export const parseCities = (cities: CityItemInstance[]) => {
  return citiesData.map((citiesPart) => {
    if (citiesPart.title === null) {
      return citiesPart
    } else
      return {
        ...citiesPart,
        data: cities.filter(
          (item) =>
            citiesPart.title === item.city[0].toUpperCase() &&
            item.city_id > 2 &&
            item.city !== 'Тула' &&
            item.city !== 'Саратов',
        ),
      }
  })
}

export const shouldSheetOpen = (error: TLocationError, location: LocationGeocodedAddress[] | null) => {
  const ifError = error && [2, 3, 4].includes(error.code)
  const isFindLocation = !error && location != null

  return ifError || isFindLocation
}
