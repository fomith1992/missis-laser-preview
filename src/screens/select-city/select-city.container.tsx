import React, { useEffect, useState } from 'react'
import { useLocation } from 'src/processes/location/use-location'
import BottomSheet from '@gorhom/bottom-sheet'
import { RequestPermissionsBottomSheetView } from './ui/request-permissions-bottom-sheet.view'
import { CityRevisionBottomSheetView } from './ui/city-revision-bottom-sheet.view'
import { SelectCityView } from '@features/select-city/select-city.view'
import { CityNotFoundBottomSheetView } from './ui/city-not-found-bottom-sheet.view'
import { StackActions, useNavigation } from '@react-navigation/core'
import { RoutesRoot } from 'src/enums/routes'

import { useRootStore } from 'src/stores/lib/use-root-store'
import { observer } from 'mobx-react'
import { LoadingIndicator } from '@shared/ui/atoms/loading-indicator'
import useAsync from 'react-use/lib/useAsync'
import { BottomSheetGorhom } from '@features/bottom-sheet-gorhom/bottom-sheet'
import * as Analytics from 'expo-firebase-analytics'
import { parseCities, shouldSheetOpen } from './lib'
import { sleep } from '@shared/lib/promises'

interface SelectCityProps {
  sheetRef: React.RefObject<BottomSheet>
  isShortcut?: boolean
}

export const SelectCity = observer(({ sheetRef, isShortcut }: SelectCityProps): React.ReactElement => {
  const navigation = useNavigation()
  const [searchItem, setSearchItem] = useState('')
  const { location, error, requestLocationPermission, locationLoading } = useLocation()
  const { cities, setCurrentCityId, getCompanies } = useRootStore().geolocationReport

  const queryState = useAsync(async () => {
    if (cities.length === 0) await getCompanies()
  }, [])

  useAsync(
    async () =>
      await Analytics.logEvent('SelectCity', {
        screen: 'SelectCity',
      }),
    [],
  )

  useEffect(() => {
    const check = async () => {
      if (!queryState.loading && shouldSheetOpen(error, location)) {
        await sleep(250)
        sheetRef.current?.snapTo(1)
      }
    }

    check()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [location, error, queryState.loading])

  const parsedCities = parseCities(cities)

  const selectHandler = (city_id: number) => {
    setCurrentCityId(city_id)
    navigation.dispatch(StackActions.push(RoutesRoot.SelectClinic, { city: city_id, isShortcut }))
  }

  return (
    <>
      {queryState.loading || locationLoading ? (
        <LoadingIndicator visible />
      ) : (
        <SelectCityView
          searchItem={searchItem}
          setSearchItem={setSearchItem}
          items={parsedCities.filter((x) => x.data.length > 0)}
          onChange={selectHandler}
        />
      )}
      {error?.code !== 5 && error?.code !== 1 && (
        <>
          {(error?.code === 3 || error?.code === 4) && (
            <BottomSheetGorhom sheetRef={sheetRef}>
              <RequestPermissionsBottomSheetView
                onSelectCity={() => sheetRef.current?.close()}
                onSubmit={async () => {
                  await requestLocationPermission()
                  sheetRef.current?.close()
                }}
              />
            </BottomSheetGorhom>
          )}
          {!error && location != null && location[0].city !== null && (
            <BottomSheetGorhom sheetRef={sheetRef}>
              <CityRevisionBottomSheetView
                city={location[0].city}
                onSelectCity={() => sheetRef.current?.snapTo(0)}
                onSubmit={() => {
                  sheetRef.current?.snapTo(0)
                  const city = cities.find((x) => x.city === location[0].city)
                  if (city) {
                    selectHandler(city.city_id)
                  } else setSearchItem(location[0].city ?? '')
                }}
              />
            </BottomSheetGorhom>
          )}
          {error == null && location == null && (
            <BottomSheetGorhom sheetRef={sheetRef}>
              <CityNotFoundBottomSheetView onPress={() => sheetRef.current?.snapTo(0)} />
            </BottomSheetGorhom>
          )}
        </>
      )}
    </>
  )
})
