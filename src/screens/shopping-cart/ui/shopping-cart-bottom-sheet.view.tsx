import React from 'react'
import { BaseView, Body, ContentLayout, RowView, Title } from '@shared/ui'
import { TextButton } from '@shared/ui/buttons/text-button.view'
import BottomSheet, { TouchableWithoutFeedback } from '@gorhom/bottom-sheet'
import { Platform } from 'react-native'

interface ShoppingCartBottomSheetViewProps {
  onSubmit: () => void
  sheetRef: React.RefObject<BottomSheet>
}

export const ShoppingCartBottomSheetView = ({
  sheetRef,
  onSubmit,
}: ShoppingCartBottomSheetViewProps): React.ReactElement => {
  const isIos = Platform.OS === 'ios'
  const handlerSubmit = () => {
    sheetRef.current?.close()
    onSubmit()
  }

  return (
    <ContentLayout>
      <Title mt={20}>Уверены, что хотите удалить эту позицию из записи?</Title>
      <Body mt={12}>
        Чтобы восстановить ее, вам придется вернуться на несколько экранов назад и повторить действия по добавлению
      </Body>
      <RowView mt={20} mb={24}>
        <BaseView flex mr={4} pb={isIos ? 32 : 0}>
          <TouchableWithoutFeedback onPress={() => (!isIos ? sheetRef.current?.close() : undefined)}>
            <TextButton onPress={() => (isIos ? sheetRef.current?.close() : undefined)} type="secondary">
              Отмена
            </TextButton>
          </TouchableWithoutFeedback>
        </BaseView>
        <BaseView flex ml={4} pb={isIos ? 32 : 0}>
          <TouchableWithoutFeedback onPress={!isIos ? handlerSubmit : undefined}>
            <TextButton onPress={isIos ? handlerSubmit : undefined} type="error">
              Удалить
            </TextButton>
          </TouchableWithoutFeedback>
        </BaseView>
      </RowView>
    </ContentLayout>
  )
}
