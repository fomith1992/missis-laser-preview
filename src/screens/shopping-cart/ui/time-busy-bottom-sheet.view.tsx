import React from 'react'
import styled from '@emotion/native'

import { BaseView, Body, ColView, ContentLayout, Headiline } from '@shared/ui'
import { TextButton } from '@shared/ui/buttons/text-button.view'
import { color } from 'src/style/mixins/color'
import { SadIcon80 } from '@shared/ui/icons/sad.icon-80'
import { TouchableWithoutFeedback } from '@gorhom/bottom-sheet'
import { Platform } from 'react-native'

type InfoBottomSheetViewProps = {
  onSubmit: () => void
}

export function TimeBusyBottomSheetView({ onSubmit }: InfoBottomSheetViewProps) {
  const isIos = Platform.OS === 'ios'
  return (
    <ContentLayout>
      <IconWrapper mt={20}>
        <SadIcon />
      </IconWrapper>
      <Title mt={20}>Время уже заняли</Title>
      <Description mt={12}>Это время только что забронировал{'\n'}другой клиент. Выберите иное время.</Description>
      <BaseView mb={isIos ? 32 : 0}>
        <BaseView mt={20} mb={24}>
          <TouchableWithoutFeedback onPress={!isIos ? onSubmit : undefined}>
            <TextButton onPress={isIos ? onSubmit : undefined}>Выбрать другое время</TextButton>
          </TouchableWithoutFeedback>
        </BaseView>
      </BaseView>
    </ContentLayout>
  )
}

const IconWrapper = styled(ColView)`
  padding: 8px;
  align-self: center;
  border-radius: 48px;
  background-color: ${color('error15')};
`

const SadIcon = styled(SadIcon80)`
  width: 80px;
  height: 80px;
  color: ${color('error')};
`

const Description = styled(Body)`
  text-align: center;
`

const Title = styled(Headiline)`
  text-align: center;
`
