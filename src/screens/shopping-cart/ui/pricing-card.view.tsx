import styled from '@emotion/native'
import { BaseView, Body, ColView, Footnote, RowView, ZoneIcon } from '@shared/ui'
import { hitSlopParams } from '@shared/ui/buttons/hit-slop-params'
import { CloseIcon16 } from '@shared/ui/icons/close.icon-16'
import { TZoneIcons } from '@shared/ui/icons/zone.icon'
import React from 'react'

export interface PricingCardProps {
  title: string
  imageType: TZoneIcons
  description: string
  price: number
  onCancel: () => void
}

export function PricingCard({ title, description, onCancel, price, imageType }: PricingCardProps): React.ReactElement {
  return (
    <PricingCardContainer mt={20}>
      <Image>
        <ZoneIcon name={imageType} />
      </Image>
      <ColView ml={8} mr={12}>
        <Body numberOfLines={2}>{title}</Body>
        <Footnote mt={4}>{description}</Footnote>
      </ColView>
      <PriceContainer aiCenter ml="auto">
        <Body>{`${price} ₽`}</Body>
        <CloseButton onPress={onCancel} hitSlop={hitSlopParams('XL')}>
          <CloseIcon16 />
        </CloseButton>
      </PriceContainer>
    </PricingCardContainer>
  )
}

const Image = styled(BaseView)`
  min-width: 44px;
  height: 44px;
  align-items: center;
  justify-content: center;
  border-radius: 8px;
  border: 1px solid rgba(233, 233, 233, 0.9);
`

const CloseButton = styled.TouchableOpacity`
  margin-left: 4px;
`

const PriceContainer = styled(RowView)`
  flex-shrink: 0;
`

const PricingCardContainer = styled(RowView)`
  align-items: flex-start;
`
