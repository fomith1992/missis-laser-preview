import React, { useMemo, useRef, useState } from 'react'
import { Platform, StyleSheet } from 'react-native'
import styled from '@emotion/native'
import BottomSheet from '@gorhom/bottom-sheet'

import { BottomSheetGorhom } from '@features/bottom-sheet-gorhom/bottom-sheet'

import { BaseView, Body, Caption, ColView, HLine, RowView, Title } from '@shared/ui'
import { hitSlopParams } from '@shared/ui/buttons/hit-slop-params'
import { TextButton } from '@shared/ui/buttons/text-button.view'

import { CalendarIcon24 } from '@shared/ui/icons/calendar.icon-24'
import { CloseIcon16 } from '@shared/ui/icons/close.icon-16'
import { ClubCardIcon } from '@shared/ui/icons/club-card.icon'
import { LocationIcon24 } from '@shared/ui/icons/location.icon-24'
import { UserIcon24 } from '@shared/ui/icons/user.icon-24'

import { color } from 'src/style/mixins/color'
import { container } from 'src/style/mixins/space'

import { PricingCard, PricingCardProps } from './pricing-card.view'
import { ShoppingCartBottomSheetView } from './shopping-cart-bottom-sheet.view'

import Animated from 'react-native-reanimated'

interface ShoppingCartViewProps {
  cardsData: PricingCardProps[]
  workerName?: string
  workerPosition?: string
  address: string | null
  date?: string
  isPricingClubCard?: boolean
  onMakeAppointment: () => void
}

export function ShoppingCartView({
  cardsData,
  date,
  address,
  workerName,
  workerPosition,
  isPricingClubCard = false,
  onMakeAppointment,
}: ShoppingCartViewProps): React.ReactElement {
  const sheetRef = useRef<BottomSheet>(null)
  const [deletedItem, setDeletedItem] = useState<number | null>(null)
  const [pricingClubCard, setPricingClubCard] = useState<boolean>(isPricingClubCard)

  const isIos = Platform.OS === 'ios'

  const cartItemsCost = useMemo(() => {
    let sum = 0
    for (const salary of Object.values(cardsData)) {
      sum += salary.price
    }
    return sum
  }, [cardsData])

  return (
    <>
      <BodyContainer contentContainerStyle={styles.scrollViewContainer}>
        {date && (
          <RowView mt={16}>
            <CalendarIcon24 style={styles.iconSize} />
            <Body ml={12}>{date}</Body>
          </RowView>
        )}
        {address && (
          <RowView mt={16}>
            <LocationIcon24 style={styles.iconSize} />
            <Body ml={12}>{address}</Body>
          </RowView>
        )}
        {workerName && (
          <RowView mt={16}>
            <UserIcon24 style={styles.iconSize} />
            <Body ml={12}>
              {workerName}, {workerPosition?.toLowerCase()}
            </Body>
          </RowView>
        )}
        <Title mt={28}>Услуги</Title>
        {cardsData.map((item, idx) => (
          <PricingCard
            key={idx + item.price}
            imageType={item.imageType}
            title={item.title}
            description={item.description}
            price={item.price}
            onCancel={() => {
              setDeletedItem(idx)
              sheetRef.current?.expand()
            }}
          />
        ))}
        <HLine mt={20} mb={20} />
        {pricingClubCard ? (
          <>
            <Title>Карта</Title>
            <PricingCardContainer mt={20}>
              <ClubCardSvg />
              <ColView ml={4}>
                <Body>Клубная карта{'\n'}на 3 месяца</Body>
                <Caption mt={4} color="secondaryText">
                  Скидки до 70%, закрытые{'\n'}распродажи и эксклюзивные{'\n'}
                  статьи и советы
                </Caption>
              </ColView>
              <RowView aiCenter ml="auto">
                <Body>{`0 ₽`}</Body>
                <CloseButton onPress={() => setPricingClubCard(!pricingClubCard)} hitSlop={hitSlopParams('XL')}>
                  <CloseIcon16 />
                </CloseButton>
              </RowView>
            </PricingCardContainer>
            <HLine mt={20} mb={20} />
          </>
        ) : null}
        <RowView aiCenter mb={96}>
          <BaseView>
            <Title>Всего</Title>
            <Body mt={8}>Оплата на месте</Body>
          </BaseView>
          <RowView aiCenter ml="auto">
            <Title>{cartItemsCost} ₽</Title>
          </RowView>
        </RowView>
      </BodyContainer>
      <BaseView mb={isIos ? 32 : 0}>
        <BottomButtonContainer pointerEvents={'auto'}>
          <TextButton onPress={onMakeAppointment}>Записаться (оплата на месте)</TextButton>
        </BottomButtonContainer>
      </BaseView>
      <BottomSheetGorhom sheetRef={sheetRef}>
        <ShoppingCartBottomSheetView
          onSubmit={() => (deletedItem != null ? cardsData[deletedItem].onCancel() : null)}
          sheetRef={sheetRef}
        />
      </BottomSheetGorhom>
    </>
  )
}

const styles = StyleSheet.create({
  scrollViewContainer: {
    flexGrow: 1,
  },
  iconSize: {
    width: 20,
    height: 20,
  },
})

const BodyContainer = styled.ScrollView`
  flex-grow: 1;
  ${container('padding')}
  background-color: ${color('background')}
`

const CloseButton = styled.TouchableOpacity`
  margin-left: 4px;
`

const PricingCardContainer = styled(RowView)`
  align-items: flex-start;
`
const ClubCardSvg = styled(ClubCardIcon)`
  color: ${color('primary')};
`
const BottomButtonContainer = styled(Animated.View)`
  position: absolute;
  bottom: 24px;
  left: 16px;
  right: 16px;
`
