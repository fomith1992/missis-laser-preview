import React from 'react'
import { WebView } from 'react-native-webview'

interface PaymentWebViewProps {
  uri: string
  onPaymentComplete: () => void
  onPaymentCancel: () => void
}

export function PaymentWebView({ uri, onPaymentComplete, onPaymentCancel }: PaymentWebViewProps): React.ReactElement {
  return (
    <WebView
      source={{ uri }}
      onNavigationStateChange={(event) => {
        if (event.url.includes('payments/external/success')) {
          onPaymentComplete()
        }
        if (event.url.includes('/booking-info/')) {
          onPaymentCancel()
        }
      }}
    />
  )
}
