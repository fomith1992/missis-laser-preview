import React, { useRef, useState } from 'react'
import useAsync from 'react-use/lib/useAsync'
import useAsyncFn from 'react-use/lib/useAsyncFn'

import { AxiosError } from 'axios'
import { observer } from 'mobx-react'

import BottomSheet from '@gorhom/bottom-sheet'
import { StackActions, useNavigation } from '@react-navigation/core'

import { BottomSheetGorhom } from '@features/bottom-sheet-gorhom/bottom-sheet'
import { bookAPI, geoAPI, promocodeAPI } from '@shared/api'
import { TResponse } from '@shared/api/types'
import { formatISOdate } from '@shared/lib/date'
import { LoadingIndicator } from '@shared/ui/atoms/loading-indicator'
import { TZoneIcons } from '@shared/ui/icons/zone.icon'

import { RoutesRoot } from 'src/enums/routes'
import { useRootStore } from 'src/stores/lib/use-root-store'

import { ShoppingCartView } from '../ui/shopping-cart.view'
import { TimeBusyBottomSheetView } from '../ui/time-busy-bottom-sheet.view'
import { PaymentWebView } from './payment.webview'

export const ShoppingCart = observer(() => {
  const sheetRef = useRef<BottomSheet>(null)
  const navigation = useNavigation()

  const { currentClinicId } = useRootStore().geolocationReport
  const { name, phone, email, user_token } = useRootStore().user
  const { currentDateTime, getCurrentStaff, currentStaff, currentServices, setCurrentServices, promocode, reset } =
    useRootStore().appointmentReport
  const { data } = useRootStore().serviceListReport

  const [webViewUri, setWebViewUri] = useState<null | string>(null)
  const [recordId, setRecordId] = useState<null | number>(null)

  const currentClinicQuery = useAsync(async () => {
    return await geoAPI.getCompany(currentClinicId)
  })

  const currentServicesQuery = useAsync(
    async () =>
      await Promise.all(
        currentServices.map(
          async (serviceId) =>
            data?.find((item) => item.id === serviceId) ??
            (
              await bookAPI.getServiceByServiceId({
                serviceId,
                clinic_id: currentClinicId,
                user_token: user_token ?? '',
              })
            ).data.data,
        ),
      ),
    [currentServices.join('')],
  )

  const [queryState, onSendQuery] = useAsyncFn(async () => {
    if (phone && user_token && currentStaff != null && currentDateTime) {
      await bookAPI
        .createAppointment({
          selectedServices: currentServices,
          workerId: currentStaff,
          date: currentDateTime,
          name: name ? name : phone,
          phone,
          email,
          clinic_id: currentClinicId,
          chargesItems: currentServicesQuery.value?.filter((x) => x.prepaid === 'required').map((y) => y.id) || null,
        })
        .then(({ data: { data } }) => {
          if (promocode) {
            promocodeAPI.activatePromocode({ phone, user_token, key: promocode })
          }

          setRecordId(data[0].record_id)
          if (data.charge != null) {
            setWebViewUri(data.charge.url)
          } else {
            reset()
            navigation.dispatch(
              StackActions.push(RoutesRoot.SuccessAppointment, {
                appointmentId: data[0].record_id,
                companyId: currentClinicId,
              }),
            )
          }
        })
        .catch((e: AxiosError<TResponse>) => {
          if (e.response?.status && e.response?.status > 400 && e.response?.status < 500) {
            sheetRef.current?.snapTo(1)
          }
          console.log('createAppointment error ', e)
        })
    } else {
      console.log('Недостаточно данных для отправки запроса')
    }
  }, [currentServices, currentStaff, currentDateTime, name, phone, email, currentServicesQuery.value])

  if (webViewUri)
    return (
      <PaymentWebView
        onPaymentCancel={() => {
          setWebViewUri(null)
        }}
        onPaymentComplete={() => {
          reset()
          navigation.dispatch(
            StackActions.push(RoutesRoot.SuccessAppointment, {
              appointmentId: recordId,
              companyId: currentClinicId,
            }),
          )
        }}
        uri={webViewUri}
      />
    )

  return (
    <>
      <ShoppingCartView
        cardsData={
          currentServicesQuery?.value?.map((service) => ({
            imageType: (service?.imageType as TZoneIcons) ?? 'bodyBodyFullAndHands',
            title: service?.title ?? 'Эпиляция',
            description: 'Эпиляция',
            price: service?.price_max ?? 0,
            onCancel: () => {
              if (currentServices.length === 1) {
                setCurrentServices([])
                navigation.dispatch(StackActions.push(RoutesRoot.BottomNavigator))
              } else {
                setCurrentServices(
                  currentServicesQuery?.value?.map((x) => x?.id ?? 0).filter((y) => y !== service?.id) ?? [],
                )
              }
            },
          })) ?? []
        }
        date={currentDateTime && formatISOdate(currentDateTime?.toString())}
        address={currentClinicQuery.value?.data.data?.address ?? null}
        workerName={getCurrentStaff()?.name}
        workerPosition={getCurrentStaff()?.specialization}
        onMakeAppointment={onSendQuery}
      />
      <BottomSheetGorhom sheetRef={sheetRef}>
        <TimeBusyBottomSheetView
          onSubmit={() => {
            sheetRef.current?.snapTo(0)
            navigation.goBack()
          }}
        />
      </BottomSheetGorhom>
      <LoadingIndicator visible={queryState.loading || currentClinicQuery.loading} />
    </>
  )
})
