import React from 'react'
import { ShoppingCart } from './lib/shopping-cart.container'
import useAsync from 'react-use/lib/useAsync'
import * as Analytics from 'expo-firebase-analytics'

export function ShoppingCartScreen() {
  useAsync(
    async () =>
      await Analytics.logEvent('ShoppingCartScreen', {
        screen: 'ShoppingCartScreen',
      }),
    [],
  )

  return <ShoppingCart />
}
