import React, { useRef } from 'react'
import { SafeAreaView } from 'react-native-safe-area-context'
import useAsyncFn from 'react-use/lib/useAsyncFn'

import { AxiosError } from 'axios'

import BottomSheet from '@gorhom/bottom-sheet'
import { CommonActions, StackActions } from '@react-navigation/core'
import { StackScreenProps } from '@react-navigation/stack'

import { BottomSheetGorhom } from '@features/bottom-sheet-gorhom/bottom-sheet'
import { TimeBusyBottomSheetView } from '@features/bottom-sheet-gorhom/templates/time-busy-bottom-sheet.view'
import { DateTimeSelect } from '@features/date-time-select'
import { RoutesTabs } from '@navigations/bottom-navigator/routes'
import { RoutesAppointments } from '@navigations/stack-appointments/routes'
import { RootStackParamList } from '@navigations/types'
import { recordsAPI } from '@shared/api'
import { ContentLayout } from '@shared/ui/layouts/content-layout'

import { RoutesRoot } from 'src/enums/routes'
import { useRootStore } from 'src/stores/lib/use-root-store'

type DateTimeSelectProps = StackScreenProps<RootStackParamList, RoutesRoot.DateTimeSelect>

export const DateTimeSelectScreen = ({ navigation, route }: DateTimeSelectProps) => {
  const { selectedServices, workerId, excludeRecordId, isMoveRecord, appointmentId, companyId } = route.params
  const { setAppointmentData } = useRootStore().appointmentReport
  const { user_token } = useRootStore().user

  const sheetRef = useRef<BottomSheet>(null)
  const updateError = useRef(false)

  const [queryState, updateRecord] = useAsyncFn(
    async (datetime: string) => {
      if (!user_token || !isMoveRecord || !appointmentId || !companyId) return
      updateError.current = false
      return await recordsAPI
        .updateRecordById({
          companyId,
          user_token,
          recordId: appointmentId,
          body: { datetime },
        })
        .catch((error: AxiosError) => {
          const code = error.response?.status || 422
          if (code >= 400 && code < 500) {
            updateError.current = true
          }
        })
    },
    [isMoveRecord, appointmentId],
  )

  const moveRecordConfirm = async (time: string) => {
    await updateRecord(time).then(() => {
      if (updateError.current) {
        sheetRef.current?.expand()
      } else {
        navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [
              {
                name: RoutesRoot.BottomNavigator,
                params: {
                  screen: RoutesTabs.TabAppointments,
                  initial: false,
                  params: {
                    initial: false,
                    screen: RoutesAppointments.Appointment,
                    params: { appointmentId, companyId },
                  },
                },
              },
            ],
          }),
        )
      }
    })
  }

  const orderConfirm = (time: string) => {
    setAppointmentData({
      currentServices: selectedServices,
      currentStaff: workerId,
      currentDateTime: time,
    })
    navigation.dispatch(StackActions.push(RoutesRoot.ShoppingCart))
  }

  return (
    <>
      <SafeAreaView>
        <ContentLayout>
          <DateTimeSelect
            recordUpdating={queryState.loading}
            selectedServices={selectedServices}
            moveRecordCompanyId={companyId}
            workerId={workerId}
            excludeRecordId={excludeRecordId}
            onConfirm={(time) => (isMoveRecord ? moveRecordConfirm(time) : orderConfirm(time))}
          />
        </ContentLayout>
      </SafeAreaView>
      <BottomSheetGorhom sheetRef={sheetRef}>
        <TimeBusyBottomSheetView onSubmit={() => sheetRef.current?.close()} />
      </BottomSheetGorhom>
    </>
  )
}
