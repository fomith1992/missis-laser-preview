import { ZonesList } from '@features/zones-list'
import { StackActions, useNavigation } from '@react-navigation/core'
import React from 'react'
import { RoutesRoot } from 'src/enums/routes'
import * as Analytics from 'expo-firebase-analytics'
import useAsync from 'react-use/lib/useAsync'

export const ZonesSelectScreen = () => {
  const nav = useNavigation()

  useAsync(
    async () =>
      await Analytics.logEvent('ZonesSelectScreen', {
        screen: 'ZonesSelectScreen',
      }),
    [],
  )

  return (
    <ZonesList
      onConfirm={(items) => {
        nav.dispatch(
          StackActions.push(RoutesRoot.WorkerSelect, {
            selectedServices: items,
          }),
        )
      }}
    />
  )
}
