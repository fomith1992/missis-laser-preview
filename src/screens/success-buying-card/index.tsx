import React from 'react'
import useAsync from 'react-use/lib/useAsync'
import * as Analytics from 'expo-firebase-analytics'
import { useNavigation } from '@react-navigation/core'
import { CommonActions } from '@react-navigation/native'
import { ProccessStateView } from '@features/proccess-state/proccess-state.view'
import { SafeAreaView } from '@shared/ui'

import { RoutesRoot } from 'src/enums/routes'

export function SuccessBuyingCardScreen(): React.ReactElement {
  const navigation = useNavigation()

  useAsync(
    async () =>
      await Analytics.logEvent('SuccessBuyingCardScreen', {
        screen: 'SuccessBuyingCardScreen',
      }),
    [],
  )

  return (
    <SafeAreaView>
      <ProccessStateView
        title="Успех"
        description={`Вы приобрели карту лояльности`}
        buttonText="Спасибо, понятно"
        onPress={() =>
          navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [
                {
                  name: RoutesRoot.BottomNavigator,
                  params: {},
                },
              ],
            }),
          )
        }
      />
    </SafeAreaView>
  )
}
