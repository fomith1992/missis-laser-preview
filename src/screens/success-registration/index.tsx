import React from 'react'
import { SafeAreaView } from '@shared/ui'
import { ProccessStateView } from '@features/proccess-state/proccess-state.view'
import { StackActions, useNavigation } from '@react-navigation/core'
import { RoutesRoot } from 'src/enums/routes'
import useAsync from 'react-use/lib/useAsync'
import * as Analytics from 'expo-firebase-analytics'

export function SuccessRegistrationScreen(): React.ReactElement {
  const nav = useNavigation()

  useAsync(
    async () =>
      await Analytics.logEvent('SuccessRegistrationScreen', {
        screen: 'SuccessRegistrationScreen',
      }),
    [],
  )

  return (
    <SafeAreaView>
      <ProccessStateView
        title="Ваш аккаунт создан!"
        description={`Теперь вы можете записываться${'\n'}на процедуры `}
        buttonText="Спасибо, понятно"
        onPress={() => nav.dispatch(StackActions.replace(RoutesRoot.BottomNavigator))}
      />
    </SafeAreaView>
  )
}
