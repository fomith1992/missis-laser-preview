import React from 'react'

import { ServicesItem } from './service.item'

import { StackActions, useNavigation } from '@react-navigation/core'
import { RoutesRoot } from 'src/enums/routes'
import { useRootStore } from 'src/stores/lib/use-root-store'
import image1 from '@entities/service/ui/1.png'
import image2 from '@entities/service/ui/2.png'
import image3 from '@entities/service/ui/3.png'

export const ServicesList = () => {
  const navigation = useNavigation()
  const { currentClinicId } = useRootStore().geolocationReport

  const data = [
    {
      photo: image1,
      title: 'Диодный лазер',
      description: 'Лучший способ избавиться от лишних волос',
      onPress: () => navigation.dispatch(StackActions.push(RoutesRoot.ProcedureSelectDiod)),
    },
  ]
  if (currentClinicId === 387989) {
    data.push({
      photo: image2,
      title: 'Александритовый лазер',
      description: 'Лазерная эпиляция с моментальным эффектом (для темных волос на светлой коже)',
      onPress: () => navigation.dispatch(StackActions.push(RoutesRoot.ProcedureSelectAlexandrit)),
    })
  }
  if (currentClinicId === 24642) {
    data.push({
      photo: image3,
      title: 'Косметология',
      description: 'Красота без хирургического вмешательства',
      onPress: () => navigation.dispatch(StackActions.push(RoutesRoot.ProcedureListCosmetology)),
    })
  }

  return (
    <>
      {data.map((item, id) => (
        <ServicesItem key={id} item={item} />
      ))}
    </>
  )
}
