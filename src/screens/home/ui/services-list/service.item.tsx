import React from 'react'
import styled from '@emotion/native'
import { TouchableOpacity } from 'react-native'

import { TServicesItem } from '@entities/service/model/types'

import { SubTitle } from '@shared/ui/atoms/text'
import { Footnote, RowView } from '@shared/ui'

interface Props {
  item: TServicesItem
}

export const ServicesItem: React.FC<Props> = ({ item }) => {
  return (
    <TouchableOpacity onPress={item.onPress}>
      <RowView mt={20}>
        <Image source={item.photo} />
        <Info>
          <SubTitle>{item.title}</SubTitle>
          <Footnote mt={4}>{item.description}</Footnote>
        </Info>
      </RowView>
    </TouchableOpacity>
  )
}

const Image = styled.Image`
  width: 88px;
  height: 88px;
  border-radius: 16px;
`

const Info = styled.View`
  margin: 12px 0 12px 16px;
  flex: 1;
  justify-content: center;
`
