import React from 'react'
import { TouchableOpacity } from 'react-native'

import { Footnote, RowView } from '@shared/ui'
import styled from '@emotion/native'

type HomeHeaderLeftProps = {
  city: string
  onPress: () => void
}

export const HomeHeaderLeft = ({ city, onPress }: HomeHeaderLeftProps) => {
  return (
    <TouchableOpacity onPress={onPress} activeOpacity={0.6}>
      <RowView aiCenter ml={16}>
        <Footnote color="primaryText">{city}</Footnote>
        <Triangle />
      </RowView>
    </TouchableOpacity>
  )
}

const Triangle = styled.View`
  width: 0;
  height: 0;
  margin-left: 8px;
  margin-top: 2px;
  border-style: solid;
  border-width: 6px 5px 0 5px;
  border-color: #000000 transparent transparent transparent;
`
