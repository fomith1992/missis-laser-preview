import React from 'react'
import { FlatList, StyleSheet } from 'react-native'

import { SlideItem } from './ui/item'
import useAsync from 'react-use/lib/useAsync'
import { buyCardAPI } from '@shared/api'

export function PromotionSlider() {
  const currentDataQuery = useAsync(async () => {
    return await buyCardAPI.getHomeCards()
  })

  if (currentDataQuery.loading) {
    return <></>
  }
  return (
    <FlatList
      showsHorizontalScrollIndicator={false}
      contentContainerStyle={styles.container}
      data={
        currentDataQuery.value?.data.map((item, idx) => ({
          ...item,
          important: idx === 0,
        })) ?? []
      }
      horizontal
      renderItem={(props) => <SlideItem {...props} />}
      keyExtractor={(_, idx) => idx.toString()}
    />
  )
}

const styles = StyleSheet.create({
  container: {
    paddingLeft: 10,
    paddingRight: 10,
  },
})
