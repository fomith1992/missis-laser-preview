export interface TSlideEvent {
  title: string[]
  price: {
    total: number
    discount?: number
    currency: string
    isFrom?: boolean
  }
  important: boolean
  type: 'event'
  cardId: number
}

export interface TPromoClubCard {
  title: string[]
  price: {
    total: number
    discount?: number
    currency: string
    isFrom?: boolean
  }
  important: boolean
  type: 'clubCard' | 'millerCard6' | 'millerCard3'
  discountText: string
  cardId: number
}

export type TPromoSlide = TSlideEvent | TPromoClubCard

export const mockData: TPromoSlide[] = [
  {
    title: ['Бикини +', 'Подмышки +', 'Голени'],
    price: {
      total: 2990,
      discount: 1000,
      currency: '₽',
    },
    important: false,
    type: 'event',
    cardId: 7938094,
  },
  {
    title: ['Карта', 'Миллер карта 6'],
    price: {
      total: 990,
      currency: '₽',
      isFrom: true,
    },
    important: false,
    type: 'millerCard6',
    cardId: 161983,
    discountText: 'Скидки 10%!',
  },
  {
    title: ['Карта', 'Миллер карта 3'],
    price: {
      total: 550,
      currency: '₽',
      isFrom: true,
    },
    important: false,
    type: 'millerCard3',
    cardId: 162241,
    discountText: 'Скидки 10%!  ',
  },
]
