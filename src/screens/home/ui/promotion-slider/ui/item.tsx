import React from 'react'

import styled from '@emotion/native'
import { ListRenderItemInfo, TouchableOpacity, View } from 'react-native'

import { BaseView, Body, RowView } from '@shared/ui'
import { TPromoSlide } from '../mock/data'
import { color } from 'src/style/mixins/color'
import { SlideBodyIcon24 } from '@shared/ui/icons/slide-body.icon'
import { SlideCardIcon24 } from '@shared/ui/icons/slide-card.icon'

import image1 from '../mock/test.png'

import { StackActions, useNavigation } from '@react-navigation/core'
import { RoutesRoot } from 'src/enums/routes'

export const SlideItem = ({ item }: ListRenderItemInfo<TPromoSlide>): React.ReactElement => {
  const currentPrice = item.price.total - (item.price?.discount || 0)
  const curr = item.price.currency

  const navigation = useNavigation()

  const SlideImage = (imageType: 'event' | 'clubCard'): React.ReactElement => {
    switch (imageType) {
      case 'clubCard':
        return <SlideCardIcon24 />
      case 'event':
        return <SlideBodyIcon24 />
    }
  }

  return (
    <TouchableOpacity
      onPress={() =>
        item.type === 'event'
          ? navigation.dispatch(
              StackActions.push(RoutesRoot.EventScreen, {
                itemId: item.cardId,
                minPrice: item.price.total - (item.price.discount ?? 0),
              }),
            )
          : navigation.dispatch(
              StackActions.push(RoutesRoot.MillerCardLanding, {
                type: item.type,
                itemId: item.cardId,
                itemPrice: item.price.total,
              }),
            )
      }
    >
      <Slide important={item.important}>
        <SlideContent>
          <View>
            {item.title.map((text, id) => (
              <Body bold key={id} color={item.important ? 'lightText' : 'primaryText'}>
                {text}
              </Body>
            ))}
          </View>
          {item.type !== 'event' && (
            <DiscountText style={{ overflow: 'hidden', borderRadius: 20 }} center color="lightText">
              {item.discountText}
            </DiscountText>
          )}
          <RowView>
            <Body bold color={item.important ? 'lightText' : 'primaryText'}>
              {item.price.isFrom && 'от'} {currentPrice} {curr}{' '}
            </Body>
            {item.price.discount && (
              <Body strikethrough={true} color={item.important ? 'lightText' : 'primaryText'}>
                {item.price.total} {curr}
              </Body>
            )}
          </RowView>
        </SlideContent>
        {item.type === 'event' ? (
          <BaseView style={{ left: 20 }}>{SlideImage(item.type)}</BaseView>
        ) : (
          <BaseView style={{ left: 10 }}>
            <Image source={image1} />
          </BaseView>
        )}
      </Slide>
    </TouchableOpacity>
  )
}

const Image = styled.Image`
  width: 112px;
  height: 160px;
  border-radius: 16px;
`

const Slide = styled.View<{ important: boolean }>`
  width: 240px;
  height: 160px;
  padding: 16px;
  border-radius: 16px;
  background-color: ${({ important }) => color(important ? 'primary' : 'surface')};
  margin: 0 6px;
  flex-direction: row;
  overflow: hidden;
`

const SlideContent = styled.View`
  flex-grow: 1;
  justify-content: space-between;
`

const DiscountText = styled(Body)`
  padding: 8px 12px;
  background-color: ${color('primary')};
`
