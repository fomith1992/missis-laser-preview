import React from 'react'
import { ScrollView } from 'react-native'
import useAsync from 'react-use/lib/useAsync'

import * as Analytics from 'expo-firebase-analytics'
import { observer } from 'mobx-react'

import { StackActions, useNavigation } from '@react-navigation/core'

import { RoutesHome } from '@navigations/stack-home/routes'
import { BaseView, ContentLayout, SafeAreaView, Title } from '@shared/ui'
import { useDefaultHeader } from '@shared/ui/header/use-default-header'

import { RoutesRoot } from 'src/enums/routes'
import { useRootStore } from 'src/stores/lib/use-root-store'

import { HomeHeaderLeft } from './ui/header-left.view'
import { PromotionSlider } from './ui/promotion-slider'
import { ServicesList } from './ui/services-list'

export const HomeScreen = observer(() => {
  const navigation = useNavigation()
  const { getCityByCityId, currentCityId } = useRootStore().geolocationReport

  useAsync(
    async () =>
      await Analytics.logEvent('HomeScreen', {
        screen: 'HomeScreen',
      }),
    [],
  )

  useDefaultHeader({
    actions: [
      {
        icon: 'promo',
        onPress: () => navigation.dispatch(StackActions.push(RoutesRoot.PromoCode, { isFromMain: true })),
      },
      {
        icon: 'settings',
        onPress: () => navigation.dispatch(StackActions.push(RoutesHome.Settings)),
      },
    ],
    headerLeft: (
      <HomeHeaderLeft
        city={getCityByCityId(currentCityId)?.city ?? 'Москва'}
        onPress={() => navigation.dispatch(StackActions.push(RoutesRoot.SelectCity, { isShortcut: true }))}
      />
    ),
  })

  return (
    <SafeAreaView topOff>
      <ScrollView>
        <BaseView mt={8}>
          <PromotionSlider />
        </BaseView>
        <ContentLayout mt={28} mb={20}>
          <Title>Услуги</Title>
          <ServicesList />
        </ContentLayout>
      </ScrollView>
    </SafeAreaView>
  )
})
