import React, { useState } from 'react'
import { ScrollView, StyleSheet } from 'react-native'
import styled from '@emotion/native'
import { StackActions } from '@react-navigation/core'
import { StackScreenProps } from '@react-navigation/stack'

import MissisLaserImage from '@entities/procedure/ui/missis-laser.large.png'
import { RootStackParamList } from '@navigations/types'
import { Body, ColView, ContentLayout, Headiline, HLine, RowView, SafeAreaView, TextButton, Title } from '@shared/ui'
import { LayoutWithBackButton } from '@shared/ui/layouts/layout-with-back-button'

import { RoutesRoot } from 'src/enums/routes'
import { color } from 'src/style/mixins/color'

import { ImportantsCells } from './ui/promocode/important-cells'
import { LargeCells } from './ui/promocode/large.cells'

import { HappyIcon24 } from '@shared/ui/icons/happy.icon-24'
import { HealthIcon64 } from '@shared/ui/icons/health.icon-64'
import { ThumbUpIcon24 } from '@shared/ui/icons/thumb-up.icon-24'

import { AccuracyIcon24 } from '../../shared/ui/icons/accuracy.icon-24'
import { SpeedIcon24 } from '../../shared/ui/icons/speed.icon-24'
import useAsync from 'react-use/lib/useAsync'
import { buyCardAPI } from '@shared/api'
import { useRootStore } from 'src/stores/lib/use-root-store'
import { LoadingIndicator } from '@shared/ui/atoms/loading-indicator'
import { PaymentWebView } from '@screens/shopping-cart/lib/payment.webview'

const millerCard3months = 161983 // id loyality card
const millerCard6months = 161983 // id loyality card

type MillerCardScreenProps = StackScreenProps<RootStackParamList, RoutesRoot.MillerCardLanding>

export const MillerCardScreen = ({ navigation, route }: MillerCardScreenProps) => {
  const { type } = route.params
  const { name, phone, email, user_token } = useRootStore().user

  const [webViewVisible, setWebViewVisible] = useState<boolean>(false)

  const currentClinicQuery = useAsync(async () => {
    if (phone && user_token) {
      return await buyCardAPI.requestForLink({
        chargesItemId: type === 'millerCard3' ? millerCard3months : millerCard6months,
        email,
        name: name ?? phone,
        phone: phone,
      })
    } else return null
  })

  if (webViewVisible && currentClinicQuery.value?.data.data.charge?.url)
    return (
      <SafeAreaView>
        <PaymentWebView
          onPaymentCancel={() => {
            setWebViewVisible(false)
          }}
          onPaymentComplete={() => {
            navigation.dispatch(StackActions.push(RoutesRoot.SuccessBuyingCard, {}))
          }}
          uri={currentClinicQuery.value?.data.data.charge?.url}
        />
      </SafeAreaView>
    )

  return (
    <>
      <LayoutWithBackButton onPress={() => navigation.goBack()}>
        <ScrollView style={styles.scrollView}>
          <ContentLayout flex mt={64} mb={24}>
            <ProcedureImage source={MissisLaserImage} />
            <Headiline mt={28}>Миллер карта ({type === 'millerCard3' ? '3 месяца' : '6 месяцев'})</Headiline>
            <Body mt={12} mb={20}>
              Купить карту по самой гладкой цене
            </Body>
            <ImportantsCells
              cells={[
                {
                  icon: HappyIcon24,
                  title: 'Выгодно',
                  description: 'Получайте больше за те же деньги',
                },
                {
                  icon: ThumbUpIcon24,
                  title: 'Эксклюзивно',
                  description: 'Получите доступ к эксклюзивным предложениям',
                },
              ]}
            />
            <HLine mt={28} mb={28} />
            <Body>Откройте больше возможностей с клубной картой чтобы чувствовать себя прекрасной каждый день!</Body>
            <HLine mt={28} mb={28} />
            <LargeCells
              cells={[
                {
                  icon: SpeedIcon24,
                  title: 'Экономия',
                  description: 'С картой вы экономите до 70% на стоимости услуг.',
                },
                {
                  icon: AccuracyIcon24,
                  title: 'Эксперты сферы красоты',
                  description:
                    'Косметологи сети «Миссис Лазер» — это специалисты с медицинским образованием. Наши эксперты подберут вам индивидуальную программу ухода, которая даст максимальный результат.',
                },
                {
                  icon: HealthIcon64,
                  title: 'Сервис высшего класса',
                  description:
                    'В клиниках «Миссис Лазер» вас всегда встретит комфортная атмосфера и внимательный персонал. Мы относимся к клиентам, как к лучшим друзьям и предлагаем качественные процедуры по выгодной цене.',
                },
              ]}
            />
            <HLine mt={28} mb={28} />
            <Body>
              Вот почему тысячи женщин доверили уход за своим лицом и телом экспертам сети клиник «Миссис Лазер»
            </Body>
          </ContentLayout>
        </ScrollView>
        <FixedContainer style={styles.shadow}>
          <Title>{`${type === 'millerCard3' ? 550 : 990} ₽`}</Title>
          <ButtonContainer>
            <TextButton onPress={() => setWebViewVisible(true)}>Купить</TextButton>
          </ButtonContainer>
        </FixedContainer>
      </LayoutWithBackButton>
      <LoadingIndicator visible={currentClinicQuery.loading} />
    </>
  )
}

const ProcedureImage = styled.Image`
  width: 100%;
  height: 216px;
  border-radius: 32px;
`

const FixedContainer = styled(RowView)`
  height: 76px;
  width: 100%;
  padding: 16px;
  align-items: center;
  justify-content: space-between;
  position: absolute;
  background-color: ${color('white')};
  bottom: 0;
`

const ButtonContainer = styled(ColView)`
  flex: 1;
  margin-left: 48px;
`

const styles = StyleSheet.create({
  shadow: {
    shadowRadius: 2,
    shadowOffset: {
      width: 0,
      height: -3,
    },
    shadowColor: '#000000',
    elevation: 10,
  },
  scrollView: {
    marginBottom: 76,
  },
})
