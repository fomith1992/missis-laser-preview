import React from 'react'
import { ScrollView, StyleSheet } from 'react-native'
import useAsync from 'react-use/lib/useAsync'

import styled from '@emotion/native'
import { StackActions } from '@react-navigation/core'
import { StackScreenProps } from '@react-navigation/stack'

import AlexLaserImage from '@entities/procedure/ui/alex-laser.large.png'
import { RootStackParamList } from '@navigations/types'
import { bookAPI } from '@shared/api'
import { Body, ColView, ContentLayout, Headiline, HLine, RowView, TextButton, Title } from '@shared/ui'
import { LoadingIndicator } from '@shared/ui/atoms/loading-indicator'
import { LayoutWithBackButton } from '@shared/ui/layouts/layout-with-back-button'

import { RoutesRoot } from 'src/enums/routes'
import { useRootStore } from 'src/stores/lib/use-root-store'
import { color } from 'src/style/mixins/color'

import { promocodeConfig } from './promo-code-landing.config'
import { Benefits, FAQ } from './ui'
import { ImportantsCells } from './ui/promocode/important-cells'
import { LargeCells } from './ui/promocode/large.cells'
import { WatchVideo } from './ui/promocode/watch-media'

type PromoCodeLandingScreenProps = StackScreenProps<RootStackParamList, RoutesRoot.PromoCodeLanding>

export const PromoCodeLandingScreen = ({ navigation }: PromoCodeLandingScreenProps) => {
  const { promocodeLanding, currentServices } = useRootStore().appointmentReport
  const { currentClinicId } = useRootStore().geolocationReport
  const { user_token } = useRootStore().user

  const currentServiceQuery = useAsync(
    async () =>
      (
        await bookAPI.getServiceByServiceId({
          serviceId: currentServices[0],
          clinic_id: currentClinicId,
          user_token: user_token ?? '',
        })
      ).data.data,
    [currentServices.join('')],
  )

  return (
    <LayoutWithBackButton onPress={() => navigation.goBack()}>
      <ScrollView style={styles.scrollView}>
        <ContentLayout flex mt={64} mb={24}>
          <ProcedureImage source={AlexLaserImage} />
          <Headiline mt={28}>{promocodeLanding?.headline}</Headiline>
          <Body mt={12} mb={20}>
            Абсолютно гладкое тело без боли и раздражений? Легко!
          </Body>
          <ImportantsCells cells={promocodeConfig.importantCells} />
          <HLine mt={28} mb={28} />
          <Body>Без раздражений, операционного вмешательства и больших затрат. Эффект после 1 процедуры.</Body>
          <HLine mt={28} mb={28} />
          <LargeCells cells={promocodeConfig.largeCells} />
          <HLine mt={28} mb={28} />
          <Benefits {...promocodeConfig.benefits} />
          <HLine mt={28} mb={28} />
          <WatchVideo videoId={promocodeConfig.videoId} />
          <HLine mt={28} mb={28} />
          <Benefits {...promocodeConfig.guarantee} />
          <HLine mt={28} mb={28} />
          <Body>Это безопасно 100%, БЕЗ рисков, побочных эффектов и периода реабилитации!</Body>
          <HLine mt={28} mb={28} />
          <FAQ />
        </ContentLayout>
      </ScrollView>
      <FixedContainer style={styles.shadow}>
        <Title>{currentServiceQuery.value?.price_max} ₽</Title>
        <ButtonContainer>
          <TextButton
            onPress={() => navigation.dispatch(StackActions.push(RoutesRoot.Contraindications, { promocode: true }))}
          >
            Записаться
          </TextButton>
        </ButtonContainer>
      </FixedContainer>
      <LoadingIndicator visible={currentServiceQuery.loading} />
    </LayoutWithBackButton>
  )
}

const ProcedureImage = styled.Image`
  width: 100%;
  height: 216px;
  border-radius: 32px;
`

const FixedContainer = styled(RowView)`
  height: 76px;
  width: 100%;
  padding: 16px;
  align-items: center;
  justify-content: space-between;
  position: absolute;
  background-color: ${color('white')};
  bottom: 0;
`

const ButtonContainer = styled(ColView)`
  flex: 1;
  margin-left: 48px;
`

const styles = StyleSheet.create({
  shadow: {
    shadowRadius: 2,
    shadowOffset: {
      width: 0,
      height: -3,
    },
    shadowColor: '#000000',
    elevation: 10,
  },
  scrollView: {
    marginBottom: 76,
  },
})
