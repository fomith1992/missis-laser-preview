import React, { useState } from 'react'
import { ScrollView, StyleSheet } from 'react-native'
import styled from '@emotion/native'
import { StackActions } from '@react-navigation/core'
import { StackScreenProps } from '@react-navigation/stack'

import MissisLaserImage from '../../assets/loyality-card-img.png'
import { RootStackParamList } from '@navigations/types'
import { Body, ColView, ContentLayout, Headiline, HLine, RowView, SafeAreaView, TextButton, Title } from '@shared/ui'
import { LayoutWithBackButton } from '@shared/ui/layouts/layout-with-back-button'

import { RoutesRoot } from 'src/enums/routes'
import { color } from 'src/style/mixins/color'

import { ImportantsCells } from './ui/promocode/important-cells'
import useAsync from 'react-use/lib/useAsync'
import { buyCardAPI } from '@shared/api'
import { useRootStore } from 'src/stores/lib/use-root-store'
import { LoadingIndicator } from '@shared/ui/atoms/loading-indicator'
import { PaymentWebView } from '@screens/shopping-cart/lib/payment.webview'
import { PercentIcon32 } from '@shared/ui/icons/percent.icon-32'
import { DefenceIcon32 } from '@shared/ui/icons/defence.icon-32'
import { ThumbUpIcon32 } from '@shared/ui/icons/thumb-up.icon-32'
import { LargeCells2Column } from './ui/promocode/large-cells-2-column'
import { Indication } from './ui'
import { CheckMarkCircleIcon32 } from '@shared/ui/icons/check-mark-circle.icon-32'
import { GuaranteeIcon32 } from '@shared/ui/icons/guarantee.icon-32'

type MillerCardScreenProps = StackScreenProps<RootStackParamList, RoutesRoot.MillerCardLanding>

export const MillerCardScreen = ({ navigation, route }: MillerCardScreenProps) => {
  const { type, itemId, itemPrice } = route.params
  const { name, phone, email, user_token } = useRootStore().user

  const [webViewVisible, setWebViewVisible] = useState<boolean>(false)

  const currentClinicQuery = useAsync(async () => {
    if (phone && user_token) {
      return await buyCardAPI.requestForLink({
        chargesItemId: itemId,
        email,
        name: name ?? phone,
        phone: phone,
      })
    } else return null
  })

  const indicationData = [
    'Заморозка цен в случае их роста',
    'Закрытые распродажи и акции ',
    'Действует во всех клиниках сети',
    `Срок действия ${type === 'millerCard3' ? 3 : 6} месяцев`,
  ]
  const indicationDataFooter = [
    'Сертифицированное\nоборудование и препараты ',
    'Эксперты сферы красоты',
    'Сервис высшего класса',
  ]
  if (webViewVisible && currentClinicQuery.value?.data.data.charge?.url)
    return (
      <SafeAreaView>
        <PaymentWebView
          onPaymentCancel={() => {
            setWebViewVisible(false)
          }}
          onPaymentComplete={() => {
            navigation.dispatch(StackActions.push(RoutesRoot.SuccessBuyingCard, {}))
          }}
          uri={currentClinicQuery.value?.data.data.charge?.url}
        />
      </SafeAreaView>
    )

  return (
    <>
      <LayoutWithBackButton onPress={() => navigation.goBack()}>
        <ScrollView style={styles.scrollView}>
          <ContentLayout flex mt={64} mb={24}>
            <ProcedureImage source={MissisLaserImage} />
            <Headiline mt={28}>Больше за те же деньги.{'\n'}Миллер карта.</Headiline>
            <Body mt={12} mb={20}>
              Карта на 3 и 6 месяцев дает возможность пользоваться специальным прайсом и получать дополнительные бонусы
            </Body>
            <ImportantsCells
              cells={[
                {
                  icon: PercentIcon32,
                  title: 'Экономия до 70%',
                  description: 'С картой вы экономите до 70%\nна стоимости услуг',
                },
                {
                  icon: DefenceIcon32,
                  title: 'Страховка цен в случае их роста',
                  description: 'За вами закрепляется прайс, актуальный\nна момент получения карты',
                },
                {
                  icon: ThumbUpIcon32,
                  title: 'Закрытые распродажи и акции',
                  description: 'Доступ к лучшим предложениям только\nдля участников клуба',
                },
              ]}
            />
            <HLine mt={28} mb={28} />
            <Body>Теперь вы можете позволить себе в 2 раза больше бьюти процедур за те же деньги.</Body>
            <HLine mt={28} />
            <LargeCells2Column
              cells={[
                {
                  title: 'Эпиляция глубокого бикини',
                  price: 1800,
                  price_with_card: 1000,
                },
                {
                  title: 'Эпиляция подмышек',
                  price: 1000,
                  price_with_card: 500,
                },
              ]}
            />
            <HLine mt={28} mb={28} />
            <Body>Откройте больше возможностей с Миллер картой чтобы чувствовать себя прекрасной каждый день!</Body>
            <HLine mt={28} mb={28} />
            <Indication items={indicationData} icon={CheckMarkCircleIcon32} />
            <HLine mt={28} mb={28} />
            <Body>Откройте больше возможностей с Миллер картой чтобы чувствовать себя прекрасной каждый день!</Body>
            <HLine mt={28} mb={28} />
            <Indication items={indicationDataFooter} icon={GuaranteeIcon32} />
          </ContentLayout>
        </ScrollView>
        <FixedContainer style={styles.shadow}>
          <Title>от {`${itemPrice} ₽`}</Title>
          <ButtonContainer>
            <TextButton onPress={() => setWebViewVisible(true)}>Купить</TextButton>
          </ButtonContainer>
        </FixedContainer>
      </LayoutWithBackButton>
      <LoadingIndicator visible={currentClinicQuery.loading} />
    </>
  )
}

const ProcedureImage = styled.Image`
  width: 100%;
  height: 216px;
  border-radius: 32px;
`

const FixedContainer = styled(RowView)`
  height: 76px;
  width: 100%;
  padding: 16px;
  align-items: center;
  justify-content: space-between;
  position: absolute;
  background-color: ${color('white')};
  bottom: 0;
`

const ButtonContainer = styled(ColView)`
  flex: 1;
  margin-left: 48px;
`

const styles = StyleSheet.create({
  shadow: {
    shadowRadius: 2,
    shadowOffset: {
      width: 0,
      height: -3,
    },
    shadowColor: '#000000',
    elevation: 10,
  },
  scrollView: {
    marginBottom: 76,
  },
})
