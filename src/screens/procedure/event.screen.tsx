import React from 'react'
import { FlatList, Image, ScrollView, StyleSheet } from 'react-native'
import { StackActions } from '@react-navigation/core'
import styled from '@emotion/native'

import { ContentLayout, Headiline, Body, RowView, ColView, Title, TextButton, BaseView } from '@shared/ui'
import { LayoutWithBackButton } from '@shared/ui/layouts/layout-with-back-button'
import { color } from 'src/style/mixins/color'
import { RoutesRoot } from 'src/enums/routes'

import { ImportantsCells, FAQ } from './ui'

import alexLaserImage from '../../assets/event-landing-title.png'
import laserTrue from '../../assets/laser-true.png'
import laserFalse from '../../assets/laser-false.png'
import laserLanding from '../../assets/landing.png'
import laser from '../../assets/laser.png'
import laser1 from '../../assets/laser1.png'
import laser3 from '../../assets/laser3.png'
import fingerUp from '../../assets/finger-up.png'
import legs from '../../assets/legs.png'
import laserDevice from '../../assets/laser-device.png'
import cloverTop from '../../assets/clover-top.png'
import cloverBottom from '../../assets/clover-bottom.png'
import certificat from '../../assets/certificat.png'

import example from '../../assets/example.png'
import example1 from '../../assets/example1.png'
import example2 from '../../assets/example2.png'
import example3 from '../../assets/example3.png'
import example4 from '../../assets/example4.png'

import feedback from '../../assets/feedback.png'
import feedback1 from '../../assets/feedback1.png'
import feedback2 from '../../assets/feedback2.png'
import feedback3 from '../../assets/feedback3.png'
import feedback4 from '../../assets/feedback4.png'
import feedback5 from '../../assets/feedback5.png'

import { StackScreenProps } from '@react-navigation/stack'
import { RootStackParamList } from '@navigations/types'
import { Caption, SubTitle } from '@shared/ui/atoms/text'
import { LargeCellsEvent } from './ui/promocode/large.cells'
import { ThumbUpIcon32 } from '@shared/ui/icons/thumb-up.icon-32'
import { HealthIcon32 } from '@shared/ui/icons/health.icon-32'
import { ForAllIcon32 } from '@shared/ui/icons/for-all.icon-32'
import { GuaranteeIcon32 } from '@shared/ui/icons/guarantee.icon-32'
import { useRootStore } from 'src/stores/lib/use-root-store'

const cellsMagicOne = [
  'Прошел все испытания',
  'Безболезненные процедуры',
  'Новейшая система охлаждения',
  'Безопасность подтверждена',
  'Высокая скорость процедуры',
  'Отсутствие запаха',
]
const cellsResults = [
  'Кожа стала мягкой и бархатистой',
  'Выпавший волос больше не вырастет',
  'Больше не нужно бриться',
  'Больше никакой щетины',
  'Больше нет вросших волос',
  'Не надо делать шугаринг',
]
const laserTrueData = [
  '• Кожа становится мягкой и бархатистой',
  '• Безболезненно и безопасно',
  '• Волосы выпадают навсегда',
  '• Оставшиеся волосы светлеют и становятся  мягче',
]
const laserFalseData = [
  '• Эффект очень временный',
  '• Процедура болезненная',
  '• Постояно вросшие волосы',
  '• Кожа остается грубой и травмируется',
]

type EventScreenProps = StackScreenProps<RootStackParamList, RoutesRoot.EventScreen>

export const EventScreen = ({ navigation, route }: EventScreenProps) => {
  const { setCurrentStaff } = useRootStore().appointmentReport
  const { itemId, minPrice } = route?.params ?? {}

  const submitEvent = () => {
    setCurrentStaff(0)
    navigation.dispatch(
      StackActions.push(RoutesRoot.DateTimeSelect, {
        selectedServices: [itemId],
        workerId: 0,
      }),
    )
  }

  return (
    <LayoutWithBackButton onPress={() => navigation.goBack()}>
      <ScrollView style={styles.scrollView}>
        <ContentLayout flex mt={64} mb={32}>
          <ProcedureImage source={alexLaserImage} />
          <Headiline mt={28}>Лазерная эпиляция всего тела за {minPrice} ₽</Headiline>
          <Body mt={12} mb={20}>
            Проводим эффективные процедуры лазерной эпиляции с помощью аппаратов MagicOne
          </Body>
          <ColView>
            <Headiline mb={28} mt={28}>
              Почему мы сделали выбор в пользу лазерной эпиляции?
            </Headiline>
            <RowView style={{ justifyContent: 'space-around' }}>
              <ColView aiCenter style={{ width: '50%' }} mr={8}>
                <LaserTrueImage source={laserTrue} />
                <SubTitle color="primaryAdditional" center mt={12}>
                  Лазерная
                </SubTitle>
                <BaseView>
                  {laserTrueData.map((x, idx) => (
                    <Caption key={idx.toString()} style={{ textAlign: 'left' }}>
                      {x}
                    </Caption>
                  ))}
                </BaseView>
              </ColView>
              <ColView aiCenter style={{ width: '50%' }}>
                <LaserTrueImage source={laserFalse} />
                <SubTitle center mt={12}>
                  Восковая
                </SubTitle>
                <BaseView>
                  {laserFalseData.map((x, idx) => (
                    <Caption key={idx.toString()} style={{ textAlign: 'left' }}>
                      {x}
                    </Caption>
                  ))}
                </BaseView>
              </ColView>
            </RowView>
          </ColView>
        </ContentLayout>
        <BaseView mt={12} mb={28}>
          <LaserLanding source={laserLanding} />
          <Laser source={laser} />
          <ContentLayout>
            <Title mt={28} mb={12}>
              Используем медицинский зарегистрированный диодный лазер Magic One
            </Title>
            {cellsMagicOne.map((text, idx) => (
              <RowView aiCenter key={idx} mb={8}>
                <FingerUp source={fingerUp} />
                <ColView ml={12}>
                  <Body>{text}</Body>
                </ColView>
              </RowView>
            ))}
          </ContentLayout>
        </BaseView>
        <LargeCellsEvent
          cells={[
            {
              img: legs,
              title: 'Максимум',
              description: 'Мастер выставит максимальную\nмощность в зависимости от типа кожи',
            },
            {
              img: laserDevice,
              title: 'Эффективность',
              description: 'Количество вспышек не ограничено, мы\nнацелены на эффективность',
            },
          ]}
        />
        <ContentLayout flex mt={28} mb={56}>
          <Image source={cloverTop} />
          <BorderContainer style={styles.shadow}>
            <Body mt={16} mb={16} ml={16} mr={16}>
              Magic One - самый мощный и безопасный лазер на рынке эпиляции, за счёт чего достигается максимальный
              эффект.
            </Body>
          </BorderContainer>
          <Image source={cloverBottom} />
        </ContentLayout>
        <ContentLayout>
          <Title mb={28}>Официально зарегистрированы как клиника</Title>
          <ImportantsCells
            cells={[
              {
                icon: HealthSvg,
                title: 'Медицинское образование',
                description:
                  'У нас работают врачи-дерматологи и\nкосметологи с соответствующим\nмедицинским образованием',
              },
              {
                icon: ThumbUpSvg,
                title: 'Соблюдены все СанПин',
                description:
                  'В каждой клинике, как в медицинском\nучреждении соблюдаются все\nсанитарные правила и нормы',
              },
              {
                icon: ForAllSvg,
                title: 'Зарегистрированное\nоборудование',
                description: 'Всё оборудование в клинике прошло\nтщательную проверку и испытания\nбезопасности',
              },
              {
                icon: GuaranteeSvg,
                title: 'Имеются все лицензии\nи сертификаты',
                description:
                  'Лицензии на медицинскую деятельность,\nна оборудование, сертификаты\nкомпетентности персонала',
              },
            ]}
          />
          <Title mt={56}>Наши лицензии и сертификаты</Title>
        </ContentLayout>
        <FlatList
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={styles.sliderContainer}
          data={[{ img: certificat }, { img: certificat }]}
          horizontal
          renderItem={(item) => (
            <SliderItemContainer>
              <Image source={item.item.img} />
            </SliderItemContainer>
          )}
          keyExtractor={(_, idx) => idx.toString()}
        />
        <ContentLayout flex mt={28} mb={56}>
          <Image style={styles.reverse} source={cloverBottom} />
          <BorderContainer style={styles.shadow}>
            <Body mt={16} mb={16} ml={16} mr={16}>
              С нашим оборудованием проведение процедур полностью <Body bold>безопасно и безболезненно</Body>
            </Body>
          </BorderContainer>
          <Image style={styles.reverse} source={cloverTop} />
        </ContentLayout>
        <LargeCellsEvent
          cells={[
            {
              img: legs,
              title: 'Безопасно',
              description: 'Воздействие светом — не облучение\nпоэтому диодный лазер не вызывает\nонкологию',
            },
            {
              img: laserDevice,
              title: 'Безболезненно',
              description:
                'В аппаратах установлена новая система\nохлаждения, поэтому процедуры\nполностью безболезненны',
            },
          ]}
        />
        <BaseView aiCenter flex mt={56}>
          <Image source={laser1} />
        </BaseView>
        <ContentLayout flex mt={28}>
          <BorderContainer style={styles.shadow}>
            <Body mt={16} mb={16} ml={16} mr={16}>
              Обучаем персонал специально под наш класс аппаратов, так как ценим ваше здоровье. Все врачи выполняют свою
              работу по одной отточенной методике. Мы следим за этим для того, чтобы избежать случаев, когда каждый врач
              выполняет процедуру по-разному
            </Body>
          </BorderContainer>
          <Image source={cloverBottom} />
        </ContentLayout>
        <BaseView aiCenter flex mt={56}>
          <Image source={laser3} />
        </BaseView>
        <ContentLayout mt={28}>
          <Title mb={12}>Результаты прохождения курса процедур</Title>
          {cellsResults.map((text, idx) => (
            <RowView aiCenter key={idx} mb={8}>
              <FingerUp source={fingerUp} />
              <ColView ml={12}>
                <Body>{text}</Body>
              </ColView>
            </RowView>
          ))}
          <Title mt={48}>Отзывы</Title>
        </ContentLayout>
        <FlatList
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={styles.sliderContainer}
          data={[
            { img: feedback },
            { img: feedback1 },
            { img: feedback2 },
            { img: feedback3 },
            { img: feedback4 },
            { img: feedback5 },
          ]}
          horizontal
          renderItem={(item) => (
            <SliderItemContainer>
              <Image style={{ width: 240, height: 138 }} source={item.item.img} />
            </SliderItemContainer>
          )}
          keyExtractor={(_, idx) => idx.toString()}
        />
        <ContentLayout mt={56}>
          <Title>До и после</Title>
        </ContentLayout>
        <FlatList
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={styles.sliderContainer}
          data={[{ img: example }, { img: example1 }, { img: example2 }, { img: example3 }, { img: example4 }]}
          horizontal
          renderItem={(item) => (
            <SliderItemContainer>
              <Image style={{ width: 238, height: 138 }} source={item.item.img} />
            </SliderItemContainer>
          )}
          keyExtractor={(_, idx) => idx.toString()}
        />
        <ContentLayout mt={56}>
          <FAQ />
        </ContentLayout>
      </ScrollView>
      <FixedContainer style={styles.shadow}>
        <Title>от {minPrice} ₽</Title>
        <ButtonContainer>
          <TextButton onPress={submitEvent}>Записаться</TextButton>
        </ButtonContainer>
      </FixedContainer>
    </LayoutWithBackButton>
  )
}

const ProcedureImage = styled.Image`
  width: 100%;
  height: 330px;
`

const LaserTrueImage = styled.Image`
  width: 96px;
  height: 96px;
`

const LaserLanding = styled.Image`
  position: absolute;
  width: 100%;
  height: 670px;
`

const Laser = styled.Image`
  margin-top: 90px;
  margin-left: auto;
  margin-right: auto;
  width: 234px;
  height: 234px;
`
const FingerUp = styled.Image`
  width: 32px;
  height: 32px;
`

const FixedContainer = styled(RowView)`
  height: 76px;
  width: 100%;
  padding: 16px;
  align-items: center;
  justify-content: space-between;
  position: absolute;
  background-color: ${color('white')};
  bottom: 0;
`

const ButtonContainer = styled(ColView)`
  flex: 1;
  margin-left: 48px;
`
//! del
const SliderItemContainer = styled(BaseView)`
  width: 240px;
  height: 180px;
  background-color: #fff;
  margin-right: 8px;
  border-radius: 8px;
  border-width: 1px;
  border-color: ${color('inactiveLight')};
  justify-content: center;
  align-items: center;
`

const BorderContainer = styled(BaseView)`
  flex: 1;
  border-radius: 16px;
  background-color: #fff;
`

const HealthSvg = styled(HealthIcon32)`
  color: ${color('errorLight')};
`

const ThumbUpSvg = styled(ThumbUpIcon32)`
  color: ${color('errorLight')};
`

const ForAllSvg = styled(ForAllIcon32)`
  color: ${color('errorLight')};
`

const GuaranteeSvg = styled(GuaranteeIcon32)`
  color: ${color('errorLight')};
`

const styles = StyleSheet.create({
  container: {
    paddingLeft: 10,
    paddingRight: 10,
  },
  sliderContainer: {
    paddingLeft: 16,
    paddingVertical: 16,
  },
  shadow: {
    shadowRadius: 2,
    shadowOffset: {
      width: 0,
      height: -3,
    },
    shadowColor: '#000000',
    elevation: 10,
  },
  scrollView: {
    marginBottom: 76,
  },
  reverse: {
    transform: [{ rotate: '180deg' }],
  },
})
