import { Body, ColView, RowView, Title } from '@shared/ui'
import React, { ElementType } from 'react'
import { StyleSheet } from 'react-native'
import { theme } from 'src/style/theme'

type IndicationProps = {
  icon: ElementType
  items: string[]
}

const styles = StyleSheet.create({
  icon: {
    color: theme.colors.primaryAdditional,
    width: 32,
    height: 32,
  },
})

export const Indication = ({ items, icon: Icon }: IndicationProps) => {
  return (
    <ColView>
      <Title>Показания</Title>
      <ColView mt={16}>
        {items.map((text, idx) => (
          <RowView aiCenter key={idx} mb={items.length - 1 === idx ? 0 : 8}>
            <Icon style={styles.icon} />
            <Body ml={12}>{text}</Body>
          </RowView>
        ))}
      </ColView>
    </ColView>
  )
}
