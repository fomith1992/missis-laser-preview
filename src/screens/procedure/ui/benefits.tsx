import React, { ComponentType } from 'react'

import { Body, ColView, RowView, Title } from '@shared/ui'
import { DoneIcon32 } from '@shared/ui/icons/done.icon-32'
import { IconComponentProps } from '@shared/ui/icons/icon-component'

import { Footnote } from '../../../shared/ui/atoms/text'

const mockData = ['Гладкая и приятная кожа', 'Ровный тон кожи', 'Экономию времени и денег', 'Уверенность в себе']

export interface BenefitsProps {
  title?: string
  data?: string[]
  icon?: ComponentType<IconComponentProps>
  iconColor?: string
  description?: string
}

type ItemProps = {
  text: string
  isLast: boolean
  icon: ComponentType<IconComponentProps>
  iconColor?: string
}

const BenefitItem = ({ text, isLast, icon: Icon, iconColor }: ItemProps) => {
  return (
    <RowView aiCenter mb={isLast ? 0 : 8}>
      <Icon style={{ color: iconColor, width: 32, height: 32 }} />
      <Body ml={12}>{text}</Body>
    </RowView>
  )
}

export const Benefits = ({
  data = mockData,
  title = 'Что дает процедура?',
  description,
  icon = DoneIcon32,
  iconColor,
}: BenefitsProps) => {
  return (
    <ColView>
      <Title>{title}</Title>
      <ColView mt={16}>
        {data.map((v, idx) => (
          <BenefitItem key={idx} text={v} isLast={data.length - 1 === idx} iconColor={iconColor} icon={icon} />
        ))}
      </ColView>
      {description && <Footnote mt={12}>{description}</Footnote>}
    </ColView>
  )
}
