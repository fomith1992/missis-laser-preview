import React, { ElementType, Fragment } from 'react'
import { Body, ColView, Footnote, RowView } from '@shared/ui'
import { StyleSheet } from 'react-native'

export type TCell = {
  icon: ElementType
  title: string
  description: string
}

type TImportantsCells = {
  cells: TCell[]
}

const styles = StyleSheet.create({
  icon: {
    width: 32,
    height: 32,
  },
})

export const ImportantsCells = ({ cells }: TImportantsCells) => {
  return (
    <Fragment>
      {cells.map(({ icon: Icon, title, description }, idx) => (
        <RowView key={idx} mb={cells.length - 1 === idx ? 0 : 16}>
          <Icon style={styles.icon} />
          <ColView ml={12}>
            <Body>{title}</Body>
            <Footnote mt={4}>{description}</Footnote>
          </ColView>
        </RowView>
      ))}
    </Fragment>
  )
}
