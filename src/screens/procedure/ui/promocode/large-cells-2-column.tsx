import React from 'react'
import { Body, ColView, Footnote, Headiline, RowView } from '@shared/ui'
import { CloseIcon32 } from '@shared/ui/icons/close.icon-32'
import { CheckMarkIcon32 } from '@shared/ui/icons/check-mark.icon-32'
import styled from '@emotion/native'
import { color } from 'src/style/mixins/color'

export type TCell = {
  title: string
  price: number
  price_with_card: number
}

type TImportantsCells = {
  cells: TCell[]
}

export const LargeCells2Column = ({ cells }: TImportantsCells) => {
  return (
    <>
      {cells.map(({ title, price, price_with_card }, idx) => (
        <ColView key={idx}>
          <Headiline mb={28} mt={28}>
            {title}
          </Headiline>
          <RowView style={{ justifyContent: 'space-around' }} key={idx}>
            <ColView aiCenter ml={20} mr={20}>
              <CloseIcon32 />
              <Body center mt={12}>
                Обычная цена
              </Body>
              <Footnote center mt={4}>
                {price}
              </Footnote>
            </ColView>
            <ColView aiCenter ml={20} mr={20}>
              <ActiveIcon />
              <Body color="primaryAdditional" center mt={12}>
                Цена с картой
              </Body>
              <Footnote color="primaryAdditional" center mt={4}>
                {price_with_card}
              </Footnote>
            </ColView>
          </RowView>
        </ColView>
      ))}
    </>
  )
}

const ActiveIcon = styled(CheckMarkIcon32)`
  color: ${color('primaryAdditional')};
`
