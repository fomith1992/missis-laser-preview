import React from 'react'
import WebView from 'react-native-webview'

import { Title } from '@shared/ui'

type WatchVideoProps = {
  videoId: string
}

export const WatchVideo = ({ videoId }: WatchVideoProps) => {
  return (
    <>
      <Title mb={16}>Посмотрите и убедитесь сами!</Title>
      <WebView
        style={{ height: 315 }}
        source={{ uri: `https://www.youtube.com/embed/${videoId}` }}
        allowsFullscreenVideo={true}
      />
    </>
  )
}
