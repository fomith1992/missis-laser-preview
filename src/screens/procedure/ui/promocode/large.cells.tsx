import React, { ElementType, Fragment } from 'react'
import { ImageSourcePropType, StyleSheet } from 'react-native'

import { Body, ColView, Footnote } from '@shared/ui'

import { theme } from 'src/style/theme'
import styled from '@emotion/native'

export type TCell = {
  icon: ElementType
  title: string
  description: string
}
export type TCellEvent = {
  img: ImageSourcePropType
  title: string
  description: string
}

type TImportantsCells = {
  cells: TCell[]
}
type TImportantsCellsEvent = {
  cells: TCellEvent[]
}

const styles = StyleSheet.create({
  icon: {
    color: theme.colors.primary,
    width: 64,
    height: 64,
  },
})

export const LargeCells = ({ cells }: TImportantsCells) => {
  return (
    <Fragment>
      {cells.map(({ title, description, icon: Icon }, idx) => (
        <ColView aiCenter key={idx} mb={cells.length - 1 === idx ? 0 : 16}>
          <Icon style={styles.icon} />
          <ColView aiCenter ml={20} mr={20}>
            <Body center mt={12}>
              {title}
            </Body>
            <Footnote center mt={4}>
              {description}
            </Footnote>
          </ColView>
        </ColView>
      ))}
    </Fragment>
  )
}

export const LargeCellsEvent = ({ cells }: TImportantsCellsEvent) => {
  return (
    <Fragment>
      {cells.map(({ title, description, img }, idx) => (
        <ColView aiCenter key={idx} mb={cells.length - 1 === idx ? 0 : 16}>
          <LargeCellsEventIcon source={img} />
          <ColView aiCenter ml={20} mr={20}>
            <Body center mt={12}>
              {title}
            </Body>
            <Footnote center mt={4}>
              {description}
            </Footnote>
          </ColView>
        </ColView>
      ))}
    </Fragment>
  )
}

const LargeCellsEventIcon = styled.Image`
  width: 64px;
  height: 64px;
`
