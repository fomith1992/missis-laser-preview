import React, { Fragment } from 'react'
import { ImageSourcePropType, ScrollView } from 'react-native'

import styled from '@emotion/native'

import { Body, ColView, RowView, Title } from '@shared/ui'

import image from './assets/1.png'

type TTrust = {
  name: string
  photo: ImageSourcePropType
}

const data: TTrust[] = [
  { name: 'Ксения Бородина', photo: image },
  { name: 'Эвелина Бледанс', photo: image },
  { name: 'Ксения Бородина', photo: image },
]

export const Trust = () => {
  return (
    <Fragment>
      <Title>Нам доверяют звезды</Title>
      <RowView mt={16}>
        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
          {data.map((v, idx) => (
            <ColView key={idx} mr={data.length - 1 === idx ? 0 : 12}>
              <Image source={v.photo} />
              <Body mt={12}>{v.name}</Body>
            </ColView>
          ))}
        </ScrollView>
      </RowView>
    </Fragment>
  )
}

const Image = styled.Image`
  width: 240px;
  height: 180px;
`
