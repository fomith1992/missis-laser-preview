import React, { Fragment } from 'react'
import { ImageSourcePropType, ScrollView } from 'react-native'

import styled from '@emotion/native'

import { ColView, RowView, Title } from '@shared/ui'

import image from './assets/2.png'

const data: ImageSourcePropType[] = [image, image, image]

export const Reviews = () => {
  return (
    <Fragment>
      <Title>Отзывы наших клиентов</Title>
      <RowView mt={16}>
        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
          {data.map((v, idx) => (
            <ColView key={idx} mr={data.length - 1 === idx ? 0 : 12}>
              <Image source={v} />
            </ColView>
          ))}
        </ScrollView>
      </RowView>
    </Fragment>
  )
}

const Image = styled.Image`
  width: 240px;
  height: 180px;
`
