import React, { Fragment, useState } from 'react'
import Accordion from 'react-native-collapsible/Accordion'

import styled from '@emotion/native'

import { BaseView, Body, ColView, RowView, Title } from '@shared/ui'
import { ShevronDownIcon24 } from '@shared/ui/icons/shevron-down.icon-24'

import faqData from '../faq.json'

type TItem = {
  title: string
  content: {
    type: string
    data: string | string[]
  }
}

const renderHeader = ({ title }: TItem, _index: number, isActive: boolean) => {
  return (
    <HeaderView mt={8} mb={8}>
      <HeaderTextContainer>
        <Body>{title}</Body>
      </HeaderTextContainer>
      {isActive ? (
        <ShevronOrientation>
          <ShevronDownIcon24 />
        </ShevronOrientation>
      ) : (
        <ShevronDownIcon24 />
      )}
    </HeaderView>
  )
}

const renderContent = ({ content }: TItem) => {
  if (content.type === 'dot-list' && Array.isArray(content.data)) {
    return (
      <ColView>
        {content.data.map((text, idx) => (
          <DotListItemContainer key={idx} mb={8}>
            <Dot />
            <Footnote>{text}</Footnote>
          </DotListItemContainer>
        ))}
      </ColView>
    )
  }

  if (content.type === 'text-list' && Array.isArray(content.data)) {
    return (
      <ColView mb={8}>
        {content.data.map((text, idx) => (
          <Footnote key={idx} mt={idx ? 16 : 0}>
            {text}
          </Footnote>
        ))}
      </ColView>
    )
  }

  // if content.type === "text"
  return <Footnote mb={8}>{content.data}</Footnote>
}

export const FAQ = () => {
  const [active, setActive] = useState<number[]>([])

  const changeHanlder = (indexes: number[]) => {
    setActive(indexes)
  }

  return (
    <Fragment>
      <Title mb={16}>Вопросы и ответы</Title>
      <Accordion
        activeSections={active}
        sections={faqData}
        renderSectionTitle={() => <Fragment />}
        renderHeader={renderHeader}
        renderContent={renderContent}
        onChange={changeHanlder}
        underlayColor={'white'}
      />
    </Fragment>
  )
}

const HeaderView = styled(BaseView)`
  height: 36px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`

const Footnote = styled(Body)`
  font-size: 14px;
`

const ShevronOrientation = styled.View`
  transform: rotate(180deg);
`

const HeaderTextContainer = styled.View`
  max-width: 90%;
`

const DotListItemContainer = styled(RowView)`
  position: relative;
  padding-left: 16px;
`
const Dot = styled.View`
  position: absolute;
  width: 4px;
  height: 4px;
  top: 8px;
  border-radius: 2px;
  background-color: black;
`
