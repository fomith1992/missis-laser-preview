import React from 'react'
import { ScrollView, StyleSheet } from 'react-native'
import { StackActions } from '@react-navigation/core'
import styled from '@emotion/native'

import { ContentLayout, Headiline, Body, HLine, RowView, ColView, Title, TextButton } from '@shared/ui'
import { LayoutWithBackButton } from '@shared/ui/layouts/layout-with-back-button'
import { color } from 'src/style/mixins/color'
import { RoutesRoot } from 'src/enums/routes'

import { TCell, ImportantsCells, Benefits, Indication, FAQ } from './ui'

import alexLaserImage from '@entities/procedure/ui/alex-laser.large.png'

import { ThumbUpIcon24 } from '@shared/ui/icons/thumb-up.icon-24'
import { SaleIcon24 } from '@shared/ui/icons/sale.icon-24'
import { DefenceIcon24 } from '@shared/ui/icons/defence.icon-24'
import { StackScreenProps } from '@react-navigation/stack'
import { RootStackParamList } from '@navigations/types'
import { CheckMarkCircleIcon32 } from '@shared/ui/icons/check-mark-circle.icon-32'

const cells: TCell[] = [
  {
    icon: DefenceIcon24,
    title: 'Защита',
    description: 'С защитой эпидермиса благодаря охлаждению хладагентом или воздухом',
  },
  {
    icon: SaleIcon24,
    title: 'Эффективность',
    description: 'Для идеального результата нужно 5-6 процедур',
  },
  {
    icon: ThumbUpIcon24,
    title: 'Без реабилитации',
    description: 'Покраснения уходят быстро, не требуя никакого периода реабилитации',
  },
]

const indicationData = ['Фототип кожи 1-3', 'Лечение гиперпигментных пятен']

type AlexLaserProcedureScreenProps = StackScreenProps<RootStackParamList, RoutesRoot.AlexLaserProcedure>

export const AlexLaserProcedureScreen = ({ navigation, route }: AlexLaserProcedureScreenProps) => {
  const params = route?.params ?? {}
  const { minPrice } = params

  return (
    <LayoutWithBackButton onPress={() => navigation.goBack()}>
      <ScrollView style={styles.scrollView}>
        <ContentLayout flex mt={64} mb={32}>
          <ProcedureImage source={alexLaserImage} />
          <Headiline mt={28}>Александритовый лазер</Headiline>
          <Body mt={12} mb={20}>
            Для моментального эффекта. Для темных волос на светлой коже.
          </Body>
          <ImportantsCells cells={cells} />
          <HLine mt={28} mb={28} />
          <Benefits />
          <HLine mt={28} mb={28} />
          <Body>В любое время дня и ночи постоянно приятная на ощупь кожа, без покраснений и черных точек</Body>
          <HLine mt={28} mb={28} />
          <Indication items={indicationData} icon={CheckMarkCircleIcon32} />
          <HLine mt={28} mb={28} />
          <FAQ />
        </ContentLayout>
      </ScrollView>
      <FixedContainer style={styles.shadow}>
        <Title>от {minPrice} ₽</Title>
        <ButtonContainer>
          <TextButton
            onPress={() => navigation.dispatch(StackActions.push(RoutesRoot.Contraindications, { promocode: false }))}
          >
            Записаться
          </TextButton>
        </ButtonContainer>
      </FixedContainer>
    </LayoutWithBackButton>
  )
}

const ProcedureImage = styled.Image`
  width: 100%;
  height: 216px;
  border-radius: 32px;
`

const FixedContainer = styled(RowView)`
  height: 76px;
  width: 100%;
  padding: 16px;
  align-items: center;
  justify-content: space-between;
  position: absolute;
  background-color: ${color('white')};
  bottom: 0;
`

const ButtonContainer = styled(ColView)`
  flex: 1;
  margin-left: 48px;
`

const styles = StyleSheet.create({
  shadow: {
    shadowRadius: 2,
    shadowOffset: {
      width: 0,
      height: -3,
    },
    shadowColor: '#000000',
    elevation: 10,
  },
  scrollView: {
    marginBottom: 76,
  },
})
