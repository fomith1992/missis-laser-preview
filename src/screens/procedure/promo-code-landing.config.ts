import { GuaranteeIcon24 } from '@shared/ui/icons/guarantee.icon-24'
import { HappyIcon24 } from '@shared/ui/icons/happy.icon-24'
import { HealthIcon64 } from '@shared/ui/icons/health.icon-64'
import { LongIcon24 } from '@shared/ui/icons/long.icon-24'
import { ThumbUpIcon24 } from '@shared/ui/icons/thumb-up.icon-24'
import { TimeIcon64 } from '@shared/ui/icons/time.icon-64'

import { theme } from 'src/style/theme'

import { AccuracyIcon24 } from '../../shared/ui/icons/accuracy.icon-24'
import { SpeedIcon24 } from '../../shared/ui/icons/speed.icon-24'
import { TCell } from './ui'
import { BenefitsProps } from './ui/benefits'

type config = {
  importantCells: TCell[]
  largeCells: TCell[]
  benefits: BenefitsProps
  guarantee: BenefitsProps
  videoId: string
}

export const promocodeConfig: config = {
  importantCells: [
    {
      icon: HappyIcon24,
      title: 'Отсутствие боли',
      description: 'При проведении процедуры нашими специалистами и современным оборудованием боль не ощущается',
    },
    {
      icon: LongIcon24,
      title: 'Долгоиграющий результат',
      description:
        'При правильно составленном курсе и качественном выполнении процедур наши специалистами, волосы не растут годами',
    },
    {
      icon: ThumbUpIcon24,
      title: 'Без реабилитации',
      description: 'Покраснения уходят быстро, не требуя никакого периода реабилитации',
    },
  ],
  largeCells: [
    {
      icon: SpeedIcon24,
      title: 'Скорость',
      description: 'По сравнению с другими способами удаления уходит на 30-60 мин. меньше',
    },
    {
      icon: TimeIcon64,
      title: 'Долговечность',
      description: 'Полное отсутствие волос на обработанных участках до 30 дней',
    },
    {
      icon: AccuracyIcon24,
      title: 'Удаление везде',
      description: 'Даже в труднодоступных участках (за исключением области ушей и глаз)',
    },
    {
      icon: HealthIcon64,
      title: 'Безопасность и стерильность',
      description: 'Процедура полность безопасна и стерильна',
    },
  ],
  benefits: {
    title: 'Клинически подтвержденная эффективность',
    data: ['Уменьшение роста волос до 3-х лет', 'Гладкая и упругая кожа', 'Без побочных эффектов'],
    iconColor: theme.colors.primary,
    description:
      ' У пациентов, прошедших четыре 30-минутные процедуры эпиляции александритовым лазером. Согласно результатам международным клинических исследований.',
  },
  guarantee: {
    icon: GuaranteeIcon24,
    title: 'Что мы гарантируем?',
    data: [
      'Удаление до 80% волос после 1 процедуры',
      'Ощутимый эффект гладкости кожи',
      'Безболезненность процедуры',
      'Удаление в малодоступных местах',
      'Скорость и комфортность',
    ],
    iconColor: theme.colors.primary,
  },
  videoId: 'X_90kvqfZD4',
}
