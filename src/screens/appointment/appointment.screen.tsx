import React, { useEffect, useRef } from 'react'
import useAsyncFn from 'react-use/lib/useAsyncFn'

import { AxiosError } from 'axios'

import BottomSheet from '@gorhom/bottom-sheet'
import { CommonActions, StackActions } from '@react-navigation/core'
import { StackScreenProps } from '@react-navigation/stack'

import { BottomSheetGorhom } from '@features/bottom-sheet-gorhom/bottom-sheet'
import { RoutesAppointments } from '@navigations/stack-appointments/routes'
import { AppointmentStackParamList } from '@navigations/stack-appointments/types'
import { recordsAPI } from '@shared/api'
import { formatISOdate } from '@shared/lib/date'
import { LoadingIndicator } from '@shared/ui/atoms/loading-indicator'

import { RoutesRoot } from 'src/enums/routes'
import { useRootStore } from 'src/stores/lib/use-root-store'

import { afterInfoData, beforeInfoData } from './mock/data'
import { AppointmentView } from './ui/appointment.view'
import { ChooseActionBottomSheetView } from './ui/sheet/choose.action.bottom-sheet.view'
import { ConfirmDeleteBottomSheetView } from './ui/sheet/delete-confirm.bottom-sheet.view'

import { isFuture, parseISO } from 'date-fns'

type AppoinmentScreenProps = StackScreenProps<AppointmentStackParamList, RoutesAppointments.Appointment>

export const AppoinmentScreen = ({ navigation, route: { params } }: AppoinmentScreenProps) => {
  const { appointmentId, companyId, appointment, showBottomSheet } = params
  const { user_token } = useRootStore().user

  const sheetRefChooseAction = useRef<BottomSheet>(null)
  const sheetRefDeleteConfirm = useRef<BottomSheet>(null)

  const deleteError = useRef(false)

  const [queryState, fetchAppointment] = useAsyncFn(async () => {
    const recordId = appointmentId || appointment?.id
    if (!user_token || !recordId) return
    return await recordsAPI
      .getRecordById({ companyId, recordId, user_token })
      .then(({ data: { data } }) => data)
      .catch((e) => console.log(e))
  }, [appointmentId])

  const [deleteQueryState, deleteRecordAsync] = useAsyncFn(async () => {
    const record = appointment || queryState.value
    if (!user_token || !record) return
    return await recordsAPI
      .deleteRecordById({ recordId: appointmentId || record.id, user_token })
      .then(() => (deleteError.current = false))
      .catch((error: AxiosError) => {
        console.log(error.response)
        deleteError.current = true
      })
  }, [appointmentId])

  useEffect(() => {
    if (!appointment) fetchAppointment()
  }, [appointment, fetchAppointment])

  if (queryState.loading) {
    return <LoadingIndicator visible />
  }

  const record = appointment || queryState.value

  if (!record) return null

  const cartData = record.services.map(({ cost, title }) => ({ name: title, price: cost }))
  const date = parseISO(record.datetime)
  const isFutureDate = isFuture(date)

  const handlerPrimaryPress = async () => {
    sheetRefDeleteConfirm.current?.close()
    sheetRefChooseAction.current?.expand()
  }

  const chooseDeleteAction = async () => {
    sheetRefChooseAction.current?.close()
    sheetRefDeleteConfirm.current?.expand()
  }

  const closeAllSheets = () => {
    sheetRefChooseAction.current?.close()
    sheetRefDeleteConfirm.current?.close()
  }

  const handlerRecordMove = () => {
    sheetRefChooseAction.current?.close()
    navigation.dispatch(
      StackActions.push(RoutesRoot.DateTimeSelect, {
        isMoveRecord: true,
        appointmentId: record.id,
        companyId: record.company.id,
        selectedServices: record.services.map(({ id }) => id),
        workerId: record.staff.id,
        excludeRecordId: record.id,
      }),
    )
  }

  const handlerConfirmDelete = () => {
    deleteRecordAsync().then(() => {
      if (deleteError.current) {
        //ошибка при удалении
      } else {
        navigation.dispatch(
          CommonActions.reset({
            index: 1,
            routes: [{ name: RoutesRoot.BottomNavigator }, { name: RoutesRoot.SuccessAppointmentDelete }],
          }),
        )
      }
    })
  }

  return (
    <>
      <AppointmentView
        showBottomSheet={showBottomSheet}
        procedureType="Диодный лазер"
        cart={cartData}
        date={formatISOdate(record.datetime)}
        address={record.company.address || record.company.city}
        actionSheetTitle={isFutureDate ? 'Как подготовиться?' : 'После процедуры'}
        worker={{
          ...record.staff,
          image: record.staff.avatar,
          bookable: true,
        }}
        sheetData={isFutureDate ? beforeInfoData : afterInfoData}
        hideButton={!isFutureDate}
        onPrimaryPress={handlerPrimaryPress}
      />
      <BottomSheetGorhom sheetRef={sheetRefChooseAction}>
        <ChooseActionBottomSheetView onDelete={chooseDeleteAction} onMove={handlerRecordMove} />
      </BottomSheetGorhom>
      <BottomSheetGorhom sheetRef={sheetRefDeleteConfirm}>
        <ConfirmDeleteBottomSheetView onCancel={closeAllSheets} onConfirmDelete={handlerConfirmDelete} />
      </BottomSheetGorhom>
      <LoadingIndicator visible={deleteQueryState.loading} />
    </>
  )
}
