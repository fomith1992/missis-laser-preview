import React from 'react'
import { SafeAreaView } from '@shared/ui'
import { ProccessStateView } from '@features/proccess-state/proccess-state.view'
import { StackActions, useNavigation } from '@react-navigation/core'
import { RoutesRoot } from 'src/enums/routes'

export function SuccessAppoinmentDeleteScreen(): React.ReactElement {
  const nav = useNavigation()

  return (
    <SafeAreaView>
      <ProccessStateView
        title="Запись отменена"
        description={``}
        buttonText="Спасибо, понятно"
        onPress={() => nav.dispatch(StackActions.replace(RoutesRoot.BottomNavigator))}
      />
    </SafeAreaView>
  )
}
