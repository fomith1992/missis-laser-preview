import React from 'react'
import styled from '@emotion/native'

import { Title, RowView } from '@shared/ui'

import image from '@entities/procedure/ui/1.png'

type ProcedureTypeViewProps = {
  title: string
}

export const ProcedureTypeView = ({ title }: ProcedureTypeViewProps) => {
  return (
    <Container>
      <Image source={image} />
      <Title ml={12}>{title}</Title>
    </Container>
  )
}

const Container = styled(RowView)`
  align-items: center;
`

const Image = styled.Image`
  width: 72px;
  height: 72px;
  border-radius: 16px;
`
