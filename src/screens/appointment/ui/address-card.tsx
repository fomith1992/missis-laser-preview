import React, { useEffect, useState } from 'react'
import { StyleSheet, TouchableOpacity } from 'react-native'
import Clipboard from 'expo-clipboard'
import styled from '@emotion/native'

import Toast, { ToastProps } from 'react-native-root-toast'

import { Body, RowView } from '@shared/ui'
import { LocationIcon24 } from '@shared/ui/icons/location.icon-24'
import { ThumbUpIcon24 } from '@shared/ui/icons/thumb-up.icon-24'
import { color } from 'src/style/mixins/color'

type AddressCardProps = {
  address: string
}

const toastOptions: ToastProps = {
  duration: Toast.durations.SHORT,
  position: 0 - 24 - 44 - 12, // margin-top + text-height + margin-bottom
  backgroundColor: 'black',
  shadow: false,
  animation: true,
  hideOnPress: true,
  delay: 0,
}

let timerId: NodeJS.Timer

export const AddressCard = ({ address }: AddressCardProps) => {
  const [visible, setVisible] = useState(false)
  const [timeoutActive, setTimeoutActive] = useState(false)

  const reset = () => {
    setVisible(false)
    setTimeoutActive(false)
  }

  const onLocationPress = () => {
    Clipboard.setString(address)
    setVisible(true)
    setTimeoutActive(true)

    if (!timeoutActive) {
      timerId = setTimeout(reset, 5000)
    }
  }

  useEffect(() => {
    return () => {
      clearTimeout(timerId)
    }
  }, [])

  return (
    <>
      <TouchableOpacity onPress={onLocationPress}>
        <RowView aiCenter>
          <LocationIcon />
          <Body ml={12}>{address}</Body>
        </RowView>
      </TouchableOpacity>
      <Toast containerStyle={styles.toast} visible={visible} {...toastOptions} onHide={reset}>
        <RowView aiCenter>
          <ThumbUpIcon />
          <Body ml={12} color="lightText">
            Адрес успешно скопирован
          </Body>
        </RowView>
      </Toast>
    </>
  )
}

const LocationIcon = styled(LocationIcon24)`
  width: 20px;
  height: 20px;
  color: ${color('primary')};
`

const ThumbUpIcon = styled(ThumbUpIcon24)`
  width: 32px;
  height: 32px;
  color: ${color('primary')};
`
const styles = StyleSheet.create({
  toast: {
    padding: 12,
    alignItems: 'flex-start',
    justifyContent: 'center',
    alignSelf: 'center',
    width: 300,
  },
})
