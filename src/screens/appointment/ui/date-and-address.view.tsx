import React from 'react'

import { Body, ColView, RowView, Title } from '@shared/ui'
import { CalendarIcon24 } from '@shared/ui/icons/calendar.icon-24'
import { AddressCard } from './address-card'
import { StyleSheet } from 'react-native'

type DateAndAdressProps = {
  date: string
  address: string
}

export const DateAndAddressView = ({ date, address }: DateAndAdressProps) => {
  return (
    <ColView>
      <Title>Дата и адрес</Title>
      <RowView aiCenter mt={16}>
        <CalendarIcon24 style={styles.icon} />
        <Body ml={12}>{date}</Body>
      </RowView>
      <RowView mt={16}>
        <AddressCard address={address} />
      </RowView>
    </ColView>
  )
}

const styles = StyleSheet.create({
  icon: {
    width: 20,
    height: 20,
  },
})
