import React from 'react'
import { Body, ColView, RowView } from '@shared/ui'

type TOrder = {
  name: string
  price: number
}

type OrderListViewProps = {
  data: TOrder[]
}

export const OrderListView = ({ data }: OrderListViewProps) => {
  const total = data.reduce((acc, { price }) => acc + price, 0)

  return (
    <ColView mt={16}>
      {data.map((item, idx) => (
        <RowView key={idx} mb={16}>
          <Body flex mr="auto">
            {item.name}
          </Body>
          <Body ml={12}>{item.price} ₽</Body>
        </RowView>
      ))}
      <RowView>
        <Body mr="auto">Всего (оплата на месте)</Body>
        <Body bold ml={12}>
          {total} ₽
        </Body>
      </RowView>
    </ColView>
  )
}
