import React, { useEffect, useRef, useState } from 'react'
import { ScrollView, StyleSheet } from 'react-native'

import { BaseView, ColView, ContentLayout, HLine, RowView, TextButton, Title } from '@shared/ui'
import { ShevronDownIcon24 } from '@shared/ui/icons/shevron-down.icon-24'

import { WorkerCardView } from '@entities/worker'
import { IWorker } from '@entities/worker/model/types'

import { ProcedureTypeView } from './procedure-type.view'
import { OrderListView } from './order-list.view'
import { DateAndAddressView } from './date-and-address.view'
import { TouchableOpacity } from 'react-native-gesture-handler'

import { InfoBottomSheetView } from './sheet/info-bottom-sheet.view'
import { InfoItem } from '../mock/data'

import BottomSheet from '@gorhom/bottom-sheet'
import { BottomSheetGorhom } from '@features/bottom-sheet-gorhom/bottom-sheet'

type AppointmentViewProps = {
  procedureType: string
  cart: Array<{ name: string; price: number }>
  date: string
  address: string
  worker: IWorker
  actionSheetTitle: string
  hideButton: boolean
  sheetData: InfoItem[]
  onPrimaryPress: () => void
  showBottomSheet?: boolean
}

export const AppointmentView = ({
  procedureType,
  cart,
  date,
  address,
  worker,
  actionSheetTitle,
  sheetData,
  hideButton,
  onPrimaryPress,
  showBottomSheet,
}: AppointmentViewProps) => {
  const sheetRef = useRef<BottomSheet>(null)
  const [bottomSheetVisible, setBottomSheetVisible] = useState(showBottomSheet)

  useEffect(() => {
    if (bottomSheetVisible === true) {
      setTimeout(() => sheetRef.current?.expand(), 1000)
      setBottomSheetVisible(false)
    }
  }, [bottomSheetVisible])

  return (
    <>
      <ScrollView style={styles.flex} contentContainerStyle={styles.scrollContainer}>
        <ContentLayout flex>
          <ProcedureTypeView title={procedureType} />
          <OrderListView data={cart} />
          <HLine mt={24} mb={24} />
          <TouchableOpacity onPress={() => sheetRef.current?.expand()}>
            <RowView>
              <Title mr="auto">{actionSheetTitle}</Title>
              <ShevronDownIcon24 />
            </RowView>
          </TouchableOpacity>
          <HLine mt={24} mb={24} />
          <DateAndAddressView date={date} address={address} />
          <HLine mt={24} mb={24} />
          <Title mb={16}>Специалист</Title>
          <BaseView mb={16}>
            <WorkerCardView imageSize={44} {...worker} />
          </BaseView>
          {!hideButton && (
            <ColView mt="auto" mb={24}>
              <TextButton onPress={onPrimaryPress}>Перенести или отменить</TextButton>
            </ColView>
          )}
        </ContentLayout>
      </ScrollView>
      <BottomSheetGorhom sheetRef={sheetRef}>
        <InfoBottomSheetView sheetRef={sheetRef} title={actionSheetTitle} data={sheetData} />
      </BottomSheetGorhom>
    </>
  )
}

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  scrollContainer: {
    flexGrow: 1,
  },
})
