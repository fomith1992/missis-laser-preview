import { TouchableWithoutFeedback } from '@gorhom/bottom-sheet'
import { BaseView, ContentLayout, Headiline, RowView, TextButton } from '@shared/ui'
import React from 'react'
import { Platform } from 'react-native'

type ChooseActionBottomSheetProps = {
  onDelete: () => void
  onMove: () => void
}

export const ChooseActionBottomSheetView = ({ onDelete, onMove }: ChooseActionBottomSheetProps) => {
  const isIos = Platform.OS === 'ios'
  return (
    <ContentLayout>
      <Headiline center mt={4}>
        Что вы хотите сделать?
      </Headiline>
      <RowView mt={20} mb={24}>
        <BaseView flex mr={12}>
          <TouchableWithoutFeedback onPress={!isIos ? onDelete : undefined}>
            <TextButton onPress={isIos ? onDelete : undefined} type="error">
              Отменить
            </TextButton>
          </TouchableWithoutFeedback>
        </BaseView>
        <BaseView flex>
          <TouchableWithoutFeedback onPress={!isIos ? onMove : undefined}>
            <TextButton onPress={isIos ? onMove : undefined}>Перенести</TextButton>
          </TouchableWithoutFeedback>
        </BaseView>
      </RowView>
    </ContentLayout>
  )
}
