import React from 'react'
import styled from '@emotion/native'

import { Body, ColView, ContentLayout, Footnote, Headiline, RowView, TextButton } from '@shared/ui'
import { SaleIcon24 } from '@shared/ui/icons/sale.icon-24'
import { InfoIcon24 } from '@shared/ui/icons/info.icon-24'
import { color } from 'src/style/mixins/color'

import { InfoItem } from '../../mock/data'
import BottomSheet, { TouchableWithoutFeedback } from '@gorhom/bottom-sheet'
import { Platform } from 'react-native'

type InfoBottomSheetViewProps = {
  sheetRef: React.RefObject<BottomSheet>
  data: InfoItem[]
  title: string
}

export const InfoBottomSheetView = ({ data, title, sheetRef }: InfoBottomSheetViewProps) => {
  const isIos = Platform.OS === 'ios'
  return (
    <ContentLayout>
      <IconWrapper mt={20}>
        <InfoIcon />
      </IconWrapper>
      <Title mt={20}>{title}</Title>
      {data.map((item, idx) => (
        <RowView mt={20} key={idx}>
          <SaleIcon24 />
          <ColView ml={12}>
            <Body bold>{item.title}</Body>
            <Footnote mt={4}>{item.description}</Footnote>
          </ColView>
        </RowView>
      ))}
      <ColView mb={24} mt={24}>
        <TouchableWithoutFeedback onPress={() => (!isIos ? sheetRef.current?.snapTo(0) : undefined)}>
          <TextButton onPress={() => (isIos ? sheetRef.current?.snapTo(0) : undefined)}>Понятно</TextButton>
        </TouchableWithoutFeedback>
      </ColView>
    </ContentLayout>
  )
}

const IconWrapper = styled(ColView)`
  padding: 8px;
  align-self: center;
  border-radius: 48px;
  background-color: ${color('primaryLight')};
`

const InfoIcon = styled(InfoIcon24)`
  width: 80px;
  height: 80px;
  color: ${color('primary')};
`

const Title = styled(Headiline)`
  text-align: center;
`
