import { TouchableWithoutFeedback } from '@gorhom/bottom-sheet'
import { BaseView, Body, ContentLayout, Headiline, RowView, TextButton } from '@shared/ui'
import React from 'react'
import { Platform } from 'react-native'

type ConfirmDeleteBottomSheetProps = {
  onCancel: () => void
  onConfirmDelete: () => void
}

export const ConfirmDeleteBottomSheetView = ({ onCancel, onConfirmDelete }: ConfirmDeleteBottomSheetProps) => {
  const isIos = Platform.OS === 'ios'
  return (
    <ContentLayout>
      <Headiline center mt={4}>
        Отменить запись?
      </Headiline>
      <Body mt={12}>После этого записаться на это же время снова возможно не получится</Body>
      <RowView mt={20} mb={24}>
        <BaseView flex mr={12}>
          <TouchableWithoutFeedback onPress={!isIos ? onCancel : undefined}>
            <TextButton onPress={isIos ? onCancel : undefined} type="secondary">
              Не отменять
            </TextButton>
          </TouchableWithoutFeedback>
        </BaseView>
        <BaseView flex>
          <TouchableWithoutFeedback onPress={!isIos ? onConfirmDelete : undefined}>
            <TextButton onPress={isIos ? onConfirmDelete : undefined} type="error">
              Да
            </TextButton>
          </TouchableWithoutFeedback>
        </BaseView>
      </RowView>
    </ContentLayout>
  )
}
