import image1 from '@entities/service/ui/1.png'
import image2 from '@entities/service/ui/2.png'
import image3 from '@entities/service/ui/3.png'

export const appointmentsData = [
  {
    title: 'Предстоящие',
    data: [
      {
        title: 'Диодный лазер',
        date: '2021-05-24T15:13:08Z',
        image: image1,
      },
      {
        title: 'Чистка Hydrafacial',
        date: '2021-05-24T15:13:08Z',
        image: image2,
      },
    ],
  },
  {
    title: 'Апрель',
    data: [
      {
        title: 'Диодный лазер',
        date: '2021-05-24T15:13:08Z',
        image: image3,
      },
      {
        title: 'Диодный лазер',
        date: '2021-05-24T15:13:08Z',
        image: image1,
      },
    ],
  },
  {
    title: 'Сентябрь',
    data: [
      {
        title: 'Диодный лазер',
        date: '2021-05-24T15:13:08Z',
        image: image2,
      },
      {
        title: 'Диодный лазер',
        date: '2021-05-24T15:13:08Z',
        image: image2,
      },
    ],
  },
]
