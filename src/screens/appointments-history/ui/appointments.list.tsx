import React from 'react'
import styled from '@emotion/native'
import { SectionListRenderItem } from 'react-native'

import { ProccessStateView } from '@features/proccess-state/proccess-state.view'
import { BaseView, HLine, Title } from '@shared/ui'
import { CalendarIcon24 } from '@shared/ui/icons/calendar.icon-24'
import { SectionList, StyleSheet } from 'react-native'
import { color } from 'src/style/mixins/color'
import { TSectionList } from '../lib/appointments.container'
import { TRecord } from '@shared/api/records'

type AppointmentsContainerProps = {
  sections: TSectionList[]
  onRefresh: () => void
  isRefreshing: boolean
  renderItem?: SectionListRenderItem<TRecord, TSectionList>
}

export const AppointmentsList = ({ sections, onRefresh, isRefreshing, renderItem }: AppointmentsContainerProps) => {
  return (
    <SectionList
      onRefresh={onRefresh}
      refreshing={isRefreshing}
      contentContainerStyle={style.sectionContainer}
      sections={sections}
      stickySectionHeadersEnabled={false}
      ListEmptyComponent={renderEmptyList}
      ListFooterComponent={<BaseView mb={24} />}
      renderItem={renderItem}
      renderSectionHeader={({ section }) => {
        if (sections.indexOf(section) === 0) {
          return <Title>{section.title}</Title>
        }

        return (
          <>
            <HLine mt={28} mb={20} />
            <Title>{section.title}</Title>
          </>
        )
      }}
    />
  )
}

const renderEmptyList = () => (
  <ProccessStateView
    title="Пока нет записей"
    description="Выбрать процедуру для записи можно на главной странице"
    SvgComponent={<CalendarIconBig />}
    noButton
  />
)

const style = StyleSheet.create({
  sectionContainer: { paddingHorizontal: 16, flexGrow: 1 },
})

const CalendarIconBig = styled(CalendarIcon24)`
  width: 64px;
  height: 64px;
  color: ${color('primary')};
`
