import React from 'react'
import { ImageSourcePropType, TouchableOpacity } from 'react-native'
import { theme } from 'src/style/theme'
import { BaseView, Body, RowView } from '@shared/ui'
import styled from '@emotion/native'
import { CalendarIcon24 } from '@shared/ui/icons/calendar.icon-24'

import { RefreshIcon20 } from '@shared/ui/icons/refresh.icon-20'
import { color } from '../../../style/mixins/color'
import { SubTitle } from '@shared/ui/atoms/text'

import { formatISOdate } from '@shared/lib/date'
import { hitSlopParams } from '@shared/ui/buttons/hit-slop-params'

type AppointmentData = {
  title: string
  date: string
  image: ImageSourcePropType
}

type Props = {
  isPast: boolean
  itemData: AppointmentData
  onPress: () => void
  onRepeatAppointmentPressed: () => void
}

export const AppointmentItem = ({ isPast, itemData, onPress, onRepeatAppointmentPressed }: Props) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <RowView aiCenter mt={20}>
        <Image source={itemData.image} />
        <BaseView ml={12}>
          <SubTitle color={isPast ? 'secondaryText' : 'primaryText'}>{itemData.title}</SubTitle>
          <RowView mt={8}>
            <CalendarSvg
              style={isPast ? { color: color('secondaryText')({ theme }) } : { color: color('primaryText')({ theme }) }}
            />
            <Body color={isPast ? 'secondaryText' : 'primaryText'} ml={8}>
              {formatISOdate(itemData.date)}
            </Body>
          </RowView>
        </BaseView>
        {isPast && (
          <RefreshButton onPress={onRepeatAppointmentPressed} hitSlop={hitSlopParams('XL')}>
            <RefreshIcon20 />
          </RefreshButton>
        )}
      </RowView>
    </TouchableOpacity>
  )
}

const Image = styled.Image`
  width: 72px;
  height: 72px;
  border-radius: 16px;
`

const RefreshButton = styled(TouchableOpacity)`
  margin-left: auto;
  margin-right: 15px;
`

const CalendarSvg = styled(CalendarIcon24)`
  width: 20px;
  height: 20px;
`
