import { format, getMonth, isFuture, parseISO } from 'date-fns'
import { ru } from 'date-fns/locale'

import { TSectionList } from './appointments.container'

import { TRecord } from '@shared/api/records'
import { capitalize } from '@shared/lib/string'

const isoToTime = (date: string) => new Date(parseISO(date)).getTime()

export const historyToSectionList = (rawHistory: TRecord[]) => {
  //Фильтрация удаленных записей из будущего
  rawHistory = rawHistory.filter((item) => {
    const future = isFuture(parseISO(item.datetime))
    if (future && item.deleted) return false
    return true
  })

  if (rawHistory.length === 0) return []

  const future: TSectionList = {
    title: 'Предстоящие',
    data: [],
  }
  const past: Record<string, TSectionList & { monthName: string }> = {}

  rawHistory
    .sort((a, b) => isoToTime(a.datetime) - isoToTime(b.datetime))
    .forEach((item) => {
      const date = parseISO(item.datetime)
      if (isFuture(date)) {
        future.data.push(item)
      } else {
        const month = getMonth(date)
        if (past[month]) {
          past[month].data.push(item)
        } else {
          past[month] = {
            title: String(month),
            data: [item],
            monthName: format(date, 'LLLL', { locale: ru }),
          }
        }
      }
    })
  const pastArray = Object.entries(past)
    .sort(([AMonthNumber], [BMonthNumber]) => +BMonthNumber - +AMonthNumber)
    .map(([_, { data, monthName }]) => ({ title: capitalize(monthName), data }))

  if (future.data.length === 0) return pastArray
  return [future, ...pastArray]
}
