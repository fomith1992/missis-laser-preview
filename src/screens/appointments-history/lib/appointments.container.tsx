import React, { useMemo, useState } from 'react'
import { SectionListRenderItemInfo } from 'react-native'
import useAsync from 'react-use/lib/useAsync'

import { AxiosError } from 'axios'
import * as Analytics from 'expo-firebase-analytics'
import { observer } from 'mobx-react'

import { StackActions, useNavigation } from '@react-navigation/core'

import image1 from '@entities/service/ui/1.png'
import { RoutesAppointments } from '@navigations/stack-appointments/routes'
import { recordsAPI } from '@shared/api'
import { TRecord } from '@shared/api/records'
import { TResponse } from '@shared/api/types'
import { LoadingIndicator } from '@shared/ui/atoms/loading-indicator'

import { RoutesRoot } from 'src/enums/routes'
import { useRootStore } from 'src/stores/lib/use-root-store'

import { AppointmentItem } from '../ui/appointment-item'
import { AppointmentsList } from '../ui/appointments.list'
import { historyToSectionList } from './data.transform'

import { isPast, parseISO } from 'date-fns'

export type TSectionList = {
  title: string
  data: TRecord[]
}

export const AppointmentsContainer = observer(() => {
  const navigation = useNavigation()
  const [counter, forceUpdate] = useState(0)
  const { user_token } = useRootStore().user
  const { setCurrentClinicId, currentClinicId } = useRootStore().geolocationReport

  useAsync(
    async () =>
      await Analytics.logEvent('AppointmentsScreen', {
        screen: 'AppointmentsScreen',
      }),
    [],
  )

  const rawHistory = useAsync(async () => {
    if (!user_token) return
    return await recordsAPI
      .getHistory(user_token, currentClinicId ?? 0)
      .then(({ data: { data } }) => data)
      .catch((e: AxiosError<TResponse>) => console.log(e.response?.data))
  }, [user_token, counter])

  const history = useMemo(() => historyToSectionList(rawHistory.value || []), [rawHistory.value])

  if (rawHistory.loading && counter === 0) {
    return <LoadingIndicator visible />
  }

  const handlerRefresh = () => {
    forceUpdate((v) => v + 1)
  }

  const renderItem = ({ item }: SectionListRenderItemInfo<TRecord, TSectionList>) => (
    <AppointmentItem
      onRepeatAppointmentPressed={() => {
        setCurrentClinicId(item.company.id)
        navigation.dispatch(
          StackActions.push(RoutesRoot.DateTimeSelect, {
            selectedServices: item.services.map((x) => x.id),
            workerId: item.staff.id,
          }),
        )
      }}
      isPast={isPast(parseISO(item.datetime))}
      onPress={() => navigation.dispatch(StackActions.push(RoutesAppointments.Appointment, { appointment: item }))}
      itemData={{
        title: item.services[0]?.title ?? 'Неизвестно',
        date: item.datetime,
        image: image1,
      }}
    />
  )

  return (
    <AppointmentsList
      sections={history}
      renderItem={renderItem}
      onRefresh={handlerRefresh}
      isRefreshing={rawHistory.loading}
    />
  )
})
