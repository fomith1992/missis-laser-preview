import React from 'react'

import { AppointmentsContainer } from './lib/appointments.container'

export const AppointmentsHistoryScreen = () => {
  return <AppointmentsContainer />
}
