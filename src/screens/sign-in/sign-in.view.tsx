import React from 'react'
import { Formik } from 'formik'
import * as yup from 'yup'
import { ColView, ContentLayout, PhoneMaskInput, TextButton } from '@shared/ui'
import { PHONE_MASK_LENGTH } from '@shared/ui/organisms/phone-mask-input'
import { KeyboardAvoidingViews } from '@shared/ui/layouts/keyboard-avoiding-view'

// const MIN_LENGTH = 8
// const MAX_LENGTH = 20
// const ERROR_MIN_MESSAGE = 'Пожалуйста удлините пароль'
// const ERROR_MAX_MESSAGE = 'Максимальная длина пароля - 20 символов. Пожалуйста, сократите ваш пароль. '
const ERROR_PHONE = 'Введенный номер некорректен. Проверьте и исправьте его, пожалуйста.'

const signinSchema = yup
  .object({
    phoneNumber: yup
      .string()
      .length(PHONE_MASK_LENGTH, ERROR_PHONE)
      .matches(/\(\d{3}\) \d{3}-\d\d-\d\d/, ERROR_PHONE)
      .required(ERROR_PHONE),
    // password: yup
    //   .string()
    //   .required(ERROR_MIN_MESSAGE)
    //   .matches(/^[a-zA-Z0-9!@#$%^&_-]+$/, 'Пароль может содержать только латинские буквы, цифры и символы !@#$%^&_-')
    //   .min(MIN_LENGTH, ERROR_MIN_MESSAGE)
    //   .max(MAX_LENGTH, ERROR_MAX_MESSAGE),
  })
  .required()

type PasswordFormData = yup.InferType<typeof signinSchema>

interface SignInViewProps {
  onSubmit: (data: PasswordFormData) => void
  onChangePasswordPressed: () => void
}

export const SignInView = ({ onSubmit }: SignInViewProps) => {
  return (
    <KeyboardAvoidingViews>
      <ContentLayout flex mb={36}>
        <Formik
          initialValues={{ phoneNumber: '' }}
          onSubmit={onSubmit}
          validationSchema={signinSchema}
          validateOnChange={false}
        >
          {({ handleSubmit, handleChange, values, errors, setErrors }) => (
            <>
              <ColView mb="auto" mt="auto">
                <PhoneMaskInput
                  focusOnMount
                  errorMessage={errors.phoneNumber}
                  onChangeText={(e: string) => {
                    handleChange('phoneNumber')(e)
                    setErrors({})
                  }}
                  value={values.phoneNumber}
                />
                {/* <PasswordInput
                mt={28}
                maxLength={20}
                placeholder="Пароль"
                onChangeText={(e: string) => {
                  handleChange('password')(e)
                  setErrors({})
                }}
                value={values.password}
                errorMessage={errors.password}
              /> */}
              </ColView>
              {/* <ColView aiCenter mb={12}>
              <Link onPress={onChangePasswordPressed}>Забыли пароль?</Link>
            </ColView> */}
              <TextButton disabled={Boolean(errors.phoneNumber)} onPress={handleSubmit} type="primary">
                Далее
              </TextButton>
            </>
          )}
        </Formik>
      </ContentLayout>
    </KeyboardAvoidingViews>
  )
}
