import { useNavigation } from '@react-navigation/core'
import { StackActions } from '@react-navigation/routers'
import React from 'react'
import { RoutesRoot } from 'src/enums/routes'
import { useRootStore } from 'src/stores/lib/use-root-store'
import { SignInView } from './sign-in.view'

export const SignInScreen = () => {
  const navigation = useNavigation()
  const { setUserData } = useRootStore().user

  return (
    <SignInView
      onChangePasswordPressed={() => navigation.dispatch(StackActions.push(RoutesRoot.RecoveryPhone))}
      //! realized demo account
      onSubmit={({ phoneNumber }) => {
        if (phoneNumber === '(909) 222-88-01') {
          setUserData({
            avatar: 'https://api.yclients.com/images/no-master.png',
            email: null,
            id: 11502127,
            login: '79092228801',
            name: 'Ольга',
            phone: '79092228801',
            sex: 1,
            user_token: '11533d43e3742325a3e07356936dd947',
          })
          navigation.dispatch(StackActions.push(RoutesRoot.BottomNavigator))
        } else {
          navigation.dispatch(
            StackActions.push(RoutesRoot.SignUpSMSConfirm, {
              phoneNumber,
              stackAction: StackActions.push(RoutesRoot.BottomNavigator),
            }),
          )
        }
      }}
    />
  )
}
