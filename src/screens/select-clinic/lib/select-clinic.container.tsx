import { StackActions, useNavigation } from '@react-navigation/core'
import { CommonActions } from '@react-navigation/native'
import React, { useMemo } from 'react'
import { RoutesRoot } from 'src/enums/routes'
import { useRootStore } from 'src/stores/lib/use-root-store'
import { SelectClinictView } from '../ui/clinic-select.view'
import * as Analytics from 'expo-firebase-analytics'
import useAsync from 'react-use/lib/useAsync'

interface SelectClinicProps {
  city_id: number
  isShortcut: boolean
}

export const SelectClinic = ({ city_id, isShortcut }: SelectClinicProps) => {
  const navigation = useNavigation()
  const { getCityByCityId, getClinicsByCityId, setCurrentClinicId, setCurrentCityId } = useRootStore().geolocationReport

  useAsync(
    async () =>
      await Analytics.logEvent('SelectClinic', {
        screen: 'SelectClinic',
      }),
    [],
  )

  const city = getCityByCityId(city_id)
  const clinics = useMemo(() => getClinicsByCityId(city_id), [city_id, getClinicsByCityId])

  const selectClinicHandler = (clinic_id: number) => {
    setCurrentClinicId(clinic_id)
    if (isShortcut) {
      navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [{ name: RoutesRoot.BottomNavigator }],
        }),
      )
    } else {
      navigation.dispatch(StackActions.replace(RoutesRoot.SexSelect))
    }
  }

  if (clinics.length === 1) {
    setCurrentCityId(city_id)
    selectClinicHandler(clinics[0].id)
  }

  return (
    <SelectClinictView
      data={clinics}
      onSelect={selectClinicHandler}
      city={city?.city}
      onCityPress={() => navigation.goBack()}
    />
  )
}
