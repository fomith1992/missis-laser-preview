import { RootStackParamList } from '@navigations/types'
import { StackScreenProps } from '@react-navigation/stack'
import React from 'react'
import { RoutesRoot } from 'src/enums/routes'

import { SelectClinic } from './lib/select-clinic.container'

export const SelectClinicScreen = ({ route }: StackScreenProps<RootStackParamList, RoutesRoot.SelectClinic>) => {
  const { city, isShortcut } = route?.params
  return <SelectClinic city_id={city} isShortcut={isShortcut} />
}
