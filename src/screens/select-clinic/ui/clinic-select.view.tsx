import React from 'react'
import styled from '@emotion/native'
import { Body, ContentLayout, HLine, RowView } from '@shared/ui'
import { AddressCard } from './address-card.view'
import { color } from 'src/style/mixins/color'
import { FlatList } from 'react-native'
import { CompanyItemInstance } from 'src/stores/domains/geolocation.store'

type SelectClinictViewProps = {
  data: CompanyItemInstance[]
  city?: string
  onSelect: (clinic_id: number) => void
  onCityPress: () => void
}
export const SelectClinictView = ({ onSelect, data, city, onCityPress }: SelectClinictViewProps) => {
  return (
    <FlatList
      data={data}
      ListHeaderComponent={() => (
        <ContentLayout>
          <RowView pt={12} pb={20} aiCenter>
            <Body>
              Ваш город: <CityName onPress={onCityPress}>{city}</CityName>
            </Body>
          </RowView>
          <HLine />
        </ContentLayout>
      )}
      renderItem={({ item }) => <AddressCard address={item} onClick={onSelect} />}
      keyExtractor={(_, id) => id.toString()}
      ItemSeparatorComponent={() => (
        <ContentLayout>
          <HLine />
        </ContentLayout>
      )}
    />
  )
}

const CityName = styled(Body)`
  color: ${color('primary')};
`
