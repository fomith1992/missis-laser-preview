import React from 'react'
import styled from '@emotion/native'

import { Body, ContentLayout, RowView } from '@shared/ui'
import { LocationIcon24 } from '@shared/ui/icons/location.icon-24'
import { color } from 'src/style/mixins/color'
import { CompanyItemInstance } from 'src/stores/domains/geolocation.store'

type AddressCardProps = {
  address: CompanyItemInstance
  onClick: (id: number) => void
}

export const AddressCard = ({ address, onClick }: AddressCardProps) => {
  return (
    <ContentLayout>
      <Card onPress={() => onClick(address.id)}>
        <RowView aiCenter>
          <LocationIcon />
          <Body ml={12} flex>
            {address.public_title}
          </Body>
        </RowView>
      </Card>
    </ContentLayout>
  )
}

const Card = styled.TouchableOpacity`
  padding: 20px 0;
  justify-content: center;
`

const LocationIcon = styled(LocationIcon24)`
  width: 32px;
  height: 32px;
  color: ${color('primary')};
`
