import React from 'react'
import { RootStoreInstance } from '../root.store'
import { RootStoreContext } from './root-store-provider'

export const useRootStore = (): RootStoreInstance => {
  const store = React.useContext(RootStoreContext)
  if (store == null) {
    throw new Error('Root store is not initialized')
  } else {
    return store
  }
}
