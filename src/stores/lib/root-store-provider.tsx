import React from 'react'
import { initializeRootStore, RootStoreInstance } from '../root.store'
import useAsync from 'react-use/lib/useAsync'

export const RootStoreContext = React.createContext<RootStoreInstance | null>(null)

export interface RootStoreProviderProps {
  children: React.ReactNode
  fallback?: React.ReactNode
}

export const RootStoreProvider = ({ children, fallback }: RootStoreProviderProps): React.ReactElement => {
  const { loading, value: rootStore } = useAsync(initializeRootStore)
  if (loading) {
    return <>{fallback}</>
  }

  return <RootStoreContext.Provider value={rootStore ?? null}>{children}</RootStoreContext.Provider>
}
