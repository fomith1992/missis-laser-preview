import { Instance, SnapshotIn, types } from 'mobx-state-tree'

export const ServiceItem = types.model({
  id: types.number,
  title: types.string,
  category_id: types.number,
  price_min: types.number,
  price_max: types.number,
  discount: types.number,
  imageType: types.maybeNull(types.string),
  categoryName: types.maybeNull(types.string),
  prepaid: types.string,
})

export const ServiceList = types
  .model({
    data: types.array(ServiceItem),
  })
  .actions((self) => ({
    setServiceListData(data: ServiceItemInstance[]) {
      self.data.replace(data)
      return data
    },
    pushServiceListData(data: ServiceItemInstance) {
      self.data.push(data)
      return data
    },
  }))

export type ServiceItemInstance = Instance<typeof ServiceItem>

export const initializeServiceListStore = (): SnapshotIn<typeof ServiceList> => ({
  data: [],
})
