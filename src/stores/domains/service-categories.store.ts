import { types, Instance, SnapshotIn } from 'mobx-state-tree'

export const ServiceCategoryItem = types.model({
  id: types.number,
  sex: types.number,
  title: types.string,
  weight: types.number,
  api_id: types.number,
})

export const ServiceCategoryList = types
  .model({
    serviceCategoryListData: types.array(ServiceCategoryItem),
    currentCategoryId: types.number,
  })
  .actions((self) => ({
    setServiceCategoryListData(data: ServiceCategoryItemInstance[]) {
      self.serviceCategoryListData.replace(data)
    },
    clearStore() {
      self.serviceCategoryListData.replace([])
    },
    setCurrentCategoryId(id: number) {
      self.currentCategoryId = id
    },
  }))

export type ServiceCategoryListInstance = Instance<typeof ServiceCategoryList>
export type ServiceCategoryItemInstance = Instance<typeof ServiceCategoryItem>

export const initializeServiceCategoryListStore = (): SnapshotIn<typeof ServiceCategoryList> => ({
  serviceCategoryListData: [],
  currentCategoryId: 0,
})
