import { types, Instance, SnapshotIn } from 'mobx-state-tree'
import { getSecureStoreAsync, SECURE_STORE_KEYS } from '@shared/lib/secure-storage'
import { geoAPI } from '@shared/api'

const CompanyItem = types.model({
  id: types.number,
  title: types.maybe(types.string),
  public_title: types.maybe(types.string),
  short_descr: types.maybe(types.string),
  logo: types.maybe(types.string),
  country_id: types.maybe(types.number),
  country: types.maybe(types.string),
  city_id: types.number,
  city: types.string,
  active: types.maybe(types.number),
  phone: types.maybe(types.string),
  phones: types.array(types.maybe(types.string)),
  email: types.union(types.string, types.null),
  address: types.maybe(types.string),
  reminds_sms_disabled: types.boolean,
  reminds_sms_default: types.maybe(types.number),
  email_hours_default: types.maybe(types.number),
  booking_notify_text: types.maybe(types.string),
  booking_comment_input_name: types.maybe(types.string),
  booking_comment_required: types.boolean,
  booking_email_required: types.boolean,
})

export const CityItem = types.model({
  city_id: types.number,
  city: types.string,
})

export type CompanyItemInstance = Instance<typeof CompanyItem>
export type CityItemInstance = Instance<typeof CityItem>

export const Geolocation = types
  .model({
    data: types.array(CompanyItem),
    cities: types.array(CityItem),
    currentClinicId: types.maybe(types.number),
    currentCityId: types.maybe(types.number),
  })
  .actions((self) => ({
    storageSaveCityId: (cityId: number) => {
      getSecureStoreAsync().then((storage) => storage?.set(SECURE_STORE_KEYS.USER_CITY, cityId))
    },
    storageSaveClinicId: (clinicId: number) => {
      getSecureStoreAsync().then((storage) => storage?.set(SECURE_STORE_KEYS.USER_CLINIC, clinicId))
    },
    reset: () => {
      self.currentCityId = undefined
      self.currentClinicId = undefined
    },
  }))
  .actions((self) => ({
    setGeolocationData: (data: CompanyItemInstance[]) => {
      self.data.replace(data)
      const cities: CityItemInstance[] = []
      data.map(({ city_id, city }) => cities.push({ city_id, city }))
      self.cities.replace(
        cities
          .sort((a, b) => a.city_id - b.city_id)
          .filter((current, index, array) => index === 0 || current.city_id !== array[index - 1].city_id),
      )
    },
    getCityByCityId: (city_id?: number) => {
      return self.cities.find((x) => x.city_id === (city_id ?? self.currentCityId))
    },
    getClinicsByCityId: (city_id: number) => {
      return self.data.filter((x) => x.city_id === city_id)
    },
    setCurrentClinicId: (clinic_id: number) => {
      self.storageSaveClinicId(clinic_id)
      self.currentClinicId = clinic_id
    },
    setCurrentCityId: (city_id: number) => {
      self.storageSaveCityId(city_id)
      self.currentCityId = city_id
    },
    getCurrentClinic: () => {
      return self.data.find((x) => x.id === self.currentClinicId)
    },
  }))
  .actions((self) => ({
    getCompanies: async () => {
      await geoAPI.getAllCompanies().then(({ data }) => self.setGeolocationData(data.data))
    },
  }))

export type GeolocationInstance = Instance<typeof Geolocation>

export const initializeGeolocationStore = (): SnapshotIn<typeof Geolocation> => ({
  data: [],
  cities: [],
})
