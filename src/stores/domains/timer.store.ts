import { types, Instance, SnapshotIn } from 'mobx-state-tree'

export const Timer = types
  .model({
    value: types.number, // MS
    duration: types.number,
    isStarted: types.boolean,
    id: types.optional(types.maybe(types.frozen<NodeJS.Timeout>()), undefined),
  })
  .actions((self) => ({
    _clearTimer: () => {
      if (self.id) {
        clearInterval(self.id)
        self.isStarted = false
        self.value = 0
        self.id = undefined
      }
    },
  }))
  .actions((self) => ({
    _tick: () => {
      if (self.value <= 0) {
        self._clearTimer()
      } else {
        self.value -= 1000
      }
    },
  }))
  .actions((self) => ({
    startTimer: (valueMS: number) => {
      if (self.id === undefined) {
        self.value = valueMS
        self.duration = valueMS
        self.isStarted = true
        if (self.value > 0 || valueMS > 0) {
          self.id = setInterval(self._tick, 1000)
        }
      }
    },
  }))
  .actions((self) => ({
    resetTimer: () => {
      self._clearTimer()
      self.startTimer(self.duration)
    },
  }))

export type TimerInstance = Instance<typeof Timer>

export const initializeTimerStore = (): SnapshotIn<typeof Timer> => ({
  value: 0,
  duration: 60000,
  isStarted: false,
})
