import { types, Instance, SnapshotIn } from 'mobx-state-tree'

export const Register = types
  .model({
    userName: types.maybe(types.string),
    phoneNumber: types.maybe(types.string),
    password: types.maybe(types.string),
    passwordRepeat: types.maybe(types.string),
  })
  .actions((self) => ({
    setPhoneNumber: (phoneNumber?: string) => {
      self.phoneNumber = phoneNumber
    },
    setUserName: (userName?: string) => {
      self.userName = userName
    },
    setPassword: (password?: string) => {
      self.password = password
    },
    setPasswordRepeat: (passwordRepeat?: string) => {
      self.passwordRepeat = passwordRepeat
    },
  }))
  .actions((self) => ({
    clearRegisterStore: () => {
      self.setPhoneNumber(undefined)
      self.setUserName(undefined)
      self.setPassword(undefined)
      self.setPasswordRepeat(undefined)
    },
  }))

export type RegisterInstance = Instance<typeof Register>

export const initializeRegisterStore = (): SnapshotIn<typeof Register> => ({
  userName: undefined,
  phoneNumber: undefined,
  password: undefined,
  passwordRepeat: undefined,
})
