import { types, Instance, SnapshotIn } from 'mobx-state-tree'

export const SMSVerify = types
  .model({
    phone: types.maybeNull(types.string),
    fullname: types.maybe(types.string),
    serverError: types.maybe(types.string),
    codeRequested: types.boolean,
  })
  .actions((self) => ({
    setPhone: (value: string) => {
      self.phone = value
    },
    setFullname: (value?: string) => {
      self.fullname = value
    },
    setServerError: (value?: string) => {
      self.serverError = value
    },
    setCodeRequested: (value: boolean) => {
      self.codeRequested = value
    },
    clearSMSVefifyStore: () => {
      self.phone = null
      self.fullname = undefined
      self.serverError = undefined
      self.codeRequested = false
    },
  }))

export type SMSVerifyInstance = Instance<typeof SMSVerify>

export const initializeSMSVerifyStore = (): SnapshotIn<typeof SMSVerify> => ({
  codeRequested: false,
})
