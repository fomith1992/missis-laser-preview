import { Instance, SnapshotIn, types } from 'mobx-state-tree'

import { backendAPI } from '@shared/api'
import { getSecureStoreAsync, SECURE_STORE_KEYS } from '@shared/lib/secure-storage'

import { IUser } from './types'

const UserSex = types.maybeNull(types.union(types.literal(0), types.literal(1), types.literal(2)))

export type UserSexInstance = Instance<typeof UserSex>

export const User = types
  .model({
    loading: types.boolean,
    avatar: types.maybeNull(types.string),
    email: types.maybeNull(types.string),
    id: types.maybeNull(types.number),
    login: types.maybeNull(types.string),
    name: types.maybeNull(types.string),
    phone: types.maybeNull(types.string),
    user_token: types.maybeNull(types.string),
    sex: UserSex,
  })
  .actions((self) => ({
    storageSaveUserData: (user: NonNullable<IUser>) => {
      getSecureStoreAsync().then((storage) => storage?.set(SECURE_STORE_KEYS.USER, user))
    },
    getUser: () => {
      return {
        avatar: self.avatar,
        email: self.email,
        id: self.id,
        login: self.login,
        name: self.name,
        phone: self.phone,
        user_token: self.user_token,
        sex: self.sex,
      }
    },
    setLoading(value: boolean) {
      self.loading = value
    },
  }))
  .actions((self) => ({
    updateUserDataOnBackend: async (user: Partial<IUser>) => {
      try {
        self.setLoading(true)
        if (user.phone && user.user_token) {
          backendAPI.user.updateUser({ ...user, username: user.phone, userToken: user.user_token })
        }
      } catch (error) {
        console.log('store updateUserDataOnBackend: ' + error)
      } finally {
        self.setLoading(false)
      }
    },
  }))
  .actions((self) => ({
    setUserData: (user: Partial<IUser>) => {
      if (user.avatar) self.avatar = user.avatar
      if (user.email) self.email = user.email
      if (user.id) self.id = user.id
      if (user.login) self.login = user.login
      if (user.name) self.name = user.name
      if (user.phone) self.phone = user.phone
      if (user.user_token) self.user_token = user.user_token
      self.sex = user.sex != null ? user.sex : 0

      const actualUser = self.getUser()

      self.updateUserDataOnBackend(actualUser)
      self.storageSaveUserData(actualUser)
    },
    reset: () => {
      self.avatar = null
      self.email = null
      self.id = null
      self.login = null
      self.name = null
      self.phone = null
      self.user_token = null
      self.sex = null

      const actualUser = self.getUser()
      self.storageSaveUserData(actualUser)
    },
  }))
  .actions((self) => ({
    async getUserDataFromBackend(username: string) {
      try {
        self.setLoading(true)
        return await backendAPI.user.fetchUserData(username)
      } catch (error) {
        self.setLoading(false)
        return null
      }
    },
  }))

export type UserInstance = Instance<typeof User>

export const initializeUserStore = (): SnapshotIn<typeof User> => ({
  sex: 0,
  loading: false,
})
