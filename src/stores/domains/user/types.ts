import { UserSexInstance } from './user.store'

type MaybeNull<T> = T | null

export interface IUser {
  avatar: MaybeNull<string>
  email: MaybeNull<string>
  id: MaybeNull<number>
  login: MaybeNull<string>
  name: MaybeNull<string>
  phone: MaybeNull<string>
  user_token: MaybeNull<string>
  sex: UserSexInstance
}
