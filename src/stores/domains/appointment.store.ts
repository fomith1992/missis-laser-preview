import { cast, Instance, SnapshotIn, types } from 'mobx-state-tree'

const PromoCodeLandingData = types.model({
  benefits: types.array(types.string),
  description: types.string,
  headline: types.string,
  image: types.string,
  importantCells: types.array(
    types.model({
      description: types.string,
      icon: types.string,
      title: types.string,
    }),
  ),
  indications: types.array(types.string),
  middleDescription: types.string,
  type: types.string,
})

const StaffItem = types.model({
  avatar: types.string,
  avatar_big: types.string,
  id: types.number,
  name: types.string,
  rating: types.number,
  schedule_till: types.string,
  specialization: types.string,
  status: types.number,
  bookable: types.boolean,
})

export type StaffItemInstance = Instance<typeof StaffItem>
export type PromocodeLandingInstance = Instance<typeof PromoCodeLandingData>

export const Appointment = types
  .model({
    staffsData: types.array(StaffItem),
    currentServices: types.array(types.number),
    currentStaff: types.maybe(types.number),
    currentDateTime: types.maybe(types.string),
    promocodeLanding: types.maybe(PromoCodeLandingData),
    promocode: types.maybe(types.string),
  })
  .actions((self) => ({
    setAppointmentData: (data: { currentServices: number[]; currentStaff?: number; currentDateTime: string }) => {
      self.currentDateTime = data.currentDateTime.toString()
      self.currentServices = cast(data.currentServices)
      self.currentStaff = data.currentStaff
    },
    setStaffsData: (data: StaffItemInstance[]) => {
      self.staffsData = cast(data)
    },
    setPromocodeLanding: (data: PromocodeLandingInstance) => {
      self.promocodeLanding = cast(data)
    },
    setCurrentServices: (currentServices: number[]) => {
      self.currentServices.replace(currentServices)
    },
    setCurrentStaff: (currentStaff: number) => {
      self.currentStaff = currentStaff
    },
    getCurrentStaff: () => {
      return self.staffsData.find((x) => x.id === self.currentStaff)
    },
    setPromocode: (promocode: string) => {
      self.promocode = promocode
    },
  }))
  .actions((self) => ({
    reset: () => {
      self.currentDateTime = undefined
      self.currentStaff = undefined
      self.promocode = undefined
      self.currentServices = cast([])
      self.staffsData = cast([])
    },
  }))

export type AppointmentInstance = Instance<typeof Appointment>

export const initializeAppointmentStore = (): SnapshotIn<typeof Appointment> => ({
  currentServices: [],
})
