import { types, Instance, SnapshotIn } from 'mobx-state-tree'

export const App = types
  .model({
    openAfterProcedureSheet: types.boolean,
  })
  .actions((self) => ({
    setOpenAfterProcedureSheet: (value: boolean) => {
      self.openAfterProcedureSheet = value
    },
  }))

export type AppInstance = Instance<typeof App>

export const initializeAppStore = (): SnapshotIn<typeof App> => ({
  openAfterProcedureSheet: false,
})
