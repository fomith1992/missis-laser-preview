import React, { useRef } from 'react'
import BottomSheet from '@gorhom/bottom-sheet'

import RootSiblings from 'react-native-root-siblings'

import { BottomSheetGorhom } from '@features/bottom-sheet-gorhom/bottom-sheet'
import { InfoBottomSheetView } from '@screens/appointment/ui/sheet/info-bottom-sheet.view'
import { afterInfoData } from '@screens/appointment/mock/data'

import { TSheet } from '../types'

export const AfterProcedure = (): TSheet => {
  const sheetRef = useRef<BottomSheet>(null)

  const component = (
    <BottomSheetGorhom sheetRef={sheetRef}>
      <InfoBottomSheetView sheetRef={sheetRef} title="После процедуры" data={afterInfoData} />
    </BottomSheetGorhom>
  )

  const sibling = new RootSiblings(component)

  return { sheetRef, component, sibling }
}
