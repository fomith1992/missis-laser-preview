import BottomSheet from '@gorhom/bottom-sheet'
import RootSiblings from 'react-native-root-siblings'

export type SheetTypes = 'after-procedure'

export type TSheet = {
  sheetRef: React.RefObject<BottomSheet>
  sibling: RootSiblings | null
  component: JSX.Element
}
