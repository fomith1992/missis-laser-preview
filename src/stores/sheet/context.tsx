import React from 'react'
import { useContext } from 'react'
import { AfterProcedure } from './templates'
import { SheetTypes, TSheet } from './types'

type TValue = Record<SheetTypes, TSheet> | null

const SheetContext = React.createContext<TValue>(null)

export const SheetProvider: React.FC = ({ children }) => {
  const libriary: TValue = {
    'after-procedure': AfterProcedure(),
  }

  return <SheetContext.Provider value={libriary}>{children}</SheetContext.Provider>
}

export const useSheetLibriary = (sheetType: SheetTypes) => {
  const libriary = useContext(SheetContext)
  if (libriary === null) return

  const template = libriary[sheetType]

  const open = () => template.sheetRef.current?.expand()
  const close = () => template.sheetRef.current?.close()

  return { open, close }
}
