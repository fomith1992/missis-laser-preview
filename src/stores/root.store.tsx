import { types, Instance } from 'mobx-state-tree'
import { Appointment, initializeAppointmentStore } from './domains/appointment.store'
import { Geolocation, initializeGeolocationStore } from './domains/geolocation.store'
import { initializeRegisterStore, Register } from './domains/register.store'
import { initializeSMSVerifyStore, SMSVerify } from './domains/sms-verify.store'
import { initializeTimerStore, Timer } from './domains/timer.store'
import { initializeUserStore, User } from './domains/user/user.store'
import { initializeServiceCategoryListStore, ServiceCategoryList } from './domains/service-categories.store'
import { initializeServiceListStore, ServiceList } from './domains/service.store'
import { App, initializeAppStore } from './domains/app.store'

const RootStore = types.model({
  app: App,
  registerReport: Register,
  user: User,
  timer: Timer,
  smsVerify: SMSVerify,
  geolocationReport: Geolocation,
  appointmentReport: Appointment,
  serviceCategoryListReport: ServiceCategoryList,
  serviceListReport: ServiceList,
})

export type RootStoreInstance = Instance<typeof RootStore>

export const initializeRootStore = async (): Promise<RootStoreInstance> => {
  try {
    const store = RootStore.create({
      app: initializeAppStore(),
      registerReport: initializeRegisterStore(),
      user: initializeUserStore(),
      timer: initializeTimerStore(),
      smsVerify: initializeSMSVerifyStore(),
      geolocationReport: initializeGeolocationStore(),
      appointmentReport: initializeAppointmentStore(),
      serviceCategoryListReport: initializeServiceCategoryListStore(),
      serviceListReport: initializeServiceListStore(),
    })
    return store
  } catch (error) {
    console.log(error)
    throw error
  }
}
