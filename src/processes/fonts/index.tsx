import React, { useState } from 'react'

import { loadAsync } from 'expo-font'
import AppLoading from 'expo-app-loading'
import useAsync from 'react-use/lib/useAsync'

export const useFontLoading = () => {
  const [loading, setLoading] = useState(true)

  useAsync(async () => {
    const load = async () => {
      await loadAsync({
        MontserratBold: require('src/assets/fonts/Montserrat-Bold.ttf'),
        MontserratSemiBold: require('src/assets/fonts/Montserrat-SemiBold.ttf'),
        RobotoRegular: require('src/assets/fonts/Roboto-Regular.ttf'),
      })
      setLoading(false)
    }

    load()
  })

  if (loading) {
    return <AppLoading />
  }

  return <></>
}
