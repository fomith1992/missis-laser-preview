import React from 'react'
import { useFontLoading } from './index'

export const FontContainer: React.FC = ({ children }) => {
  useFontLoading()

  return <>{children}</>
}
