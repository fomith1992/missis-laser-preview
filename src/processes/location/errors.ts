//TODO errors description
// error code 1: is not device
// error code 2: you must enable service location
// error code 3: user did not give permission
// error code 4: user has forbidden the use location
// error code 5: user has forbidden the use location and you can no longer request it

import { TLocationError } from './use-location'

const IS_NOT_DEVICE = 'Oops, this will not work on Snack in an Android emulator. Try it on your device!'
const NOT_ENABLE_LOCATION = 'you must enable service location'
const UNDETERMINED = 'Undetermined status'
const FORBIDDEN_LOCATION = 'User has forbidden the use location'

const messages = {
  1: IS_NOT_DEVICE,
  2: NOT_ENABLE_LOCATION,
  3: UNDETERMINED,
  4: FORBIDDEN_LOCATION,
  5: FORBIDDEN_LOCATION,
}

type TCodeErrors = 1 | 2 | 3 | 4 | 5

export const toLocationError = (code: TCodeErrors): TLocationError => {
  return { code, message: messages[code] || 'error' }
}
