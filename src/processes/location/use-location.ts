import { useEffect, useState } from 'react'
import { Platform } from 'react-native'
import Constants from 'expo-constants'
import * as Location from 'expo-location'
import { LocationGeocodedAddress } from 'expo-location'
import { toLocationError } from './errors'

export type TLocationError = {
  code: number
  message: string
} | null
interface UseLocation {
  error: TLocationError
  location: LocationGeocodedAddress[] | null
  locationLoading: boolean
  requestLocationPermission: () => Promise<void>
}

export function useLocation(): UseLocation {
  const [location, setLocation] = useState<LocationGeocodedAddress[] | null>(null)
  const [status, setUpdateStatus] = useState<string | null>(null)
  const [error, setError] = useState<TLocationError>(null)
  const [locationLoading, setLocationLoading] = useState(false)

  useEffect(() => {
    const tryGetLocation = async () => {
      if (Platform.OS === 'android' && !Constants.isDevice) {
        setError(toLocationError(1))
        return
      }

      const getForegroundPermissions = await Location.getForegroundPermissionsAsync()
      if (getForegroundPermissions.status === 'undetermined') {
        setError(toLocationError(3))
        return
      } else if (getForegroundPermissions.status === 'denied') {
        const codeNumber = getForegroundPermissions.canAskAgain ? 5 : 4
        setError(toLocationError(codeNumber))
        return
      }

      const hasServicesEnabled = await Location.hasServicesEnabledAsync()
      if (!hasServicesEnabled) {
        await Location.enableNetworkProviderAsync()
      }

      const reverseGeocodeAsync = async (latitude: number, longitude: number) => {
        const data = await Location.reverseGeocodeAsync({
          latitude: latitude,
          longitude: longitude,
        })
        setLocation(data)
        setError(null)
      }

      const locationPosition = await Location.getCurrentPositionAsync({})
      reverseGeocodeAsync(locationPosition.coords.latitude, locationPosition.coords.longitude)
    }

    try {
      setLocationLoading(true)
      tryGetLocation()
    } catch (getLocationError) {
      console.error(getLocationError)
    } finally {
      setLocationLoading(false)
    }
  }, [status])

  const requestLocationPermission = async () => {
    try {
      const { status: requestStatus } = await Location.requestForegroundPermissionsAsync()
      setUpdateStatus(requestStatus)
    } catch (requestError) {
      console.error(requestError)
    }
  }

  return { error, location, requestLocationPermission, locationLoading }
}
