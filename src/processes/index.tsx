import { useState } from 'react'
import useEffectOnce from 'react-use/lib/useEffectOnce'

import { getSecureStoreAsync, SECURE_STORE_KEYS } from '@shared/lib/secure-storage'

import { IUser } from 'src/stores/domains/user/types'
import { useRootStore } from 'src/stores/lib/use-root-store'

export const useLoadingProcesses = () => {
  const [loading, setLoading] = useState(true)

  const { setUserData } = useRootStore().user
  const { setCurrentCityId, setCurrentClinicId, getCompanies } = useRootStore().geolocationReport

  useEffectOnce(() => {
    const loadProcesses = async () => {
      await getCompanies()
      try {
        const storage = await getSecureStoreAsync()
        const userData = await storage?.get<IUser>(SECURE_STORE_KEYS.USER)
        const currentCityId = (await storage?.get<number>(SECURE_STORE_KEYS.USER_CITY)) ?? 2
        const currentClinicId = (await storage?.get<number>(SECURE_STORE_KEYS.USER_CLINIC)) ?? 291422

        if (userData) setUserData(userData)
        if (currentCityId) setCurrentCityId(currentCityId)
        if (currentClinicId) setCurrentClinicId(currentClinicId)
      } catch (e) {
        console.error('Unable to use secure store' + e)
      }

      setLoading(false)
    }

    loadProcesses().catch((e) => {
      throw new Error('Processes dont finished' + e)
    })
  })

  return loading
}
