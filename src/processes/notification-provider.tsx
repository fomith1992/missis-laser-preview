import { notificationsAPI } from '@shared/api'
import Constants from 'expo-constants'
import * as Notifications from 'expo-notifications'
import { observer } from 'mobx-react'
import React, { useState, useEffect } from 'react'
import { Alert } from 'react-native'
import useAsync from 'react-use/lib/useAsync'
import { useRootStore } from 'src/stores/lib/use-root-store'
import { useSheetLibriary } from 'src/stores/sheet'
import { NotificationTokenProvider } from '../app/contexts/notification-token-context'

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: false,
    shouldSetBadge: false,
  }),
})

interface NotificationProviderProps {
  children: React.ReactElement
  fallback?: React.ReactNode
}

type ContextValue =
  | {
      init: true
      value: string | null
    }
  | {
      init: false
      value: null | undefined
    }

export const NotificationProvider = observer(
  ({ children, fallback }: NotificationProviderProps): React.ReactElement | null => {
    const { phone, user_token } = useRootStore().user
    const [expoPushToken, setExpoPushToken] = useState<ContextValue>({
      init: false,
      value: null,
    })

    const afterProcedureSheet = useSheetLibriary('after-procedure')

    useEffect(() => {
      registerForPushNotificationsAsync().then((token) => {
        setExpoPushToken({ init: true, value: token ?? null })
      })
    }, [])

    useEffect(() => {
      // This listener is fired whenever a notification is received while the app is foregrounded
      // Notifications.addNotificationReceivedListener((notification) => {
      //   console.log('incoming notification', notification)
      // })

      // This listener is fired whenever a user taps on or interacts with a notification (works when app is foregrounded, backgrounded, or killed)
      Notifications.addNotificationResponseReceivedListener(({ notification }) => {
        const data = notification.request.content.data
        if (data.isAfter) {
          afterProcedureSheet?.open()
        }
      })
    }, [afterProcedureSheet])

    const queryState = useAsync(async () => {
      if (phone != null && expoPushToken.value && user_token && expoPushToken.init) {
        return await notificationsAPI.sendExponentPushToken({
          phone,
          pushToken: expoPushToken.value,
          userToken: user_token,
        })
      } else return null
    }, [phone, user_token, expoPushToken, expoPushToken.init])

    if (!expoPushToken.init) {
      return <>{fallback}</>
    }

    if (queryState.value?.status === 201) {
      console.log(`[backend] ${expoPushToken.value} has been updated`)
    }

    return <NotificationTokenProvider value={expoPushToken.value ?? 'nothing'}>{children}</NotificationTokenProvider>
  },
)

// Can use this function below, OR use Expo's Push Notification Tool-> https://expo.io/notifications
/* async function sendPushNotification(expoPushToken) {
  const message = {
    to: expoPushToken,
    sound: 'default',
    title: 'Original Title',
    body: 'And here is the body!',
    data: { someData: 'goes here' },
  }

  await fetch('https://exp.host/--/api/v2/push/send', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Accept-encoding': 'gzip, deflate',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(message),
  })
} */

async function registerForPushNotificationsAsync() {
  let token
  if (Constants.isDevice && !Constants.platform?.web) {
    const { status: existingStatus } = await Notifications.getPermissionsAsync()
    let finalStatus = existingStatus
    if (existingStatus !== 'granted') {
      const { status } = await Notifications.requestPermissionsAsync()
      finalStatus = status
    }
    if (finalStatus !== 'granted') {
      Alert.alert('Failed to get push token for push notification!')
      return
    }
    token = (await Notifications.getExpoPushTokenAsync()).data
  } else {
    Alert.alert('Must use physical device for Push Notifications')
  }
  return token
}
