export const theme = {
  colors: {
    white: '#fff',
    black: '#000',

    primary: '#1F86FE',
    primaryAdditional: '#28B9BE',
    primaryVariant: '#1D6ECD',
    primaryLight: '#ddedff',

    background: '#fff',
    surface: '#ebebeb',

    error: '#FA8383',
    errorDark: '#e27878',
    error15: '#FEEDED',
    errorLight: '#FF98A5',
    warning: '#FECA57',

    inactive: '#B8B8B8',
    inactiveLight: '#E9E9E9',

    primaryText: '#191919',
    secondaryText: '#808080',
    tertiaryText: '#B8B8B8',
    lightText: '#fff',

    selectionColor: '#ddedff',
  },
}

export type TTheme = typeof theme

export type TColorType = keyof TTheme['colors']
