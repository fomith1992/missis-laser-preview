import { css, ReactNativeStyle } from '@emotion/native'
import { theme, TTheme } from '../theme'

type TFontVariant = 'Headline' | 'Title' | 'SubTitle' | 'Body' | 'footnote' | 'caption'
type TFontColorType = keyof TTheme['colors'] | null

export type TFontParams = {
  variant: TFontVariant
  color?: TFontColorType
}

const MontserratBold = 'MontserratBold'
const MontserratSemiBold = 'MontserratSemiBold'
const RobotoRegular = 'RobotoRegular'

const fontVariants: Record<TFontVariant, ReactNativeStyle> = {
  Headline: css`
    font-size: 24px;
    line-height: 28px;
    font-weight: 700;
    font-family: ${MontserratBold};
  `,
  Title: css`
    font-size: 18px;
    line-height: 24px;
    font-weight: 700;
    font-family: ${MontserratBold};
  `,
  SubTitle: css`
    font-size: 16px;
    line-height: 20px;
    font-weight: 600;
    font-family: ${MontserratSemiBold};
  `,
  Body: css`
    font-size: 16px;
    line-height: 20px;
    font-family: ${RobotoRegular};
  `,
  footnote: css`
    font-size: 14px;
    line-height: 20px;
    font-family: ${RobotoRegular};
  `,
  caption: css`
    font-size: 12px;
    line-height: 16px;
    font-family: ${RobotoRegular};
  `,
}

export const font = (params: TFontParams): ReactNativeStyle => {
  const defaultStyles = getStylesByVariant(params.variant)

  return css`
    color: ${theme.colors[params.color ?? 'primaryText']};

    ${defaultStyles};
  `
}

function getStylesByVariant(variant: TFontVariant) {
  return fontVariants[variant]
}
