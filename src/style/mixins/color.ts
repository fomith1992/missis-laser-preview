import { TColorType, TTheme } from '../theme'

type StyledProps = {
  theme: TTheme
}

export function color(pickedColor: TColorType) {
  return ({ theme }: StyledProps) => theme.colors[pickedColor]
}
