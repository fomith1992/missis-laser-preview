import { css, ReactNativeStyle } from '@emotion/native'
import { TScaleUnits } from './types'

type TPaddingKeys = 'pl' | 'pr' | 'pb' | 'pt'
type TMarginKeys = 'ml' | 'mr' | 'mb' | 'mt'

type TKeys = TPaddingKeys | TMarginKeys

export type SpaceProps = Partial<Record<TKeys, TScaleUnits | 'auto'>>

const spaceToCSS = (params: SpaceProps) => {
  const styles: ReactNativeStyle[] = []

  const { ml, mr, mb, mt } = params
  const { pl, pr, pb, pt } = params

  if (ml) styles.push(css({ marginLeft: params.ml }))
  if (mr) styles.push(css({ marginRight: params.mr }))
  if (mb) styles.push(css({ marginBottom: params.mb }))
  if (mt) styles.push(css({ marginTop: params.mt }))

  if (pl) styles.push(css({ paddingLeft: params.pl }))
  if (pr) styles.push(css({ paddingRight: params.pr }))
  if (pb) styles.push(css({ paddingBottom: params.pb }))
  if (pt) styles.push(css({ paddingTop: params.pt }))

  return css`
    ${styles};
  `
}

export const space = () => {
  return ({ ml, mb, mr, mt, pl, pr, pb, pt }: SpaceProps) => spaceToCSS({ ml, mb, mr, mt, pl, pr, pb, pt })
}

export const container = (type: 'padding' | 'margin') => {
  return (): ReactNativeStyle => {
    switch (type) {
      case 'margin':
        return css`
          margin-left: 16px;
          margin-right: 16px;
        `
      case 'padding':
        return css`
          padding-left: 16px;
          padding-right: 16px;
        `
    }
  }
}
