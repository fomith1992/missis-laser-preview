/* eslint-disable @typescript-eslint/no-empty-interface */

import '@emotion/react'

import { TTheme } from 'src/style/theme'

declare module '@emotion/react' {
  export interface Theme extends TTheme {}
}
