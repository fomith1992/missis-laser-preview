import { ReactNativeStyle } from '@emotion/native'

export function IfThen<T>(prop: keyof T, style: ReactNativeStyle) {
  return (params: T) => {
    const value = params[prop]
    if (value) {
      return style
    }
    return
  }
}
