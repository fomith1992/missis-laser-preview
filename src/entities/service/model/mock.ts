import { TServicesItem } from './types'

import image1 from '../ui/1.png'
/* import image2 from '../ui/2.png'
import image3 from '../ui/3.png' */

export const mockData: TServicesItem[] = [
  {
    photo: image1,
    title: 'Лазерная эпиляция',
    description: 'Лучший способ избавиться от лишних волос',
    onPress: () => console.log('tick'),
  },
  /* {
    photo: image2,
    title: 'Аппаратная косметология',
    description: 'Красота без хирургического вмешательства',
  },
  {
    photo: image3,
    title: 'Аппаратный массаж',
    description: 'Красивый результат и приятный процесс',
  }, */
]
