import { ImageSourcePropType } from 'react-native'

export type TServicesItem = {
  title: string
  description: string
  photo: ImageSourcePropType
  onPress: () => void
}
