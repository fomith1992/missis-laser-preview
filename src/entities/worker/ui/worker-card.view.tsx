import React from 'react'
import styled from '@emotion/native'
import { TouchableOpacity } from 'react-native'

import { IWorker } from '@entities/worker/model/types'
import { BaseView, Body, ColView, Footnote, RowView } from '@shared/ui'

interface WorkerCardViewProps extends IWorker {
  onPress?: (item: IWorker) => void
  imageSize: number
}

export const WorkerCardView = ({ onPress, imageSize, ...item }: WorkerCardViewProps) => {
  const pressHandler = () => {
    if (onPress) {
      onPress(item)
    }
  }

  return (
    <TouchableOpacity activeOpacity={onPress ? undefined : 1} onPress={pressHandler}>
      <RowView>
        <BaseView mr={16}>
          <Image size={imageSize} source={{ uri: item.image }} />
        </BaseView>
        <Info>
          <Body bold>{item.name}</Body>
          <Footnote>{item.specialization}</Footnote>
        </Info>
      </RowView>
    </TouchableOpacity>
  )
}

const Image = styled.Image<{ size: number }>`
  width: ${({ size }) => `${size}px`};
  height: ${({ size }) => `${size}px`};
  border-radius: 10px;
`

const Info = styled(ColView)`
  flex: 1;
  justify-content: center;
`
