export interface IWorker {
  id: number
  name: string
  specialization: string
  image: string
  bookable: boolean
}
