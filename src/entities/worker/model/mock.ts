import { IWorker } from './types'

export const mockStaff: IWorker[] = [
  {
    id: 1,
    name: 'Решетникова Татьяна',
    specialization: 'Косметолог',
    image: 'https://assets.yclients.com/masters/sm/c/ce/ce812ace43f9592_20210511102835.png',
    bookable: true,
  },
  {
    id: 2,
    name: 'Джабраилова Зулейхат',
    specialization: 'Косметолог',
    image: 'https://assets.yclients.com/masters/sm/c/ce/ce812ace43f9592_20210511102835.png',
    bookable: true,
  },
  {
    id: 3,
    name: 'Лукашова Дарья',
    specialization: 'Косметолог',
    image: 'https://assets.yclients.com/masters/sm/c/ce/ce812ace43f9592_20210511102835.png',
    bookable: true,
  },
  {
    id: 4,
    name: 'Киселевич Татьяна Романовна',
    specialization: 'Косметолог',
    image: 'https://assets.yclients.com/masters/sm/c/ce/ce812ace43f9592_20210511102835.png',
    bookable: true,
  },
]
