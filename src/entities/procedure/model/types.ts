import { ImageSourcePropType } from 'react-native'

export type TProcedure = {
  title: string
  description: string
  price: number
  photo: ImageSourcePropType
}
