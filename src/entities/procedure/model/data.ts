import image1 from '../ui/1.png'
import image2 from '../ui/2.png'

import { TProcedure } from './types'

export const mockData: Array<TProcedure> = [
  {
    title: 'Диодный лазер',
    description: 'Менее болезненно и подходит для любого цвета кожи',
    price: 390,
    photo: image1,
  },
  {
    title: 'Александритовый лазер',
    description: 'Моментальный эффект. Для тёмных волос и светлой кожи. ',
    price: 490,
    photo: image2,
  },
]
