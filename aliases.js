module.exports = {
  '@assets': './src/assets',
  '@shared': './src/shared',
  '@features': './src/features',
  '@navigations': './src/navigations',
  '@screens': './src/screens',
  '@entities': './src/entities',
}
