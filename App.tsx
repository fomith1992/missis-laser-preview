import React from 'react'

import { SafeAreaProvider } from 'react-native-safe-area-context'
import { ThemeProvider } from '@emotion/react'

import { RootSiblingParent } from 'react-native-root-siblings'
import useEffectOnce from 'react-use/lib/useEffectOnce'

import { Application } from 'src/application'
import { theme } from 'src/style/theme'
import { NotificationProvider } from 'src/processes/notification-provider'
import { RootStoreProvider } from 'src/stores/lib/root-store-provider'
import * as Analytics from 'expo-firebase-analytics'
import { NavigationContainer } from '@navigations/ui/nav-container'
import { SheetProvider } from 'src/stores/sheet'
import AppLoading from 'expo-app-loading'

import { FontContainer } from 'src/processes/fonts/font-container'
import useAsync from 'react-use/lib/useAsync'
import * as Facebook from 'expo-facebook'

export default function App() {
  useEffectOnce(() => Analytics.setUnavailabilityLogging(false))

  useAsync(async () => {
    try {
      await Facebook.initializeAsync({
        appId: '1223831794696194',
      })
    } catch ({ message }) {
      console.log(`Facebook initialize Error: ${message}`)
    }
  }, [])

  return (
    <FontContainer>
      <ThemeProvider theme={theme}>
        <SafeAreaProvider>
          <NavigationContainer>
            <RootStoreProvider fallback={<AppLoading autoHideSplash={false} />}>
              <RootSiblingParent>
                <SheetProvider>
                  <NotificationProvider fallback={<AppLoading autoHideSplash={false} />}>
                    <Application />
                  </NotificationProvider>
                </SheetProvider>
              </RootSiblingParent>
            </RootStoreProvider>
          </NavigationContainer>
        </SafeAreaProvider>
      </ThemeProvider>
    </FontContainer>
  )
}
